<?php

Auth::routes();
/*____________________________________________________________________________________________________________________
|
|                                                  ADMIN SECTION
|_____________________________________________________________________________________________________________________
|
| Here declared route of admin section. To all of them "admin" prefix will be added.
| Subsections:
|   - General - main page routes
|   - Profile - profile displaying and editing
|   - User - user managing section
|   - Site - recyclers entities managing
|   - Brand - manufacturer entities managing
|   - BrandGroup - manufacturer groups entities managing
|   - Parsing reports - parsing reports managing
|   - Product - products entities managing
*/
Route::name('admin.')->middleware('auth')->group(function () {

    /*
    |                                          Admin Main Section
    |_________________________________________________________________________________________________________________
    |
    |  Here routes for admin main page
    |
    */
    Route::get('/', 'Admin\HomeController@index')->name('main');
    Route::post('/files/upload', 'Admin\FileController@upload')
        ->name('files.upload')
        ->middleware('upload.file');

    Route::get('/dev', 'Admin\HomeController@test');

  /*
   |                                          Admin User Section
   |_________________________________________________________________________________________________________________
   |
   |  Here routes for user managing are declared
   |
   */
    Route::prefix('users')->name('user.')->group(function () {
        Route::get('/', 'Admin\UserController@index')->name('main');
        Route::get('/create', 'Admin\UserController@create')->name('create');
        Route::get('/delete/{user}', 'Admin\UserController@destroy')
            ->where('user', '[0-9]+')
            ->name('delete');
        Route::get('/update/{user}', 'Admin\UserController@update')
            ->where('user', '[0-9]+')
            ->name('update');
        Route::post('/storage', 'Admin\UserController@storage')->name('storage');
    });
});
