<?php

return [

    /*
    |___________________________________________________________________________________________________________________
    |
    |                                                 PARSER BUILDING SCHEMA
    |___________________________________________________________________________________________________________________
    |
    | Here you may specify from which components build parser for specific site.
    | If site name not specified - the default building schema will be used
    | 'schema' => [
    |     'site_parsing_name' => [
    |         'source' => SourceInterface class
    |         'preparator' => DataPreparatorInterface class,
    |         'importer' => ModelImporter class,
    |     ],
    |
    */
    'scheme' => [
        /*'default' => [
            'source' => \App\Parsers\SourceGetters\JsonSource::class,
            'preparator' => \App\Parsers\DataPreparators\DataPreparator::class,
            'importer' => \App\Parsers\ModelImporters\ModelImporter::class,
        ],*/
        'default' => [
            'source' => \App\Services\Parsing\Parser\Source\JsonSource::class,
            'preparator' => \App\Utiles\Parser\Preparator\Preparator::class,
            'importer' => \App\Services\Parsing\Parser\Import\ModelImporter::class
        ],
    ],

    /*
    |___________________________________________________________________________________________________________________
    |
    |                                                 DATA PREPARATION
    |___________________________________________________________________________________________________________________
    |
    | Here you may specify configs for proper preparation of data for import
    |
    */
    'preparator' => [

        /*-------------------------------------------------------------------------------------------------------------
        |
        |                                             Data-structure
        |--------------------------------------------------------------------------------------------------------------
        |
        | Here declared the schema for transforming of data structure into standard.
        | Structure transformers, should be specified by alias, that declared at data-transforming config file.
        | 'data-structure' => [
        |    Preparator class name => [
        |        'transformer alias' => [
        |           'Transformer configs according the data-transforming config file
        |        ],
        |
        */
        'data-structure' => [
            'default' => [
                'structure' => [
                    'manufacturer' => [
                        'alias' => 'manufacturer'
                    ],
                    'model' => [
                        'alias' => 'model'
                    ],
                    'url' => [
                        'alias' => 'url'
                    ],
                    'width' => [
                        'alias' => 'width'
                    ],
                    'profile' => [
                        'alias' => 'profile'
                    ],
                    'rim_size' => [
                        'alias' => 'rim_size'
                    ],
                    'speed_rating' => [
                        'alias' => 'speed_rating'
                    ],
                    'load_index' => [
                        'alias' => 'load_index'
                    ],
                    'has_label' => [
                        'alias' => 'has_label'
                    ],
                    'site_id' => [
                        'alias' => 'site_id'
                    ],
                    'label' => [
                        'alias' => 'label',
                        'nested' => [
                            'fuel_efficiency' => [
                                'alias' => 'fuel_efficiency'
                            ],
                            'wet_grip' => [
                                'alias' => 'wet_grip'
                            ],
                            'noise_level' => [
                                'alias' => 'noise_level'
                            ],
                        ]
                    ],
                    'prices' => [
                        'alias' => 'prices',
                        'nested' => [
                            'Delivered' => [
                                'alias' => 'delivered'
                            ],
                            'Fitted' => [
                                'alias' => 'fitted'
                            ],
                            'MobileFitted' => [
                                'alias' => 'mobileFitted'
                            ],
                        ]
                    ],
                    'specials' => [
                        'alias' => 'specials'
                    ],
                ]
            ],
        ],

        /*-------------------------------------------------------------------------------------------------------------
        |
        |                                            Preparator Building schema
        |--------------------------------------------------------------------------------------------------------------
        |
        | Here declared the schema for preparator class building. If class schema not specify -
        |  standard schema will be used
        |   'schema' => [
        |        'Preparator class name' => [
        |            'validator' => Validator class name,
        |            'builder' => Parser object builder,
        |            'objectsFactory' => DTO factory,
        |            'specialTransformer' => Special transformer class name,
        |            'tyreTransformer' => Tyre transformer class name,
        |            'propertyTransformer' => Property transformer class name,
        |            'labelTransformer' => Label transformer class name,
        |        ],
        |    ],
        */
        'schema' => [
            'default' => [
                'validator' => \App\Utiles\Validators\Builder::class,
                'builder' => \App\Services\Parsing\Parser\Preparator\DataBuilder::class,
                'transformer' => \App\Utiles\Transformers\Arrays\Data\TransformerService::class,
                'structureTransformer' => \App\Utiles\Transformers\Arrays\Structure\Service::class,
            ],
        ],

        /*-------------------------------------------------------------------------------------------------------------
        |
        |                                            Parsing DTO objects list
        |--------------------------------------------------------------------------------------------------------------
        |
        | Here declared the schema list of DTO object with connected with aliases
        |   'DTO' => [
        |       'alias' => DTO class name,
        |    ],
        */
        'DTO' => [
            'default' => [
                'parse' => \App\Services\Parsing\Parser\Preparator\Objects\ParseTransferObject::class,
                'tyre' => \App\Services\Tyre\Tyre\Parsing\Objects\TyreDTO::class,
                'brand' => \App\Services\Tyre\Brand\Brand\Parsing\Objects\BrandDTO::class,
                'price' => \App\Services\Price\Price\Parsing\Objects\PriceDTO::class,
                'priceGroup' => \App\Services\Price\Fitting\Parsing\Objects\PriceGroupDTO::class,
                'link' => \App\Services\Shared\Link\Parsing\Object\LinkDTO::class,
                'property' => \App\Services\Tyre\Property\Parsing\Objects\PropertyDTO::class,
                'value' => \App\Services\Tyre\Property\Parsing\Objects\ValueDTO::class,
                'special' => \App\Services\Tyre\Special\Special\Parsing\Objects\SpecialDTO::class,
                'tyreProperty' => \App\Services\Tyre\Property\Parsing\Objects\TyrePropertyDTO::class,
                'model' => \App\Services\Tyre\Model\Parsing\Objects\TyreModelDTO::class,
                'collection' => \App\Utiles\DTO\Samples\CompositeObject::class,
            ]
        ],

        'validation' => [
            'default' => 'parsing'
        ],

        'properties' => [
            'default' => [
                'properties' => ['width', 'profile', 'rim_size', 'speed_rating', 'load_index'],
                'labels' => ['fuel_efficiency', 'wet_grip', 'noise_level'],
            ],
        ],

        /*-------------------------------------------------------------------------------------------------------------
        |
        |                                            Transformers
        |--------------------------------------------------------------------------------------------------------------
        |
        | Here declared the filters that should use for data transforming.
        | 'filter' index is specify for filter class
        | 'schema' index used for filter configuration. There declared the alias of the filter
        | and list of filter classes that should be added to the base filter composite. If filter alias not specify
        | default schema will be used
        |
        |   'transformers' => [
        |       'preparator class name' => [
        |           'transformer alias' => [
        |                'filters' => [
        |                   'filter' => Filter class name,
        |                   'schema' => [
        |                       'width' => [
        |                            FilterItem1, FilterItem2 ...
        |                       ],
        |                   ],
        |               ],
        |               'properties => ['list of properties need to be filtered']
        |            ],
        |        ],
        |    ]
        */

        'transformers' => [
            'default' => [
                'label' => [
                    /*
                     * Filters declaring
                     */
                    'filters' => [
                        'filter' => \App\Utiles\Filters\String\Filter::class,
                        'schema' => [
                            'noise_level' => [
                                \App\Services\Tyre\Property\Parsing\Filters\NoiseLevelMerging::class,
                            ],
                            'default' => [],
                        ],
                    ],
                    'aliases' => [
                        'noise_level'
                    ]
                ],
                'property' => [
                    /*
                     * Filters declaring
                     */
                    'filters' => [
                        'filter' => \App\Utiles\Filters\String\Filter::class,
                        'schema' => [
                            'width' => [
                                \App\Utiles\Filters\String\FilterItems\RemoveZeroes::class,
                            ],
                            'rim_size' => [
                                \App\Services\Tyre\Property\Parsing\Filters\MakeRimSizeFormate::class,
                            ],
                            'speed_rating' => [
                                \App\Utiles\Filters\String\FilterItems\RemoveBrackets::class,
                            ],
                            'load_index' => [
                                \App\Utiles\Filters\String\FilterItems\RemoveNotNumerous::class
                            ],
                            'default' => [],
                        ],
                    ],
                    'aliases' => [
                        'width', 'rim_size', 'speed_rating', 'load_index', 'profile'
                    ]
                ],
                'manufacturer' => [
                    /*
                     * Filters declaring
                     */
                    'filters' => [
                        'filter' => \App\Utiles\Filters\String\Filter::class,
                        'schema' => [
                            'manufacturer' => [
                                \App\Services\Tyre\Brand\Brand\Parsing\Filters\BrandSynonymsFilter::class,
                            ],
                            'default' => [],
                        ],
                    ],
                    'aliases' => [
                        'manufacturer'
                    ]
                ],
                'model-name' => [
                    /*
                    * Filters declaring
                    */
                    'filters' => [
                        'filter' => \App\Utiles\Filters\String\Filter::class,
                        'schema' => [
                            'model' => [
                                \App\Services\Tyre\Brand\Brand\Parsing\Filters\RemoveBrandName::class,
                                \App\Services\Tyre\Model\Parsing\Filters\RemoveFromModelName::class,
                                \App\Services\Tyre\Model\Parsing\Filters\TyreSynonymsFilter::class,
                                \App\Services\Tyre\Brand\Brand\Parsing\Filters\BrandSpecificModelSynonymChange::class,
                                \App\Services\Tyre\Model\Parsing\Filters\ModelSynonymWithExceptionCheck::class,
                            ],
                            'default' => [],
                        ],
                    ],
                    'aliases' => [
                        'model'
                    ]
                ],
                'specials' => [
                    /*
                     * Filters declaring
                     */
                    'filters' => [
                        'filter' => \App\Utiles\Filters\String\Filter::class,
                        'schema' => [
                            'specials' => [
                                \App\Services\Tyre\Special\Special\Parsing\Filters\RemoveIgnoredSpecials::class,
                                \App\Services\Tyre\Special\Special\Parsing\Filters\MakeStandardSpecial::class,
                            ],
                            'default' => [],
                        ],
                    ],
                    'aliases' => [
                        'specials'
                    ]
                ],
                'model' => [
                    /*
                     * Filters declaring
                     */
                    'filters' => [
                        'filter' => \App\Utiles\Filters\String\Filter::class,
                        'schema' => [
                            'model' => [
                                \App\Services\Tyre\Property\Parsing\Filters\RemoveAllProperties::class,
                                \App\Services\Tyre\Property\Parsing\Filters\RemoveLoadIndex::class
                            ],
                            'default' => [],
                        ],
                    ],
                    'aliases' => [
                        'model'
                    ]
                ],
                'tyre' => [],
                'price' => [],
            ],

        ],
    ],

    'transformers' => [
        'label' => \App\Services\Tyre\Property\Parsing\Transformers\LabelTransformer::class,
        'property' => \App\Services\Tyre\Property\Parsing\Transformers\PropertyTransformer::class,
        'specials' => \App\Services\Tyre\Special\Special\Parsing\Transformers\SpecialsTransformer::class,
        'tyre' => \App\Services\Tyre\Tyre\Parsing\Transformers\TyreTransformer::class,
        'price' => \App\Services\Price\Price\Parsing\Transformers\PriceTransformer::class,
        'model' => \App\Services\Tyre\Model\Parsing\Transformers\ModelTransformer::class,
        'model-name' => \App\Services\Tyre\Model\Parsing\Transformers\ModelNameTransformer::class,
        'manufacturer' => \App\Services\Tyre\Brand\Brand\Parsing\Transformers\BrandTransformer::class,
    ],


    /*
    |___________________________________________________________________________________________________________________
    |
    |                                                      SPECIALS
    |___________________________________________________________________________________________________________________
    |
    | Here you may specify the list of specials, that should be ignored or
    | should not be used for specification of tyres and should be removed from model name
    |
    */
    'specials' => [
        'ignored' => ['Summer', 'Car', 'Fitment', 'Tyre', 'B F Goodrich', 'LHD', 'Temp'],
        'notRelevant' => [
            '6 ply',
            '8 ply',
            '10 ply',
            '4x4',
            'Alfa Romeo',
            'Aston Martin',
            'Audi',
            'Bentley',
            'BMW',
            'Citreon',
            'Chevrolet',
            'Ferrari',
            'Fiat',
            'Ford',
            'Honda',
            'Hyundai',
            'Infinity',
            'Jaguar',
            'Jeep',
            'Kia',
            'Lamborghini',
            'Lancia',
            'Land Rover',
            'Land Cruiser',
            'Lexus',
            'Lotus',
            'Maserati',
            'Mazda',
            'McLaren',
            'Mercedes',
            'Mitsubishi',
            'Nissan',
            'Opel',
            'Porsche',
            'Range Rover Evoque',
            'Renault',
            'Rolls Royce',
            'Seat',
            'Skoda',
            'Subaru',
            'Suzuki',
            'Tesla',
            'Toyota',
            'Van',
            'Vauxhall',
            'Volkswagen',
            'Volvo',

        ],

        /*______________________________________________________________________________________________________________
         *
         *                          SPECIALS AT MODEL NAME RELATED TO ALL MODELS
         *______________________________________________________________________________________________________________
         */
        'specials-at-name' => [
            /*-------------------------------------------
             * SHOULD BE REMOVED
             *-------------------------------------------
             */
            'SSR',
            'B F Goodrich',
            'Tyre',
            'Fitment',
            'LHD',

            /*-------------------------------------------------------
             *
             * General
             *-------------------------------------------------------
             *_______________________________________________________
             *
             * Asymmetric - symmetric
             */
            'Asymmetricetric',
            'Aysmmetric',
            'ASIMMETRICO',
            'Assimetrico',
            'ASYMMETRIC',
            'Asymmetric',
            'SYMMETRIC',
            'ASIMMETR',
            'asym',
            'asy',

            /*_____________________________________________________
             *
             * PLY
             */
            '6PR',
            '8PR',
            '10PR',
            '10 ply',
            '8 ply',
            '6 ply',
            '4x4',
            '4X4',

            /*_____________________________________________________
             *
             * Weather
             */
            'All Season',
            'ALL SEA',
            '4Seasons',
            '4Season',
            '4 Seasons',
            '4 Season',
            'Four Seasons',
            'Four Season',
            'Snow ',
            'Winter ',
            'Summer',

            /*_____________________________________________________
             *
             * Spare
             */
            'Spare/',
            'Spare',
            /*_____________________________________________________
             *
             * Color - sidewall and letters
             */
            ' OWL',
            'BSW',
            ' WW',
            'White Side Wall',
            'White Sidewall',
            'White Wall',
            'Black Wall',
            'Black Side Wall',

            /*_____________________________________________________
             *
             * Runflat
             */
            'Runflat',
            'Run Flat',
            'RFT',
            'HRS',

            /*_____________________________________________________
             *
             * Reinforced
             */
            'Reinforced',
            ' XL',
            ' ROF',
            'DSROF',
            'Extra Load',

            /*_____________________________________________________
             *
             * Others
             */
            'RHD',

            /*----------------------------------------------------
             * AUTO MANUFACTURERS
             *----------------------------------------------------
             *____________________________________________________
             *
             * Audi
             */
            'Audi A2 1.2 TDi',
            'AO2 (Audi A3)',
            'Audi RS7',
            'Audi RS6',
            'AO Audi Q7',
            'AO Audi A4 AS',
            'AO Audi Q3 Tiguan',
            'AO Audi A7',
            'AO Audi A6',
            'AO1 Audi A3',
            'AO2 Audi A3',
            'Audi Q7',
            'Audi A6',
            'Audi TT',
            'Audi S5',
            'Audi A4',
            'AO Audi',
            'RO1 Audi',
            'EZ Audi/',
            'EZ Audi',
            'AOE Audi',
            'RO2 Audi',
            'AUDI',
            'AOE',
            'RO1',
            'AO S8',
            'AO Q7',
            'AO (Q7)',
            'AO (AUD Q7',

            /*_____________________________________________________
             *
             * Alfa Romeo
             */
            'Alfa Romeo',
            'Alfa/',
            'Alfa',
            'Alf',

            /*_____________________________________________________
             *
             * Aston Martin
             */
            'AM9 Aston Martin',
            'AM4 Aston Martin',
            'Aston Martin',

            /*_____________________________________________________
             *
             * Bentley
             */
            'B BENTLEY',

            /*_____________________________________________________
             *
             * BMW
             */
            'BMW I8',
            'BMW',

            /*_____________________________________________________
             *
             * Citreon
             */
            'Citreon C3 & DS',
            '(Citreon C3) AS',
            'Citreon',

            /*_____________________________________________________
             *
             * Ferrari
             */
            'Ferrari F430',
            'Ferrari 575',
            'F FERRARI',
            'K1 FERRARI',

            /*_____________________________________________________
             *
             * Fiat
             */
            'FIAT TIPO',
            'Fiat 500L',
            'Fiat 500',
            'Fiat AS',
            'Fiat/',
            'Fiat',
            'Fia',

            /*_____________________________________________________
             *
             * Ford
             */
            'Ford Kuga',
            'FORD',

            /*_____________________________________________________
             *
             * Honda
             */
            'HO Honda',

            /*_____________________________________________________
             *
             * Hyundai
             */
            'Hyundai',

            /*_____________________________________________________
             *
             * Jaguar
             */
            'J Jaguar',
            'J (Jaguar)',
            'jlr',

            /*_____________________________________________________
             *
             * Jeep
             */
            'Jeep',

            /*_____________________________________________________
             *
             * Kia
             */
            'Kia',

            /*_____________________________________________________
             *
             * Lancia
             */
            'Lancia',

            /*_____________________________________________________
             *
             * Lexus
             */
            'Lexus',

            /*_____________________________________________________
             *
             * Maserati
             */
            'MGT Maserati',
            'Maserati',

            /*_____________________________________________________
             *
             * Mazda
             */
            'Mazda 6',
            'Mazda CX5',
            'Mazda',

            /*____________________________________________________
             *
             * Mercedes
             */
            'MOE (GLC \'15 WAR)',
            'MO (GLC AMG)',
            'MO C Class',
            'MO (C Class)',
            'MO S Class',
            'MO (S Class)',
            'MOE EXTENDED',
            'MO Mercedes',
            'MO GLK',
            'GLK',
            'MO EXT',
            'Mercedes MO',
            'Mercedes',
            'MOE',
            'MO1',

            /*____________________________________________________
             *
             * Nissan
             */
            'Nissan NV200',
            'Nissan Note',
            'Nissan Qashqai',
            'Nissan/Renault',
            'Nissan',
            '370z\'16',
            '370Z',

            /*_____________________________________________________
             *
             * Opel
             */
            'Opel',

            /*_____________________________________________________
             *
             * Peugeot
             */
            'Peugeot',

            /*_____________________________________________________
             *
             * Porsche
             */
            'N4 Porsche',
            'N3 Porsche',
            'N1 Porsche',
            'N2 Porsche',
            'N0 Porsche',
            'N0 Cayenne',
            'N0 Porsche Cayenne',
            'Cayenne',
            'CAYEN',

            /*_____________________________________________________
             *
             * Renault
             */
            'Renault Twizy',
            'Renault',

            /*_____________________________________________________
             *
             * Rolls Royce
             */
            'Rolls Royce',

            /*_____________________________________________________
             *
             * Seat
             */
            'Seat Ibiza Po',
            'Seat Ibiza',
            'Seat Leon AS',
            'Seat Ateca AS',
            'Seat Ateca',
            'Seat Leon',
            'Seat AS',
            'Seat',

            /*_____________________________________________________
             *
             * Skoda
             */
            'Skoda Rid',
            'Skoda',

            /*_____________________________________________________
             *
             * Subaru
             */
            'Subaru Forester',
            'Subaru Outback',
            'Legacy',
            'Subaru',

            /*_____________________________________________________
             *
             * Suzuki
             */
            'Baleno',

            /*_____________________________________________________
             *
             * Toyota
             */
            'Prius+',
            'Prius',
            'LCRUISER',
            'Avensis',
            'LCRUIS',
            'Toyota Land Cruiser',
            'LR Land Rover',
            'T0 Toyota',
            'Toyota CS',
            'Toyota',
            'YARIS FL',

            /*_____________________________________________________
             *
             * Vauxhall
             */
            'Vauxhall Astra',
            'Vauxhall MS',
            'Vauxhall',
            'Vaux',

            /*_____________________________________________________
             *
             * Volkswagen
             */
            'VW Volkswagen',
            'VW Caddy',
            'VW Polo',
            'VW Golf',
            'CADDY',
            'Golf',
            'POLO \'14',
            'POLO \'17',
            'POLO\'17',
            'POLO 14',
            'POLO 17',
            'POLO17',
            'Polo',
            'VW-',
            'VW1',
            'VW/',
            'VW',
            'Vauxhall Touareg MS',
            'PZ VW',
            'Touareg',
            'TOUAR',
            'tourag',
            'Volkswagen',
        ],
        /*______________________________________________________________________________________________________________
         *
         *                          SPECIALS AT MODEL NAME RELATED TO BRAND
         *______________________________________________________________________________________________________________
         */
        'specials-at-name-for-brand' => [

            /*-------------------------------------------------------
             *
             * Atlas
             *-------------------------------------------------------
             *_______________________________________________________
             *
             * General
             */
            'atlas' => [
                /*_____________________________________________________
                 *
                 * General
                 */
                'Van',
            ],

            /*-------------------------------------------------------
             *
             * Avon-Cooper
             *-------------------------------------------------------
             *_______________________________________________________
             *
             * General
             */
            'avon-cooper' => [
                /*_____________________________________________________
                 *
                 * General
                 */
                'Snow',
            ],

            /*-------------------------------------------------------
             *
             * BF GOODRICH
             *-------------------------------------------------------
             *_______________________________________________________
             *
             * General
             */
            'bf goodrich' => [
                /*_____________________________________________________
                 *
                 * General
                 */
                'winter',
            ],

            /*-------------------------------------------------------
             *
             * Bridgestone
             *-------------------------------------------------------
             *_______________________________________________________
             *
             * General
             */
            'bridgestone' => [
                /*_____________________________________________________
                 *
                 * Alfa Romeo
                 */
                'GUILIA',

                /*_____________________________________________________
                 *
                 * Aston Martin
                 */
                'AMA DB7',

                /*_____________________________________________________
                 *
                 * Audi
                 */
                'AO (LRR A3 \'16',
                'AO (Q2 \'16',
                'AO (AUD TT',
                'AO (A3 \'16',
                'AO (A4/5',
                'AO',
                'RO2',
                'R01',

                /*_____________________________________________________
                 *
                 * BMW
                 */
                'X3 \'17',
                'X3\'17',
                'X3',
                '3 SER\'12',
                '3 SER',
                '3 & 4SE',
                '2 SER',
                '3&4SE',

                /*_____________________________________________________
                 *
                 * Citreon
                 */
                'CIT & OPE',

                /*_____________________________________________________
                 *
                 * Fiat
                 */
                'punto',
                '500X',

                /*_____________________________________________________
                 *
                 * Infinity
                 */
                'Infinity',
                'Infiniti',

                /*_____________________________________________________
                 *
                 * Jeep
                 */
                'RENEGADE',

                /*_____________________________________________________
                 *
                 * Ferrari
                 */
                'Ferrari',
                'F458',

                /*_____________________________________________________
                 *
                 * Ford
                 */
                'Mondeo',
                'Galaxy',

                /*_____________________________________________________
                *
                * Honda
                */
                'S2000',
                'INSIG\'16',
                'HONDA',

                /*_____________________________________________________
                *
                * Lexus
                */
                'LEX LS',
                'LC500',

                /*_____________________________________________________
                 *
                 * Mazda
                 */
                'CX-3 \'15',
                'CX-3',
                'MX-5 \'15',
                'MX-5',

                /*_____________________________________________________
                 *
                 * Mercedes
                 */
                'MO',

                /*____________________________________________________
                 *
                 * Mitsubishi
                 */
                'MIT',
                'triton',

                /*_____________________________________________________
                 *
                 * Nissan
                 */
                'X-TRAI',
                'NIS MICRA',
                'NIS',

                /*_____________________________________________________
                 *
                 * Opel
                 */
                'ZAFIRA',

                /*_____________________________________________________
                 *
                 * Porsche
                 */
                'N1',
                'N0',
                ' Por',

                /*_____________________________________________________
                 *
                 * Renault
                 */
                'REN TALISM',

                /*_____________________________________________________
                 *
                 * Seat
                 */
                'ATECA\'16',

                /*_____________________________________________________
                 *
                 * Skoda
                 */
                'KAROQ\'17',

                /*_____________________________________________________
                 *
                 * Subaru
                 */
                'IMPREZA',

                /*_____________________________________________________
                 *
                 * Suzuki
                 */
                'Vitara',

                /*_____________________________________________________
                 *
                 * Toyota
                 */
                'CTRY \'16',
                'Yaris',

                /*_____________________________________________________
                 *
                 * Vauxhall
                 */
                'Astra',
                'Astra +',

                /*_____________________________________________________
                 *
                 * Volkswagen
                 */
                'T-ROC\'16',
                'Tiguan',

                /*_____________________________________________________
                 *
                 * Volvo
                 */
                'Volvo S60',
                'Volvo',
                'V40',
            ],

            /*-------------------------------------------------------
             *
             * Continental
             *-------------------------------------------------------
             *_______________________________________________________
             *
             * General
             */
            'continental' => [
                /*
                 * General
                 */
                'Temp',

                /*_____________________________________________________
                 *
                 * Alfa Romeo
                 */
                'AR',

                /*_____________________________________________________
                 *
                 * Audi
                 */
                'AO',
                'RO2',

                /*_____________________________________________________
                 *
                 * Mercedes
                 */
                'MO',
                'CLS Class',
                'AMG',
                'Label Update',

                /*_____________________________________________________
                 *
                 * Porsche
                 */
                'N1',
                'N0 Porsche',
                'N0',
                'N2 Porsche',
                'Porsche',
                'N2',

                /*_____________________________________________________
                 *
                 * Volvo
                 */
                'VOL',
            ],

            /*-------------------------------------------------------
             *
             * Dunlop
             *-------------------------------------------------------
             *_______________________________________________________
             *
             * General
             */
            'dunlop' => [

                /*_____________________________________________________
                 *
                 *  Runflat
                 *
                 */
                'DSST',

                /*_____________________________________________________
                 *
                 * Audi
                 */
                'AO ( A4 & A5)',
                'AO1 ( A3)',
                'AO (A6)',
                'AO (A4)',
                'AO ( - A7)',
                'AO ( - A6)',
                'AO (AU1)',
                'AO',
                'R01',

                /*_____________________________________________________
                 *
                 * Bentley
                 */
                'B (Bentley)',
                'B (Bentley Mulsanne',

                /*_____________________________________________________
                 *
                 * Jaguar
                 */
                'j (jaguar xj)',
                'j (jaguar xf)',

                /*_____________________________________________________
                 *
                 * Lexus
                 */
                'RX300',

                /*_____________________________________________________
                *
                * Mercedes
                */
                'MO (V-Class & Vito)',
                'MO (5 Series & E',
                'MO (CLS, E Class,',
                'MO (E Class)',
                'MO (E Class)',
                'MO (C43 AMG)',
                'MO (5 Ser',
                'CClass',
                'S Class',
                'C63 AMG',
                'MO',

                /*_____________________________________________________
                 *
                 * Porsche
                 */
                'N0 Porsche',
                'N0',
                'Porsche',
                'Porsche - 911',

                /*_____________________________________________________
                *
                * Skoda
                */
                'Fabia',
            ],

            /*-------------------------------------------------------
             *
             * Goodyear
             *-------------------------------------------------------
             *_______________________________________________________
             *
             * General
             */
            'goodyear' => [

                /*_____________________________________________________
                 *
                 * General
                 */
                'Van',
                'Winter',

                /*_____________________________________________________
                 *
                 * Audi
                 */
                'AO (Audi Q3',
                'AO1',
                'AO',

                /*_____________________________________________________
                 *
                 * Ford
                 */
                'Mondeo',
                'Transit',
                'Fiesta',
                'FO1',

                /*_____________________________________________________
                 *
                 * Jaguar
                 */
                'Jaguar F-Pace',
                'jaguar',

                /*_____________________________________________________
                 *
                 * Honda
                 */
                'Honda crv',

                /*_____________________________________________________
                 *
                 * Mercedes
                 */
                'S Class',
                'Merce',
                'MO',

                /*_____________________________________________________
                 *
                 * Renault
                 */
                'Re2',
                'Megane',
                'Kadjar',
                /*_____________________________________________________
                 *
                 * Porsche
                 */
                'N0 POR1',
                'N0 PO1',
                'PO1',
                'N0',
                'N1',
                'Porsche - 911',
                'Panamera',

                /*_____________________________________________________
                 *
                 * Tesla
                 */
                'Tesla Model X',

                /*_____________________________________________________
                 *
                 * Vauxhall
                 */
                'Astra',

                /*_____________________________________________________
                 *
                 * Volkswagen
                 */
                'Tiguan',
            ],

            /*-------------------------------------------------------
             *
             * Hankook
             *-------------------------------------------------------
             *_______________________________________________________
             *
             * General
             */
            'hankook' => [

                /*_____________________________________________________
                 *
                 * Ford
                 */
                'Fiesta',

                /*______________________________________________
                 *
                 * Porsche
                 */
                'N0',

                /*_____________________________________________________
                 *
                 * Volkswagen
                 */
                'Tiguan',
            ],

            /*-------------------------------------------------------
             *
             * Michelin
             *-------------------------------------------------------
             *_______________________________________________________
             *
             * General
             */
            'michelin' => [

                /*___________________________________________________
                 *
                 * General
                 */
                'Asymetrque',

                /*_____________________________________________________
                 *
                 * Audi
                 */
                'ao1',
                'ao',

                /*_____________________________________________________
                 *
                 * Ferrari
                 */
                'k1',

                /*_____________________________________________________
                 *
                 * Mercedes
                 */
                'MO',

                /*______________________________________________
                 *
                 * Porsche
                 */
                'N0',
                'N1',
                'N2',
                'N3',
                'N4',
            ],

            /*-------------------------------------------------------
             *
             * Pirelli
             *-------------------------------------------------------
             *_______________________________________________________
             *
             * General
             */
            'pirelli' => [

                /*_____________________________________________________
                 *
                 * General
                 */
                'Asim',
                'Assymetrico',
                'Winter',

                /*_____________________________________________________
                 *
                 * Aston Martin
                 */
                'AM9',
                'AM4',
                'vanquish',

                /*_____________________________________________________
                 *
                 * Audi
                 */
                'ro2 ao',
                'r01',
                'ro2',
                'ao (a4)',
                'ao (a6)',
                'ao',

                /*_____________________________________________________
                 *
                 * Bentley
                 */
                'B (Bentley',
                'B1 (Bentley GT)',
                'B1 (Bentley)',
                'B Bentley',
                'BL Bentley',

                /*_____________________________________________________
                 *
                 * Ferrari
                 */
                'Scaglietti',
                'F01 Ferrari',
                'Ferrari',
                'F01',
                'F02',
                'K2',
                'K1/',
                'K1',

                /*_____________________________________________________
                 *
                 * Ford
                 */
                'FO1',
                'FO2',

                /*_____________________________________________________
                 *
                 * Jaguar
                 */
                'Jaguar',
                'Jag',
                'jrs',

                /*_____________________________________________________
                 *
                 * Lamborghini
                 */
                'Lamborghini',

                /*_____________________________________________________
                 *
                 * Land Rover
                 */
                'Land Rover',
                'L/Rover',

                /*_____________________________________________________
                 *
                 * McLaren
                 */
                'McLaren',
                'MC1',
                'MC',

                /*_____________________________________________________
                 *
                 * Mercedes
                 */
                'MO (AMG or SL Class)',
                'MO',
                'Merc',

                /*_____________________________________________________
                 *
                 * Porsche
                 */
                'N3 (Porsche Carrera/Boxter)',
                'N2 Porsche',
                'N2',
                'N0 Porsche',
                'NO Porsche',
                'N0',
                'N1 Porsche',
                'Porsche',
                'N1',
                'N4',
                'N3',




                /*_____________________________________________________
                 *
                 * Volvo
                 */
                '/Volvo',
                'Volvo XC90',
                'Volvo'
            ],

            /*-------------------------------------------------------
             *
             * Toyo
             *-------------------------------------------------------
             *_______________________________________________________
             *
             * General
             */
            'toyo' => [
                /*_____________________________________________________
                *
                * Porsche
                */
                'Por'
            ],

            /*-------------------------------------------------------
             *
             * Vredestein
             *-------------------------------------------------------
             *_______________________________________________________
             *
             * General
             */
            'vredestein' => [
                /*_____________________________________________________
                 *
                 * General
                 */
                'Winter',
            ],

            /*-------------------------------------------------------
             *
             * Yokohama
             *-------------------------------------------------------
             *_______________________________________________________
             *
             * General
             */
            'yokohama' => [
                /*_____________________________________________________
                 *
                 * General
                 */
                'Winter',

                /*_____________________________________________________
                 *
                 * Audi
                 */
                'AO 99Y (A7)',
                'AO (S8)',
                'A7',
                'S8',
                'AO',

                /*_____________________________________________________
                 *
                 * Mercedes
                 */
                'MO',

                /*_____________________________________________________
                 *
                 * Porsche
                 */
                'N0 (CaymanBoxter)',
                'N2',
                'Porsche',
                'N0',
                'N1',
            ],
        ],

        /*______________________________________________________________________________________________________________
         *
         *                          SPECIALS STANDARDS
         *______________________________________________________________________________________________________________
         */
        'standards' => [
            '6 Ply' => [
                '6 ply', '6pr'
            ],
            '8 Ply' => [
                '8 ply', '8pr'
            ],
            '10 Ply' => [
                '10 ply', '10pr'
            ],
            'All Seasons' => [
                '4seasons',
                '4season',
                '4 seasons',
                '4 season',
                'four seasons',
                'four season',
                'all seasons',
                'all season',
                'all sea',
            ],
            'Aston Martin' => [
                'aston martin am9',
                'aston martin am2',
                'aston martin amr',
                'aston martin am8',
                'am9 aston martin',
                'am4 aston martin',
                'aston martin',
                'vanquish',
                'am9',
                'am4',
                'ama db7'
            ],
            'Alfa Romeo' => [
                'guilia',
                'alfa romeo',
                'alfa/',
                'alfa',
                'alf',
                'ar',
            ],
            'Asymmetric' => [
                'aysmmetric',
                'asymetrque',
                'asymmetric',
                'asymmetricetric',
                'asymmetr',
                'asimmetr',
                'asimmetrico',
                'asym',
                'asy',
                'asim',
                'assimetrico',
                'assymetrico',
                'asymmetrical',
            ],
            'Audi' => [
                'audi ro1',
                'ao (audi q3',
                'audi ao',
                'ao audi',
                'ro1 audi',
                'ro2 audi',
                'ez audi',
                'aoe audi',
                'ao1 audi a3',
                'ao2 audi a3',
                'audi tt',
                'aoe',
                'ao1',
                's8',
                'a7',
                'ao s8',
                'audi a4',
                'ao (aud Q7',
                'ao (q7)',
                'audi S5',
                'audi rs7',
                'audi q7',
                'ro1',
                'ao q7',
                'ao (a4)',
                'ao (a6)',
                'audi a2 1.2 tdi',
                'ao2 (audi a3)',
                'ao (a3 \'16',
                'ao (a4/5',
                'ao (aud tt',
                'ao (q2 \'16',
                'ao (lrr a3 \'16',
                'ao ( a4 & a5)',
                'aO1 ( a3)',
                'ro2 ao',
                'ro2',
                'r01',
                'ao',
                'ez audi/',
                'audi',
                'ao 99Y (a7)',
                'ao (s8)',
                'ao ( - a7)',
                'ao ( - a6)',
                'ao (au1)',
            ],
            'Bentley' => [
                'b bentley',
                'bl bentley',
                'b (bentley mulsanne',
                'b (bentley)',
                'b (bentley',
                'bentley',
                'b (bentley f/s & gt)',
                'b1 (bentley gt)',
                'b1 (bentley)',
            ],
            'Black Side Wall' => [
                'black side wall',
                'bsw',
                'black wall'
            ],
            'BMW' => [
                '3&4se',
                '3 & 4se',
                '2 ser',
                'bmw i8',
                'bmw',
                'x3 \'17',
                'x3\'17',
                'x3',
                '3 ser\'12',
                '3 ser',
            ],
            'Citreon' => [
                'citreon c3 & ds', 'citreon', '(citreon c3) as', 'cit & ope'
            ],
            'Ferrari' => [
                'ferrari k1',
                'scaglietti',
                'ferrari',
                'k1 ferrari',
                'f ferrari',
                'f01 ferrari',
                'f01',
                'f02',
                'k1',
                'k1/',
                'k2',
                'ferrari 575',
                'ferrari f430',
                'f458',
            ],
            'Fiat' => [
                'fiat 500l',
                'fiat 500',
                'fiat as',
                'fiat/',
                'fiat',
                'fia',
                'punto',
                '500x',
            ],
            'Ford' => [
                'ford kuga',
                'ford',
                'transit',
                'mondeo',
                'fo1',
                'fo2',
                'fiesta',
                'galaxy',
            ],
            'Honda' => [
                'ho honda', 'honda', 's2000', 'insig\'16', 'honda crv',
            ],
            'Hyundai' => [
                'hyundai'
            ],
            'Infinity' => [
                'infinity', 'infiniti'
            ],
            'Jaguar' => [
                'j jaguar',
                'j (jaguar)',
                'jaguar',
                'j (jaguar xj)',
                'j (jaguar xf)',
                'jaguar f-pace',
                'jlr',
                'jrs',
                'jag',
            ],
            'Jeep' => [
                'jeep', 'renegade'
            ],
            'Kia' => [
                'kia'
            ],
            'Land Rover' => [
                'lr land rover', 'land rover', 'l/rover',
            ],
            'Land Cruiser' => [
                'lcruiser', 'lcruis', 'toyota land cruiser',
            ],
            'Lexus' => [
                'lexus', 'lex ls', 'lc500', 'rx300'
            ],
            'Maserati' => [
                'maserati', 'mgt maserati',
            ],
            'Mazda' => [
                'mazda 6',
                'mazda cx5',
                'mazda',
                'cx-3 \'15',
                'cx-3',
                'mx-5 \'15',
                'mx-5',
            ],
            'McLaren' => [
                'mclaren', 'mc1', 'mc'
            ],
            'Mercedes' => [
                'mo (amg or sl class)',
                'mo (e class)',
                'cclass',
                's class',
                'mo (5 ser',
                'mo (5 series & e',
                'moe (glc \'15 war)',
                'mo (glc amg)',
                'mo (v-class & vito)',
                'mo (c43 amg)',
                'cls class',
                'label update',
                'mo mercedes',
                'mo (cls, e class,',
                'mercedes mo',
                'merce',
                'merc',
                'mercedes mo1',
                'moe extended',
                'mercedes',
                'moe',
                'c63 amg',
                'mo c class',
                'mo (c class)',
                'mo s class',
                'mo (s class)',
                'mo ext',
                'mo1',
                'mo glk',
                'glk',
                'amg',
                'mo',
            ],
            'Mitsubishi' => [
                'mit', 'triton',
            ],
            'Nissan' => [
                'nissan nv200',
                'nissan note',
                'nissan qashqai',
                'nissan/renault',
                'nissan',
                'x-trai',
                '370z\'16',
                '370z',
                'nis',
                'nis micra',
            ],
            'Off White Letter' => [
                'owl'
            ],
            'Opel' => [
                'zafira', 'opel',
            ],
            'Peugeot' => [
                'peugeot'
            ],
            'Porsche' => [
                'porsche',
                'panamera',
                'n0 po1',
                'n0 (caymanboxter)',
                'porsche n4',
                'porsche n3',
                'porsche n1',
                'porsche n2',
                'porsche n0',
                'n4 porsche',
                'n3 porsche',
                'n1 porsche',
                'n2 porsche',
                'n0 porsche',
                'no porsche',
                'n0 cayenne',
                'porsche - 911',
                'n0 porsche cayenne',
                'n3 (porsche carrera/boxter)',
                'n0 por1',
                'n1',
                'n3',
                'n0',
                'n2',
                'n4',
                'po1',
                'cayen',
                'cayenne',
                'por',
            ],
            'Reinforced' => [
                'xl', 'reinforced', 'extra load'
            ],
            'Right Hand Drive' => [
                'rhd'
            ],
            'Rolls Royce' => [
                'rolls royce',
            ],
            'Runflat' => [
                'rft', 'runflat', 'rof', 'dsrof', 'ssr', 'run flat', 'hrs', 'dsst',
            ],
            'Seat' => [
                'seat ibiza po',
                'ateca\'16',
                'seat ibiza',
                'seat leon as',
                'seat ateca as',
                'seat ateca',
                'seat leon',
                'seat as',
                'seat',
            ],
            'Skoda' => [
                'skoda rid',
                'skoda',
                'karoq\'17',
                'fabia',
            ],
            'Spare' => [
                'spare/',
                'spare',
            ],
            'Subaru' => [
                'subaru forester',
                'subaru outback',
                'subaru',
                'impreza',
                'legacy',
            ],
            'Suzuki' => [
                'baleno', 'suzuki', 'vitara',
            ],
            'Symmetric' => [
                'symmetric'
            ],
            'Temp' => [
                'temp'
            ],
            'Tesla' => [
                'tesla model x',
            ],
            'Toyota' => [
                't0 toyota', 'avensis', 'yaris fl', 'prius+', 'prius', 'toyota cs', 'ctry \'16', 'yaris',
            ],
            'Renault' => [
                'renault twizy', 'renault', 'ren talism', 'megane', 'kadjar', 're2',
            ],
            'Van' => [
                'van'
            ],
            'Vauxhall' => [
                'vauxhall astra', 'vaux', 'vauxhall ms', 'vauxhall', 'astra', 'astra +'
            ],
            'Volkswagen' => [
                'vw volkswagen',
                'vw polo',
                'vw caddy',
                'vw golf',
                'golf',
                'caddy',
                'vw',
                'vw1',
                'vw-',
                'vw/',
                'polo 14',
                'polo \'14',
                'polo 17',
                'polo \'17',
                'polo\'17',
                'polo17',
                'polo',
                'vauxhall touareg ms',
                'touareg',
                'touar',
                'tiguan',
                'volkswagen',
                'pz vw',
                't-roc\'16',
            ],
            'Volvo' => [
                'volvo', 'v40', 'vol', '/volvo', 'volvo xc90', 'volvo s60',
            ],
            'Winter' => [
                'winter',
                'snow',
                'snow master'
            ],
            'White Side Wall' => [
                'white side wall',
                'white sidewall',
                'white side',
                'white wall',
                'ww'
            ],
            '4x4' => [
                '4x4', '4X4'
            ],

        ]
    ]
];
