<?php

return [

    /*
    |--------------------------------------------------------------------------
    | INDEX FILE NAMES
    |--------------------------------------------------------------------------
    |
    | Here you may specify the file names for storage the indexes.
    | 'type' => [
    |     'index type' => 'file name'
    | ],
    |
    */
    'properties' => [
        \App\Models\Property::PROFILE => 'width-profiles.json',
        \App\Models\Property::RIM_SIZE => 'width-profile-rimSizes.json',
        \App\Models\Property::LOAD_INDEX => 'width-profiles-rimSize-loadIndexes.json',
        \App\Models\Property::SPEED_RATING => 'width-profiles-rimSize-loadIndex-speedRatings.json'
    ],

];