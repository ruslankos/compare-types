<?php

return [

    'tyre-search' => [
        'list' => [
            'by-price-asc' => 'Price low to high',
            'by-price-desc' => 'Price high to low',
            'by-model-rating-desc' => 'Best rating',
            'by-alphabet' => 'Sort by A-Z'
        ],
    ],
];
