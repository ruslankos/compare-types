<?php

return [
    'schema' => [
        'by-price-asc' => [
            'sorter' => \App\Services\Tyre\Tyre\Sorters\Items\ByPrice::class,
         ],
        'by-price-desc' => [
            'sorter' => \App\Services\Tyre\Tyre\Sorters\Items\ByPrice::class,
            'direction' => \App\Utiles\Sorters\Collection\SorterInterface::DESCENDING
         ],
        'by-alphabet' => [
            'sorter' => \App\Services\Tyre\Tyre\Sorters\Items\ByAlphabetic::class,
         ],
        'by-model-rating-desc' => [
            'sorter' => \App\Services\Tyre\Model\Sorters\Items\ByModelRating::class,
            'direction' => \App\Utiles\Sorters\Collection\SorterInterface::DESCENDING
         ],
    ]
];
