<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Filters Build Schema
    |--------------------------------------------------------------------------
    |
    | Here you may specify the aliases and components of the filters.
    | The filters will be used in order they specified at config file
    |
    | 'schema' => [
    |     'alias' => [
    |         'components' => [
    |              'component alias' => Component class name
    |          ],
    |          'filter' => filter ClassName
    |     ],
    |],
    */

    'schema' => [
        'search-by-filters' => [
            'components' => [
                'group' => \App\Services\Tyre\Search\Filters\Items\ByGroupFilter::class,
                'brands' => \App\Services\Tyre\Search\Filters\Items\ByBrandsFilter::class,
                'specials' => \App\Services\Tyre\Search\Filters\Items\BySpecialsFilter::class,
                'fitting' => \App\Services\Tyre\Search\Filters\Items\ByPriceFitting::class,
                'model' => \App\Services\Tyre\Search\Filters\Items\ByModelName::class,
            ],
            'filter' => \App\Utiles\Filters\Filter::class,
        ],
        'price' => [
            'components' => [
                \App\Models\Price\Fitting::ALIAS =>
                    \App\Services\Price\Price\Filters\Filters\ByFittingFilter::class
            ],
            'filter' => \App\Utiles\Filters\Filter::class,
        ],
    ],





    'display' => [
        'specials' => [
            ['alias' => 'winter', 'name' => 'Winter'],
            ['alias' => 'reinforced', 'name' => 'Extra Load / XL'],
            ['alias' => 'runflat', 'name' => 'Runflat']
        ],
    ],
];