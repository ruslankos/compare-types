<?php

return [
    /*__________________________________________________________________________________________________________________
     *
     *                                           SCENARIOS
     *------------------------------------------------------------------------------------------------------------------
     *
     * Here you can declare the list of scenarios
     *__________________________________________________________________________________________________________________
     */

    'scenarios' => [
        'visited.tyre.sizes.saver',
        'selected.size.saver',
        'selected.fitting.saver',
        'tyre.sizes.converter',
        'tyre.sizes.decoder'
    ],

    /*__________________________________________________________________________________________________________________
     *
     *                                           CONVERTERS SERVICES CONFIGS
     *------------------------------------------------------------------------------------------------------------------
     *
     * Here you can declare the args that should be given to the configurable elements
     *__________________________________________________________________________________________________________________
     */

    'configs' => [

    ],


    /*__________________________________________________________________________________________________________________
     *
     *                                      SCENARIOS DECLARATION SCHEMA
     *------------------------------------------------------------------------------------------------------------------
     *
     * Here you can declare the building schema of components according the scenario
     *
     * Example.
     * 'scenario name' => [
     *     'base' => Name of class that should be built,
     *      --------------------------------------------
     *      * Here you should declare the components which is needed to build the base class
     *      * Each component should have the type property, if type resolve - you should declare the
     *      * the name of the components class. If component is also consist from some components you should declare
     *      * name of scenario for its building and put the type of component to build.
     *      * If your component need some scalar properties it should implements Configurable Interface
     *      * and properties should be declared at configs section of this file.
     *      * To inject this properties into class you have to declare config property with alias to
     *      * the necessary config list
     *     'components' => [
     *          'name of the class if type resolve' => [
     *              'type' => 'resolve',
     *              'config' => 'alias to the necessary configs declared at config section'
     *          ],
     *          'scenario if class should be build from components' => [
     *              'type => 'build',
     *          ]
     *     ]
     * ]
     *__________________________________________________________________________________________________________________
     */
    'schema' => [
        /*__________________________________________________________________________
         *
         * Cookie storage services
         *__________________________________________________________________________
         */
        'selected.size.saver' => [
            'base' => \App\Services\Tyre\Tyre\Storage\Cookie\SelectedSizeSaver::class,
            'components' => [
                \App\Utiles\Cookie\CookieServiceInterface::class => [
                    'type' => 'resolve'
                ],
                'tyre.sizes.converter' => [
                    'type' => 'build'
                ],
            ],
        ],
        'selected.fitting.saver' => [
            'base' => \App\Services\Price\Fitting\Storage\Cookie\SelectedFittingSaver::class,
            'components' => [
                \App\Utiles\Cookie\CookieServiceInterface::class => [
                    'type' => 'resolve'
                ],
                \App\Helpers\Encrypters\TwoSideEncrypter::class => [
                    'type' => 'resolve'
                ],
            ],
        ],

        'visited.tyre.sizes.saver' => [
            'base' => \App\Services\Tyre\Tyre\Storage\Cookie\TyreSizeSaver::class,
            'components' => [
                \App\Utiles\Cookie\CookieServiceInterface::class => [
                    'type' => 'resolve'
                ],
                'tyre.sizes.converter' => [
                    'type' => 'build'
                ],
                \App\Services\Tyre\Tyre\Storage\Cookie\Transformers\PropertiesForCookie::class => [
                    'type' => 'resolve'
                ],
            ],
        ],

        'tyre.sizes.converter' => [
            'base' => \App\Utiles\Converter\Converter::class,
            'components' => [
                \App\Utiles\Converter\Transformers\EncryptedJsonTransformer::class => [
                    'type' => 'resolve'
                ],
                \App\Services\Tyre\Tyre\Storage\Cookie\Encoders\TyreSizeEncoder::class => [
                    'type' => 'resolve'
                ],
                'tyre.sizes.decoder' => [
                    'type' => 'build'
                ],
            ]
        ],
        'tyre.sizes.decoder' => [
            'base' => \App\Services\Tyre\Tyre\Storage\Cookie\Decoders\TyreSizeDecoder::class,
            'components' => [
                \App\Services\Tyre\Property\Search\PropertiesByValues::class => [
                    'type' => 'resolve'
                ],
            ]
        ],
    ],
];
