<?php

return [

    'transformers' => [
        \App\Services\Shared\Link\Transformers\Skimlinks\CustomTransformer::class => [
            /*
            |--------------------------------------------------------------------------
            | Skimlinks Api Key
            |--------------------------------------------------------------------------
            |
            | This value is the api key for skimlinks.com api. This value is used when the
            | outgoing links need to be transformed for Skimlinks service usage.
            |
            */
            'key' => env('CUSTOM_SKIMLINKS_SECRET'),

            /*
            |--------------------------------------------------------------------------
            | Skimlinks Domain
            |--------------------------------------------------------------------------
            |
            | This value is the domain, that registered for Skimlinks service.
            |
            */
            'domain' => env('CUSTOM_SKIMLINKS_SUBDOMAIN'),

            /*
            |--------------------------------------------------------------------------
            | Skimlinks XS
            |--------------------------------------------------------------------------
            |
            | This value is used for link transformation as part of it.
            |
            */
            'xs' => env('CUSTOM_SKIMLINKS_XS', 1),
        ],
        \App\Services\Shared\Link\Transformers\Skimlinks\ViglinkTransformer::class => [
            /*
            |--------------------------------------------------------------------------
            | Viglink Api Key
            |--------------------------------------------------------------------------
            |
            | This value is the api key for viglink api. This value is used when the
            | outgoing links need to be transformed for Viglink service usage.
            |
            */
            'key' => env('VIGLINK_SKIMLINKS_SECRET'),

            /*
            |--------------------------------------------------------------------------
            | Viglink Domain
            |--------------------------------------------------------------------------
            |
            | This value is the domain, that registered for Viglink service.
            |
            */
            'domain' => env('VIGLINK_SKIMLINKS_SUBDOMAIN'),
        ],
    ],

    'schema' => [
        \App\Utiles\Skimlinks\Skimlinks::DEFAULT =>
            \App\Services\Shared\Link\Transformers\Skimlinks\CustomTransformer::class,
        'viglink' => \App\Services\Shared\Link\Transformers\Skimlinks\ViglinkTransformer::class,
    ],
];
