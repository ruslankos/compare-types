<?php

return [
    /*
     |__________________________________________________________________________________________________________________
     |
     |                          OBJECTS SECTION
     |__________________________________________________________________________________________________________________
     |
     | Here you may specify the objects and builders for them by some alias.
     | NB! If you want declare composite object, you should specify composite of which object it is
     |
     | 'objects' => [
     |      'alias' => [
     |          'builder' => Builder class name
     |          'object' => Object class name
     |      ]
     |]
     |
     | NB! For composite objects
     |
     | 'objects' => [
     |      'alias' => [
     |          'composite' => Composite builder class name
     |          'builder' => object alias from this section
     |          'object' => Composite object class name
     |      ]
     |]
     */
    'objects' => [
        'address' => [
            'builder' => \App\Services\Location\Address\Import\Assemblers\AddressBuilder::class,
            'object' => \App\Services\Location\Address\Import\Objects\AddressObject::class,
        ],
        'brand'  => [
            'builder' => \App\Services\Tyre\Brand\Brand\Import\General\Assemblers\BrandBuilder::class,
            'object' => \App\Services\Tyre\Brand\Brand\Import\General\Objects\BrandObject::class,
        ],
        'brand-image' => [
            'builder' => \App\Services\Tyre\Brand\Brand\Import\Image\Assemblers\BrandBuilder::class,
            'object' => \App\Services\Tyre\Brand\Brand\Import\Image\Objects\Brand::class
        ],
        'county' => [
            'builder' => \App\Services\Location\County\Import\Assemblers\CountyBuilder::class,
            'object' => \App\Services\Location\County\Import\Objects\CountyObject::class,
        ],
        'description' => [
            'builder' => \App\Services\Shared\Description\Import\Assemblers\DescriptionBuilder::class,
            'object' => \App\Services\Shared\Description\Import\Objects\DescriptionObject::class
        ],
        'fitting-entity' => [
            'builder' => \App\Services\Price\Fitting\Repository\Assemblers\FittingAssembler::class,
            'object' => \App\Services\Price\Fitting\Repository\DTO\Fitting::class
        ],
        'footer-column' => [
            'builder' => \App\Services\Tyre\Model\DataCollectors\Assemblers\FooterColumnAssembler::class,
            'object' => \App\Services\Tyre\Model\DataCollectors\Objects\FooterColumn::class
        ],
        'image' => [
            'builder' => \App\Services\Shared\Images\Import\Assemblers\ImageBuilder::class,
            'object' => \App\Services\Shared\Images\Import\Objects\Image::class,
        ],
        'brand-image-import' => [
            'builder' => \App\Services\Shared\Images\Import\Assemblers\BrandImageBuilder::class,
            'object' => \App\Services\Shared\Images\Import\Objects\BrandImport::class,
        ],
        'link' => [
            'builder' => \App\Services\Shared\Link\Import\Assemblers\LinkBuilder::class,
            'object' => \App\Services\Shared\Link\Import\Objects\LinkObject::class,
        ],
        'location' => [
            'builder' => \App\Services\Location\Import\Assemblers\LocationBuilder::class,
            'object' => \App\Services\Location\Import\Objects\LocationObject::class,
        ],
        'model' => [
            'builder' => \App\Services\Tyre\Model\Import\Model\Assemblers\ModelBuilder::class,
            'object' => \App\Services\Tyre\Model\Import\Model\Objects\Model::class,
        ],
        'model-description' => [
            'builder' => \App\Services\Tyre\Model\Import\Description\Assemblers\ModelDescriptionBuilder::class,
            'object' => \App\Services\Tyre\Model\Import\Description\Objects\ModelDescription::class,
        ],
        'model-review' => [
            'builder' => \App\Services\Tyre\Model\Import\Reviews\Assemblers\ModelReviewBuilder::class,
            'object' => \App\Services\Tyre\Model\Import\Reviews\Objects\ModelReview::class,
        ],
        'model-rating' => [
            'builder' => \App\Services\Tyre\Model\Import\Rating\Assemblers\ModelRatingBuilder::class,
            'object' => \App\Services\Tyre\Model\Import\Rating\Objects\ModelRatingCSV::class,
        ],
        'model-for-rating' => [
            'builder' => \App\Services\Tyre\Model\Import\Rating\Assemblers\ModelAssembler::class,
            'object' => \App\Services\Tyre\Model\Import\Rating\Objects\Model::class,
        ],
        'rating-from-csv' => [
            'builder' => \App\Services\Tyre\Model\Import\Rating\Assemblers\RatingAssembler::class,
            'object' => \App\Services\Shared\Rating\Import\Objects\RatingObject::class,
        ],
        'model-image' => [
            'builder' => \App\Services\Tyre\Model\Import\Image\Assemblers\ModelBuilder::class,
            'object' => \App\Services\Tyre\Model\Import\Image\Objects\Model::class,
        ],
        'model-image-import' => [
            'builder' => \App\Services\Shared\Images\Import\Assemblers\ModelImageBuilder::class,
            'object' => \App\Services\Shared\Images\Import\Objects\ModelImage::class,
        ],
        'price.with.voucher' => [
            'builder' => \App\Services\Price\Price\Transformers\Assemblers\PriceWithVoucherAssembler::class,
            'object' => \App\Services\Price\Price\Transformers\DTO\PriceWithVoucher::class
        ],
        'rating' => [
            'builder' => \App\Services\Shared\Rating\Import\Assemblers\RatingBuilder::class,
            'object' => \App\Services\Shared\Rating\Import\Objects\RatingObject::class,
        ],
        'region' => [
            'builder' => \App\Services\Location\Region\Import\Assemblers\RegionBuilder::class,
            'object' => \App\Services\Location\Region\Import\Objects\RegionObject::class,
        ],
        'review' => [
            'builder' => \App\Services\Shared\Reviews\Import\Assemblers\ReviewBuilder::class,
            'object' => \App\Services\Shared\Reviews\Import\Objects\ReviewObject::class,
        ],
        'reviews' => [
            'composite' => \App\Services\Shared\Reviews\Import\Assemblers\ReviewsBuilder::class,
            'builder' => 'review',
            'object' => \App\Utiles\DTO\Samples\CompositeObject::class,
        ],
        'snapshot-object' => [
            'builder' => \App\Utiles\Snapshot\Importers\Assemblers\SnapshotBuilder::class,
            'object' => \App\Utiles\Snapshot\Importers\Objects\SnapshotObject::class,
        ],
        'search-by-size' => [
            'builder' => \App\Helpers\DTO\Builders\Tyre\Search\SearchBySizeAssembler::class,
            'object' => \App\Helpers\DTO\Objects\Tyre\Search\SearchBySize::class,
        ],
        'sort-select' => [
            'builder' => \App\Services\Tyre\Tyre\Search\Templates\Sorting\Assembler::class,
            'object' => \App\Services\Tyre\Tyre\Search\Templates\Sorting\SortDropdown::class,
        ],
        'synonyms-map' => [
            'builder' => \App\Services\Tyre\Model\Synonyms\Synonyms\Import\Assemblers\SynonymMapBuilder::class,
            'object' => \App\Services\Tyre\Model\Synonyms\Synonyms\Import\Objects\SynonymsMap::class,
        ],
        'town' => [
            'builder' => \App\Services\Location\Town\Import\Assemblers\TownBuilder::class,
            'object' => \App\Services\Location\Town\Import\Objects\TownObject::class,
        ],
        'voucher.models.group' => [
            'builder' => \App\Helpers\DTO\Builders\VoucherModelsGroupBuilder::class,
            'object' => \App\Helpers\DTO\Objects\Voucher\ModelsGroup::class,
        ],
    ],
];
