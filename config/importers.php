<?php

return [
    /*
     |__________________________________________________________________________________________________________________
     |
     |                          DATA IMPORTING
     |__________________________________________________________________________________________________________________
     |
     | Here declared the configs for data import component work.
     | You may specify:
     | - Scenario
     | - Importers build schema according to scenario
     | - Preparators configs
     | - List of objects, that should be used for import
     |
     */

    /*
    |--------------------------------------------------------------------------
    | Scenario choosing
    |--------------------------------------------------------------------------
    |
    | Here you may specify which scenario should be taken depending on the type and target of import
    |
    | 'scenario' => [
    |    'target' => [
    |        'type' => Scenario name,
    |    ],
    |],
    */
    'scenario' => [
        'models' => [
            'reviews' => [
                'txt' => 'model-review',
                'json' => 'model-review',
            ],
            'rating' => [
                'csv' => 'model-rating',
            ],
            'descriptions' => [
                'txt' => 'model-description',
                'json' => 'model-description'
            ],
            'images' => [
                'zip' => 'model-images',
            ],
        ],
        'brands' => [
            'images' => [
                'zip' => 'brand-image',
            ],
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Importers Build Schema
    |--------------------------------------------------------------------------
    |
    | Here you may specify from which components importer will be built for specific scenario.
    |
    | 'schema' => [
    |    'scenario' => [
    |        'handler' => Handler class name,
    |       'importer' => Importer class name,
    |       'uploader' => Uploader class name,
    |       'preparator' => Preparator class name,
    |    ],
    |],
    */
    'schema' => [
        'brand-group-map' => [
            'handler' => \App\Utiles\Files\Uploaders\Handler::class,
            'importer' => \App\Services\Tyre\Brand\Group\Import\BrandGroupImporter::class,
            'uploader' => \App\Utiles\Files\Uploaders\Uploaders\JSONUploader::class,
            'preparator' => \App\Utiles\Files\Uploaders\Preparators\Preparator::class,
        ],
        'brand-image' => [
            'handler' => \App\Utiles\Files\Uploaders\Handler::class,
            'importer' => \App\Services\Tyre\Brand\Brand\Import\Image\BrandImageImporter::class,
            'uploader' => \App\Utiles\Files\Uploaders\Uploaders\ZipUploader::class,
            'preparator' => \App\Utiles\Files\Uploaders\Preparators\Preparator::class,
        ],
        'filter-synonyms-map' => [
            'handler' => \App\Utiles\Files\Uploaders\Handler::class,
            'importer' => \App\Services\Tyre\Model\Synonyms\Filters\Import\FilterSynonymsImporter::class,
            'uploader' => \App\Utiles\Files\Uploaders\Uploaders\JSONUploader::class,
            'preparator' => \App\Utiles\Files\Uploaders\Preparators\Preparator::class,
        ],
        'location' => [
            'handler' => \App\Utiles\Files\Uploaders\Handler::class,
            'importer' => \App\Services\Location\Import\LocationImporter::class,
            'uploader' => \App\Utiles\Files\Uploaders\Uploaders\JSONUploader::class,
            'preparator' => \App\Utiles\Files\Uploaders\Preparators\Preparator::class,
        ],
        'model-images' => [
            'handler' => \App\Utiles\Files\Uploaders\Handler::class,
            'importer' => \App\Services\Tyre\Model\Import\Image\ModelImageImporter::class,
            'uploader' => \App\Utiles\Files\Uploaders\Uploaders\ZipUploader::class,
            'preparator' => \App\Utiles\Files\Uploaders\Preparators\Preparator::class,
        ],
        'model-description' => [
            'handler' => \App\Utiles\Files\Uploaders\Handler::class,
            'importer' => \App\Services\Tyre\Model\Import\Description\ModelDescriptionImporter::class,
            'uploader' => \App\Utiles\Files\Uploaders\Uploaders\JSONUploader::class,
            'preparator' => \App\Utiles\Files\Uploaders\Preparators\Preparator::class,
        ],
        'model-review' => [
            'handler' => \App\Utiles\Files\Uploaders\Handler::class,
            'importer' => \App\Services\Tyre\Model\Import\Reviews\ModelReviewsImporter::class,
            'uploader' => \App\Utiles\Files\Uploaders\Uploaders\JSONUploader::class,
            'preparator' => \App\Utiles\Files\Uploaders\Preparators\Preparator::class,
        ],
        'model-rating' => [
            'handler' => \App\Utiles\Files\Uploaders\Handler::class,
            'importer' => \App\Services\Tyre\Model\Import\Rating\ModelRatingFromCSVImporter::class,
            'uploader' => \App\Utiles\Files\Uploaders\Uploaders\CSVUploader::class,
            'preparator' => \App\Utiles\Files\Uploaders\Preparators\Preparator::class,
        ],
        'synonyms-map' => [
            'handler' => \App\Utiles\Files\Uploaders\Handler::class,
            'importer' => \App\Services\Tyre\Model\Synonyms\Synonyms\Import\SynonymsImporter::class,
            'uploader' => \App\Utiles\Files\Uploaders\Uploaders\JSONUploader::class,
            'preparator' => \App\Utiles\Files\Uploaders\Preparators\Preparator::class,
        ],

    ],

    /*
     |__________________________________________________________________________________________________________________
     |
     |                          PREPARATORS SECTION
     |__________________________________________________________________________________________________________________
     |
     |
     */
    'preparators' => [


        /*
         |--------------------------------------------------------------------------
         |                          Building Schema
         |--------------------------------------------------------------------------
         |
         | Here you may specify the components that need for specific preparator building.
         | If you class doesn't have own schema - default settings will be used
         |
         | 'schema' => [
         |    'Preparator class name' => [
         |       'structure' => Structure service class name,
         |       'builder' => Object builder factory name
         |    ],
         |],
         */
        'schema' => [
            'default' => [
                'structure' => \App\Utiles\Transformers\Arrays\Structure\Service::class,
                'builder' => \App\Services\Uploaders\ObjectBuilders\Factory::class,
                'validator' => \App\Utiles\Validators\Builder::class,
            ],
            \App\Utiles\Files\Uploaders\Preparators\Preparator::class => [
                'structure' => \App\Utiles\Transformers\Arrays\Structure\Service::class,
                'builder' => \App\Utiles\DTO\Factory::class,
                'validator' => \App\Utiles\Validators\Builder::class,
            ],
        ],

        /*
         |--------------------------------------------------------------------------
         |                          Object list
         |--------------------------------------------------------------------------
         |
         | Here you may specify objects, that should be used for specific scenario. There should be
         | Import object mentioned and object items. Objects should be declared by alias, that should be
         | registered at OBJECT section below.
         |
         | 'objects' => [
         |    'scenario name' => [
         |       'import' => Import object alias,
         |       'items' => [object alias, object2 alias]
         |    ],
         |],
         */

        'objects' => [
            'model-review' => [
                'import' => 'model-review',
                'items' => [
                    'brand', 'model', 'reviews', 'rating', 'link'
                ]
            ],
            'model-rating' => [
                'import' => 'model-rating',
                'items' => [
                    'model-for-rating', 'rating-from-csv',
                ]
            ],
            'model-description' => [
                'import' => 'model-description',
                'items' => [
                    'brand', 'model', 'description'
                ]
            ],
            'model-images' => [
                'import' => 'model-image-import',
                'items' => ['brand-image', 'model-image', 'image']
            ],
            'location' => [
                'import' => 'location',
                'items' => [
                    'region', 'county', 'town', 'address',
                ]
            ],
            'synonyms-map' => [
                'import' => 'synonyms-map',
                'items' => []
            ],
            'brand-group-map' => [
                'import' => 'snapshot-object',
                'items' => []
            ],
            'brand-image' => [
                'import' => 'brand-image-import',
                'items' => ['brand-image', 'image']
            ],

            'filter-synonyms-map' => [
                'import' => 'snapshot-object',
                'items' => []
            ],
            'footer-column' => [
                'import' => 'footer-column',
            ],
            'voucher.models.group' => [
                'import' => 'voucher.models.group',
            ],
            'price.with.voucher' => [
                'import' => 'price.with.voucher',
            ],
        ],


        /*
        |--------------------------------------------------------------------------
        |                          Data structure
        |--------------------------------------------------------------------------
        |
        | Here you may specify the transforming of the obtained data structure, into given standard for each scenario.
        | Structure transformers, should be specified by alias, that declared at data-transforming config file.
        | If you not configure data-structure for some scenario it will be left at native state
        |
        | 'structure' => [
        |    'scenario name' => [
        |       'structure alias' => ['Describing of the structure according configs/data-transforming.php file']
        |    ],
        |],
        */
        'structure' => [
            'model-review' => [
                'iterated' => [
                    'reviews' => [
                        'Review_Text_#num' => 'review',
                        'Car_Make_#num' => 'car',
                        'Car_Model_#num' => 'model',
                    ]
                ],
                'structure' => [
                    'URL' => [
                        'alias' => 'link'
                    ],
                    'Tyre_Brand' => [
                        'alias' => 'brand',
                    ],
                    'Tyre_Model' => [
                        'alias' => "model",
                    ],
                    'Rating' => [
                        'alias' => 'rating',
                    ],
                    'Number_of_Ratings' => [
                        'alias' => 'number',
                    ],
                    'reviews' => [
                        'alias' => 'reviews',
                    ],
                ],

            ],
            'model-rating' => [
                'structure' => [
                    'Title' => [
                        'alias' => 'model',
                    ],
                    'Rating' => [
                        'alias' => 'rating',
                    ],
                    '# of Reviews' => [
                        'alias' => 'number',
                    ],
                ],

            ],

            'model-description' => [
                'structure' => [
                    'brand' => [
                        'alias' => 'brand',
                    ],
                    'tyre' => [
                        'alias' => "model",
                    ],
                    'description' => [
                        'alias' => 'description',
                    ],
                ],
            ],
            'location' => [
                'structure' => [
                    'regionName' => [
                        'alias' => 'region'
                    ],
                    'countyName' => [
                        'alias' => 'county'
                    ],
                    'townName' => [
                        'alias' => 'town'
                    ],
                    'writtenText' => [
                        'alias' => 'text',
                    ],
                    'address' => [
                        'alias' => 'address',
                        'nested' => [
                            'postCode' => [
                                'alias' => 'postCode'
                            ],
                            'streetAddress' => [
                                'alias' => 'street',
                            ],
                            'addressCountry' => [
                                'alias' => 'country',
                            ],
                        ],
                    ],
                ],
            ],
            'synonyms-map' => [
                'structure' => [
                    'parent' => [
                        'alias' => 'parent'
                    ],
                    'child' => [
                        'alias' => 'child'
                    ],
                ],
            ],
            'brand-group-map' => [
                'structure' => [
                    'group' => [
                        'alias' => 'group'
                    ],
                    'brands' => [
                        'alias' => 'brands'
                    ],
                ],
            ],
            'brand-image' => [
                'structure' => [
                    'name' => [
                        'alias' => 'name'
                    ],
                    'path' => [
                        'alias' => 'path'
                    ],
                ],
            ],
            'model-images' => [
                'structure' => [
                    'name' => [
                        'alias' => 'name'
                    ],
                    'path' => [
                        'alias' => 'path'
                    ],
                ],
            ],
            'filter-synonyms-map' => [
                'structure' => [
                    'brand' => [
                        'alias' => 'brand'
                    ],
                    'filters' => [
                        'alias' => 'filters'
                    ],
                ],
            ],
        ],

        /*
        |--------------------------------------------------------------------------
        |                          Validation
        |--------------------------------------------------------------------------
        |
        | Here you may specify the list of array indexes that must be present at preparing data.
        | NB! Validation take place after data structure transforming, so validation should be
        | configure for proper data structure
        |
        | 'validation' => [
        |    'scenario name' => 'validator alias declared at validators.php config file',
        |],
        */
        'validation' => [
            'default' => 'default',
            'model-review' => 'model-review',
            'model-rating' => 'model-rating',
            'model-description' => 'model-description',
            'location' => 'location',
            'synonyms-map' => 'synonyms-map',
            'brand-group-map' => 'brand-group-map',
            'filter-synonyms-map' => 'filter-synonyms-map',
            'brand-image' => 'image.upload',
            'model-images' => 'image.upload',
        ],

    ],
];




