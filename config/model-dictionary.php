<?php


return [

    /*__________________________________________________________________________________________________________________
     |
     |                                      General
     |------------------------------------------------------------------------------------------------------------------
     | Here you may specify the standard names of models, that should change the incorrect model names
     |  [
     |      'general' => [
     |           'standard' => 'incorrect', or ['incorrect', 'incorrect2']
     |  ]
     |__________________________________________________________________________________________________________________
     */
    'general' => [
        'Geolander' => ['geolandar', 'geoloandar'],
        'Geolander A/T' => 'Geolander A T',
        'Geolander H/T' => 'Geolander H T',
        'BlueEarth' => 'BluEarthA ',
        'Econo Drive' => 'Econodrive',
        'Fast Response' => 'Fastresponse',
        'Street Response' => 'StreetResponse',
        'PHIR' => 'PHI R',
        'PHI2' => 'PHI 2',
        'SnowControl' => 'Snow Control',
        'WeatherMaster Van' => 'WMVAN',
        'A/T ' => ['All Terrain', 'ALLTERRAIN', 'All-Terrain', 'A/T'],
        ' A/T' => ['/AT'],
        'M/T ' => ['Mudterrain', 'Mud Terrain', 'Mud-Terrain', 'M/T'],
        'H/T ' => ['Highway Terrain', 'H/T'],
        'WDrive' => 'W drive',
        'Ice Blazer WST2' => 'Ice Blazer WS T2',
        'Ice Blazer WSL2' => 'Ice Blazer WS L2',
        'RHP780' => 'RHP 780',
        'RHP780P' => 'RHP780 P',
        'P Zero' => 'PZero',
        'SnowG' => 'Snow G',
        'N Fera' => 'NFERA',
        'N Blue' => 'NBLUE',
        'IcePlus' => 'Ice Plus',
        'VAN01' => 'VAN 01',
        'Dynapro' => 'Dynpro',
    ],

    /*__________________________________________________________________________________________________________________
     |
     |                                      Brand dependent model names
     |------------------------------------------------------------------------------------------------------------------
     | Here you may specify the standard names of models, that should change the incorrect model names
     |  [
     |      'brand' => [
     |           'standard' => 'incorrect', or ['incorrect', 'incorrect2']
     |  ]
     |__________________________________________________________________________________________________________________
     */

    'brand-dependent' => [
        /*_____________________________________________________________________
         *
         * Avon
         *_____________________________________________________________________
         */
        'avon-cooper' => [
            '' => ['avon', 'cooper'],
            'AV11' => ['copper av11', 'Avanza AV11',],
            'AV10' => ['copper av10', 'Avanza AV10',],
            'AV9' => ['copper av9', 'Avanza AV9',],
            'WeatherMaster Van' => ['WMVAN', 'WeatherMasterVan',],
            'WeatherMaster ' => ['WM'],
            'CR11B' => ['RR CR11B', 'ACR 11B'],
        ],

        /*_____________________________________________________________________
         *
         * DB Goodrich
         *_____________________________________________________________________
         */
        'bf goodrich' => [
            '' => ['suv',],
        ],

        /*_____________________________________________________________________
         *
         * Bridgestone
         *_____________________________________________________________________
         */
        'bridgestone' => [
            '' => ['ext'],
            'B250' => ['b 250 ecopia', 'b 250'],
            'ER300A' => ['Turanza ER300A Ecopia', 'Turanza ER300A'],
            'ER300' => ['Turanza ER300'],
            'RE050' => ['Potenza RE050'],
            'Alenza' => ['Alenze'],
            'Alenza 001' => ['Alenza1', 'Alenza 1'],
            'Turanza ER300A' => ['ER300A'],
            'Turanza ER300' => ['ER300'],
            'Potenza RE050' => ['RE050'],
            'H/P Sport' => ['hp sport'],
            'EL42' => ['EL 42', 'EL 40002'],
        ],

        /*_____________________________________________________________________
         *
         * Continental
         *_____________________________________________________________________
         */
        'continental' => [
            '' => ['conti', 'suv', 'extended'],

        ],

        /*_____________________________________________________________________
         *
         * Continental
         *_____________________________________________________________________
         */
        'dunlop' => [
            '' => ['nst', 'mfs', 'Espace', 'suv'],

        ],

        /*________________________________________________________
         *
         * Falkan
         *_______________________________________________________
         */
        'falkan' => [
            'Landair' => ['Land air', 'LA']
        ],

        /*________________________________________________________
         *
         * Firestone
         *_______________________________________________________
         */
        'firestone' => [
            'TZ300a' => ['TZ300α'],
        ],

        /*________________________________________________________
         *
         * Goodyear
         *_______________________________________________________
         */
         'goodyear' => [
             '' => ['suv', 'fit'],
         ],

        /*________________________________________________________
         *
         * Hankook
         *_______________________________________________________
         */
        'hankook' => [
            '' => ['suv'],
        ],

        /*________________________________________________________
         *
         * Pirelli
         *_______________________________________________________
         */
        'pirelli' => [
            'Pirelli Scorpion' => 'Pirelli SCORPION WINTER',
        ],

        /*_____________________________________________________________________
         *
         * Yokohama
         *_____________________________________________________________________
         */
        'yokohama' => [
            '' => ['ZPS'],

        ],
    ],

    'with-exceptions' => [
        /*_____________________________________________________________________
         *
         * Bridgestone
         *_____________________________________________________________________
         */
        'avon-cooper' => [
            'Supervan AV4' => [
                'change' => 'AV4',
                'except' => ['Supervan AV4'],
            ],
            'Turbospeed CR27' => [
                'change' => 'CR27',
                'except' => ['Turbospeed CR27'],
            ],
            'Turbospeed CR227' => [
                'change' => 'CR227',
                'except' => ['Turbospeed CR227'],
            ],
            'Potenza S001' => [
                'change' => 'S001',
                'except' => ['Potenza S001'],
            ],
            'H/T ' => [
                'change' => 'HT',
                'except' => ['HTT'],
            ],
        ],
        /*_____________________________________________________________________
         *
         * Bridgestone
         *_____________________________________________________________________
         */
        'bridgestone' => [
            'Ecopia' => [
                'change' => 'eco',
                'except' => ['Ecopia']
            ],
            'Turanza T001' => [
                'change' => 'T001',
                'except' => ['Turanza T001']
            ],

        ],

        /*_____________________________________________________________________
         *
         * Continental
         *_____________________________________________________________________
         */
        'continental' => [
            'Silent' => [
                'change' => 'sile',
                'except' => ['Silent']
            ],
        ],

        /*________________________________________________________
         *
         * Goodyear
         *_______________________________________________________
         */
        'goodyear' => [
            'Performance' => [
                'change' => 'perf',
                'except' => ['Performance']
            ]
        ],
    ],
];
