<?php

return [

    /*
     |__________________________________________________________________________________________________________________
     |
     |                          DATA TRANSFORMERS
     |__________________________________________________________________________________________________________________
     |
     | Here declared the configs for data structure transformers.
     | They consist from basic transformer - that should be a composite of transformer items.
     | And items list with their aliases, that will be used for basic transformer filling.
     | NB! Specify the config for each type of the declared item here or at class itself.
     | NB!!! When you use data structure transformers, please be accurate and use the 'structure' transformer
     | as the LAST ONE, because of it specific properties and for proper usage, please, CONFIGURE transformers CORRECTLY
     |
     */

    'transformers' => [
        'basic' => \App\Utiles\Transformers\Arrays\Structure\Transformer::class,
        'items' => [
            'iterated' => \App\Utiles\Transformers\Arrays\Structure\Transformers\IteratedIndex::class,
            'structure' => \App\Utiles\Transformers\Arrays\Structure\Transformers\DataStructureStandard::class,
        ],
    ]
];