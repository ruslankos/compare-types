<?php

return [

    /*__________________________________________________________________________________________
     *
     *                                           VALIDATORS LIST
     *__________________________________________________________________________________________
     */
    'validators' => [
        'basic' => \App\Utiles\Validators\Validator::class,
        'required' => \App\Utiles\Validators\Validators\CheckRequiredIndexes::class,
        'valid-values' => \App\Utiles\Validators\Validators\CheckIfInvalidValues::class,
        'parsing-price' => \App\Services\Parsing\Parser\Preparator\Validators\PriceValidator::class,
        'parsing-brand' => \App\Services\Parsing\Parser\Preparator\Validators\BrandValidator::class,
        'price.group.display.order.content.type' =>
            \App\Services\Price\Fitting\Storage\Validatation\Validators\DisplayOrderContentTypeChecker::class
    ],

    /*______________________________________________________________________________________________________________
     *
     *                                           SCHEMA
     *______________________________________________________________________________________________________________
     *
     *  'schema' => [
     *      'alias' => [
     *          'validator' => 'Alias of the base composite validator'
     *          'items' => [
     *              [
     *                  'name' => 'alias of the validator item',
     *                  'configs' => ['Any scalar data in form of array that should be set into validator item']
     *              ]
     *          ],
     *      ],
     * ],
     *
     *--------------------------------------------------------------------------------------------------------------
     */
    'schema' => [
        'default' => [
            'validator' => 'basic',
            'items' => [],
        ],

        /*______________________________________________________________________________________________________________
         *
         *                                           Parsing
         *______________________________________________________________________________________________________________
         *
         *
         *--------------------------------------------------------------------------------------------------------------
         */
        'parsing' => [
            'validator' => 'basic',
            'items' => [
                [
                    'name' => 'required',
                    'configs' => [
                        'manufacturer',
                        'model',
                        'url',
                        'width',
                        'profile',
                        'rim_size',
                        'speed_rating',
                        'prices',
                        'load_index'
                    ],
                ],
                [
                    'name' => 'valid-values',
                    'configs' => [
                        'indexes' => [
                            'manufacturer',
                            'model',
                            'url',
                            'width',
                            'profile',
                            'rim_size',
                            'speed_rating',
                            'prices',
                            'load_index'
                        ],
                        'invalid' => ['0.0000']
                    ]
                ],
                [
                    'name' => 'valid-values',
                    'configs' => [
                        'indexes' => ['width', 'profile', 'rim_size', 'speed_rating', 'load_index'],
                        'invalid' => ['Van']
                    ]
                ],
                [
                    'name' => 'parsing-price',
                ],
                [
                    'name' => 'parsing-brand',
                ],

            ]
        ],
        /*______________________________________________________________________________________________________________
         *
         *                                           Model context
         *______________________________________________________________________________________________________________
         *
         *
         *--------------------------------------------------------------------------------------------------------------
         */
        'model-review' => [
            'validator' => 'basic',
            'items' => [
                [
                    'name' => 'required',
                    'configs' => ['link', 'brand', 'model', 'rating']
                ]
            ]
        ],
        'model-rating' => [
            'validator' => 'basic',
            'items' => [
                [
                    'name' => 'required',
                    'configs' => ['model', 'rating']
                ]
            ]
        ],
        'model-description' => [
            'validator' => 'basic',
            'items' => [
                [
                    'name' => 'required',
                    'configs' =>  ['brand', 'model', 'description'],
                ]
            ]
        ],
        'synonyms-map' => [
            'validator' => 'basic',
            'items' => [
                [
                    'name' => 'required',
                    'configs' =>  ['parent', 'child'],
                ]
            ]
        ],
        'filter-synonyms-map' => [
            'validator' => 'basic',
            'items' => [
                [
                    'name' => 'required',
                    'configs' =>  ['brand', 'filters'],
                ]
            ]
        ],

        /*______________________________________________________________________________________________________________
         *
         *                                           Location
         *______________________________________________________________________________________________________________
         *
         *
         *--------------------------------------------------------------------------------------------------------------
         */
        'location' => [
            'validator' => 'basic',
            'items' => [
                [
                    'name' => 'required',
                    'configs' =>  ['region', 'county', 'town'],
                ]
            ]
        ],

        /*______________________________________________________________________________________________________________
         *
         *                                           Brand
         *______________________________________________________________________________________________________________
         *
         *
         *--------------------------------------------------------------------------------------------------------------
         */
        'brand-group-map' => [
            'validator' => 'basic',
            'items' => [
                [
                    'name' => 'required',
                    'configs' =>  ['group', 'brands'],
                ]
            ]
        ],

        /*______________________________________________________________________________________________________________
         *
         *                                           Images
         *______________________________________________________________________________________________________________
         *
         *
         *--------------------------------------------------------------------------------------------------------------
         */
        'image.upload' => [
            'validator' => 'basic',
            'items' => [
                [
                    'name' => 'required',
                    'configs' =>  ['name', 'path'],
                ]
            ]
        ],

        /*______________________________________________________________________________________________________________
         *
         *                                           Prices
         *______________________________________________________________________________________________________________
         *
         *
         *--------------------------------------------------------------------------------------------------------------
         */
        'price.group.display.order.update' => [
            'validator' => 'basic',
            'items' => [
                [
                    'name' => 'required',
                    'configs' =>  ['id', 'order'],
                ],
                [
                    'name' => 'price.group.display.order.content.type'
                ],
            ]
        ]
    ],
];
