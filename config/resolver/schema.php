<?php

return [

    /*
     * 'schema' => ['object' => 'class']
     */
    'schema' => [

        /*______________________________________________________________________________________________________________
         |
         |                                          TYRES CONTEXT
         |______________________________________________________________________________________________________________
         |
         |                                         Tyre module
         |--------------------------------------------------------------------------------------------------------------
         */
        \App\Commands\Tyre\Tyre\FindTyreByIdCommand::class =>
            \App\Services\Tyre\Tyre\Handlers\GetTyreByIdHandler::class,

       /*_______________________________________________________________________________________________________________
        |
        |                                         Model module
        |---------------------------------------------------------------------------------------------------------------
        */
        \App\Helpers\DTO\Objects\Tyre\Model\FindModelByBrandIdAndAlias::class =>
            \App\Services\Tyre\Model\Handlers\FindModelByBrandIdAndAliasHandler::class,
        \App\Helpers\DTO\Objects\Tyre\Model\StoreModelData::class =>
            \App\Services\Tyre\Model\Handlers\StoreModelDataHandler::class,
        \App\Helpers\DTO\Objects\Tyre\Model\FindModelsByBrandId::class =>
            \App\Services\Tyre\Model\Handlers\FindModelsByBrandIdHandler::class,
        \App\Commands\Tyre\Model\FindTopModelsForMainPageCommand::class =>
            \App\Services\Tyre\Model\Handlers\FindTopModelsForMainPageHandler::class,

       /*_______________________________________________________________________________________________________________
        |
        |                                         Brand module
        |---------------------------------------------------------------------------------------------------------------
        */
        \App\Helpers\DTO\Objects\Tyre\Brand\StoreBrandData::class =>
            \App\Services\Tyre\Brand\Brand\Handlers\StoreBrandDataHandler::class,
        \App\Commands\Tyre\Brand\FindBrandsForMainPageCommand::class =>
            \App\Services\Tyre\Brand\Brand\Handlers\FindBrandsForMainPageHandler::class,

       /*_______________________________________________________________________________________________________________
        |
        |                                         Property module
        |---------------------------------------------------------------------------------------------------------------
        */
        \App\Helpers\DTO\Objects\Tyre\Properties\FindSelectedSizes::class =>
            \App\Services\Tyre\Property\Handlers\FindSelectedSizeHandler::class,

       /*_______________________________________________________________________________________________________________
        |
        |                                         Search module
        |---------------------------------------------------------------------------------------------------------------
        */
        \App\Helpers\DTO\Objects\Tyre\Search\SearchModelByName::class =>
            \App\Services\Tyre\Search\Handlers\SearchModelByNameHandler::class,
        \App\Helpers\DTO\Objects\Tyre\Search\SearchBySize::class =>
            \App\Services\Tyre\Search\Handlers\BySizeSearcherHandler::class,
        \App\Helpers\DTO\Objects\Tyre\Search\SearchByVRM::class =>
            \App\Services\Tyre\Search\Handlers\SearchTyreByVRMHandler::class,
        \App\Helpers\DTO\Objects\Tyre\Search\FindTyresForAutocomplete::class =>
            \App\Services\Tyre\Search\Handlers\FindTyresForAutocompleteHandler::class,
        \App\Helpers\DTO\Objects\Tyre\Search\FindFiltersForSearchResult::class =>
            \App\Services\Tyre\Search\Handlers\FIndFilterForSearchResultHandler::class,
        /*______________________________________________________________________________________________________________
         |
         |                                          PRICES CONTEXT
         |______________________________________________________________________________________________________________
         |
         |                                         Prices module
         |--------------------------------------------------------------------------------------------------------------
         */
       \App\Commands\Price\Price\FindPricesByTyreIdAndVouchersCommand::class =>
            \App\Services\Price\Price\Handlers\FindPricesByTyreIdHandler::class,
        \App\Commands\Price\Price\FilterPricesByFittingCommand::class =>
            \App\Services\Price\Price\Handlers\FilterPriceByFittingHandler::class,
        \App\Commands\Price\Price\FindBestPricesCommand::class =>
            \App\Services\Price\Price\Handlers\FindBestPricesHandler::class,
        \App\Helpers\DTO\Objects\Prices\Prices\FindPricesBySiteId::class =>
            \App\Services\Price\Price\Handlers\FindPricesBySiteIdHandler::class,

       /*_______________________________________________________________________________________________________________
        |
        |                                         Fitting module
        |---------------------------------------------------------------------------------------------------------------
        */
        \App\Helpers\DTO\Objects\Prices\Fitting\FindSelectedFitting::class =>
            \App\Services\Price\Fitting\Handlers\FindSelectedFittingHandler::class,


        /*______________________________________________________________________________________________________________
         |
         |                                          SHARED CONTEXT
         |______________________________________________________________________________________________________________
         |
         |                                         Link module
         |--------------------------------------------------------------------------------------------------------------
         */
        \App\Helpers\DTO\Objects\Shared\Link\FindSkimlink::class =>
            \App\Services\Shared\Link\Handlers\FindSkimlinkHandler::class,
        \App\Commands\Shared\Link\FindSkimlinksList::class =>
            \App\Services\Shared\Link\Handlers\FindSkimlinksListHandler::class,

        /*______________________________________________________________________________________________________________
         |
         |                                          SITES CONTEXT
         |______________________________________________________________________________________________________________
         |
         |                                         Site module
         |--------------------------------------------------------------------------------------------------------------
         */
        \App\Helpers\DTO\Objects\Site\StoreSiteData::class =>
            \App\Services\Site\Handlers\StoreSiteDataHandler::class,
        \App\Commands\Site\SetSkimlinksType::class =>
            \App\Services\Site\Handlers\SetSkimlinksTypeHandler::class,
        \App\Commands\Site\FindSiteForMainPageCommand::class =>
            \App\Services\Site\Handlers\FindSiteForMainPageHandler::class,

        /*______________________________________________________________________________________________________________
         |
         |                                          CARS CONTEXT
         |______________________________________________________________________________________________________________
         |
         |                                          Import module
         |--------------------------------------------------------------------------------------------------------------
         */
        \App\Commands\Cars\Import\ImportCarsDataCommand::class =>
            \App\Services\Car\Import\Handlers\ImportCarsDataHandler::class,

    ],
];
