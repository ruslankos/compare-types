<?php

return [

    /*
     | Here declared default properties with the relations declared at tyre entity.
     | Order of the properties is very important for the work of several services, that used for
     | Caching of properties combinations, properties selectors etc
     |
     */

    'default' => [
        \App\Models\Property::WIDTH => \App\Models\Tyre::WIDTH,
        \App\Models\Property::PROFILE => \App\Models\Tyre::PROFILE,
        \App\Models\Property::RIM_SIZE => \App\Models\Tyre::RIM_SIZE,
        \App\Models\Property::LOAD_INDEX => \App\Models\Tyre::LOAD_INDEX,
        \App\Models\Property::SPEED_RATING => \App\Models\Tyre::SPEED_RATING,
    ],

    'schema' => [
        \App\Models\Property::WIDTH => [
            \App\Models\Property::PROFILE => [
                \App\Models\Property::RIM_SIZE => [
                    \App\Models\Property::LOAD_INDEX => \App\Models\Property::SPEED_RATING
                ]
            ]
        ]
    ],

    'pre-selected-sizes' => [
        \App\Models\Property::WIDTH => '195',
        \App\Models\Property::PROFILE => '65',
        \App\Models\Property::RIM_SIZE => 'R15',
    ]
];
