<?php

return [
    /*
     |--------------------------------------------------------------------------
     |                          ROLES GROUPS
     |--------------------------------------------------------------------------
     |
     | Here you may specify the roles combined by groups
     |
     | 'group' => [
     |       'role 1', 'role 2' ...
     |    ],
     |],
     */
    'editors' => [
        \App\Models\Role::SUPER_ADMIN,
        \App\Models\Role::ADMIN,
        \App\Models\Role::EDITOR,
    ],

    'admins' => [
        \App\Models\Role::SUPER_ADMIN,
        \App\Models\Role::ADMIN,
    ]
];