<?php

return [
    /*
    |--------------------------------------------------------------------------
    |                           VEHICLE DATA
    |--------------------------------------------------------------------------
    |
    | Here declared the configs for connection to the vehicledata.co.uk API
    |
    */

    /*
    |--------------------------------------------------------------------------
    | API secret key
    |--------------------------------------------------------------------------
    |
    | This value is the api key that will be used for connection with Vehicle Data API.
    |
    */
    'key' =>  env('VEHICLE_DATA_KEY'),

    /*
    |--------------------------------------------------------------------------
    | Package id
    |--------------------------------------------------------------------------
    |
    | This value is the package id that will be used for connection with Vehicle Data API.
    |
    */
    'package_id' =>  env('VEHICLE_DATA_PACKAGE_ID', 'TyreData'),

    /*
    |--------------------------------------------------------------------------
    | Url
    |--------------------------------------------------------------------------
    |
    | This value is the url that will be used for connection with Vehicle Data API.
    |
    */
    'url' =>  env('VEHICLE_DATA_URL', 'https://uk1.ukvehicledata.co.uk/api/datapackage/'),
];