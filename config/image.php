<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Image size
    |--------------------------------------------------------------------------
    |
    | Here you may specify the images size. Two types of images are available:
    | general and thumb. You should declare width and height in pixels for both types
    |
    */
    'general' => [
        'width' => 350,
        'height' => 500
    ],

    'thumb' => [
        'width' => 175,
        'height' => 250
    ],

    'parsing' => [
        'relations' => [
            \App\Models\Tyre::class => \App\Models\Tyre::IMAGES,
        ],
        'disk' => [
            \App\Models\Tyre::class => 'tyres'
        ],
        'path' => [
            \App\Models\Tyre::class => 'storage/images/tyres/',
        ],
    ],

    'thumbs' => [
        \App\Models\Tyre::class => [
            'thumb' => \App\Models\Tyre::IMAGE_THUMBNAIL,
            'image' => \App\Models\Tyre::IMAGE,
        ],
    ],

    'store-path' => [
        \App\Models\Brand::class => 'public/images/logos/brands',
        \App\Models\Site::class => 'public/images/logos/sites',
        \App\Models\Special::class => 'public/images/logos/specials',
        \App\Models\TyreModel::class => 'public/images/logos/models',
    ],

    'import' => [
        'store-path' => [
            \App\Models\Brand::class => 'public/images/logos/brands',
            \App\Models\TyreModel::class => 'public/images/logos/models',
        ],
    ],
];
