<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Decorators Build Schema
    |--------------------------------------------------------------------------
    |
    | Here you may specify the aliases and components of the decorators.
    | The decorators will be used in order they specified at config file
    |
    | 'schema' => [
    |     'alias' => [
    |          'basic' => Composite decorator class name,
    |          'items' => [
    |              'Decorator 1', 'Decorator 2'
    |          ],
    |     ],
    |],
    |
    | NB! If you want to use simple decorator and not composite. Just put it on the
    | basic index and don't use items index
    | 'schema' => [
    |     'alias' => [
    |          'basic' => Decorator class name,
    |     ],
    |],
    */
    'schema' => [
        'tyre' => [
            'basic' => \App\Utiles\Mutators\Mutator::class,
            'items' => [
                \App\Services\Tyre\Tyre\Mutators\TyrePriceInfo::class,
                \App\Services\Tyre\Tyre\Mutators\SelectedQuantity::class,

            ],
        ],
        'tyre-view' => [
            'basic' => \App\Utiles\Mutators\Mutator::class,
            'items' => [
                \App\Services\Tyre\Tyre\Mutators\SelectedQuantity::class,
                \App\Services\Tyre\Tyre\Mutators\AddRelatedTyres::class,
            ],
        ],

        'model' => [
            'basic' => \App\Utiles\Mutators\Mutator::class,
            'items' => [
                \App\Services\Tyre\Model\Mutators\ModelActiveTyres::class,
                \App\Services\Tyre\Model\Mutators\ModelPriceRange::class,
                \App\Services\Tyre\Model\Mutators\ModelOfferCount::class,
                \App\Services\Tyre\Model\Mutators\LastChecked::class,
                \App\Services\Tyre\Model\Mutators\ModelSizes::class,
            ]
        ],
        'link' => [
            'basic' => \App\Utiles\Mutators\Mutator::class,
            'items' => [
                \App\Services\Shared\Link\Mutators\TargetSite::class
            ]
        ],
    ]
];
