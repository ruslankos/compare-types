<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Sitemap generators list
    |--------------------------------------------------------------------------
    |
    | Here you may specify the sitemap generation schema according to the composite pattern.
    |
    | 'schema' => [
    |     'alias' => [
    |         'base' => Composite generator class name,
    |         'items' => [generator item classname list]
    |     ],
    |],
    */
    'schema' => [
        'default' => [
            'base' => App\Utiles\Sitemaps\Generators\Generator::class,
            'items' => [
                App\Utiles\Sitemaps\Generators\Items\MainRoutes::class,
                App\Utiles\Sitemaps\Generators\Items\Entities\TyreModel::class,
                App\Utiles\Sitemaps\Generators\Items\Entities\Brand::class,
                App\Utiles\Sitemaps\Generators\Items\Entities\Site::class,
            ],
        ],
        'location' => [
            'base' => App\Utiles\Sitemaps\Generators\Generator::class,
            'items' => [
                \App\Services\Location\Region\Sitemaps\Generator::class,
                \App\Services\Location\County\Sitemaps\Generator::class,
                \App\Services\Location\Town\Sitemaps\Generator::class,
            ],
        ],
        'site' => [
            'base' => App\Utiles\Sitemaps\Generators\Generator::class,
            'items' => [
                \App\Services\Site\Sitemaps\Generator::class
            ],
        ]
    ]
];
