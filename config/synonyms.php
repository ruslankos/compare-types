<?php

return [
    'model-relations' => [
        \App\Models\TyreModel::REAL_TYRES,
        sprintf('%s.%s', \App\Models\TyreModel::REAL_TYRES, \App\Models\Tyre::REAL_PRICES),
        \App\Models\TyreModel::CHILDREN,
        sprintf('%s.%s', \App\Models\TyreModel::CHILDREN, \App\Models\TyreModel::REAL_TYRES),
        sprintf(
            '%s.%s.%s',
            \App\Models\TyreModel::CHILDREN,
            \App\Models\TyreModel::REAL_TYRES,
            \App\Models\Tyre::REAL_PRICES
        ),
    ],

    'children-relations' => [
        \App\Models\TyreModel::CHILDREN,
    ]
];
