<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Decorators Build Schema
    |--------------------------------------------------------------------------
    |
    | Here you may specify the aliases and components of the decorators.
    | The decorators will be used in order they specified at config file
    |
    | 'schema' => [
    |     'alias' => [
    |          'basic' => Composite decorator class name,
    |          'items' => [
    |              'Decorator 1', 'Decorator 2'
    |          ],
    |     ],
    |],
    |
    | NB! If you want to use simple decorator and not composite. Just put it on the
    | basic index and don't use items index
    | 'schema' => [
    |     'alias' => [
    |          'basic' => Decorator class name,
    |     ],
    |],
    */
    'schema' => [
        'tyre' => [
            'basic' => \App\Services\ModelDecorators\Decorator::class,
            'items' => [
                \App\Services\ModelDecorators\Items\Tyre\TyrePriceInfo::class,
            ],
        ],
        'model' => [
            'basic' => \App\Services\ModelDecorators\Decorator::class,
            'items' => [
                \App\Services\ModelDecorators\Items\Model\ModelActiveTyres::class,
                \App\Services\ModelDecorators\Items\Model\ModelPriceRange::class,
                \App\Services\ModelDecorators\Items\Model\ModelOfferCount::class,
                \App\Services\ModelDecorators\Items\Model\LastChecked::class,
            ]
        ],
        'link' => [
            'basic' => \App\Services\ModelDecorators\Decorator::class,
            'items' => [
                \App\Services\ModelDecorators\Items\Link\TargetSite::class,
            ]
        ],
    ]
];
