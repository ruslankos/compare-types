<?php

return [

    'uploaders' => [
        \App\Services\Uploaders\Uploaders\JSONUploader::class => [
            'upload_path' => 'public/upload/json',
            'storage_path' => 'app/public/upload/json',
        ],
        \App\Services\Uploaders\Uploaders\ZipUploader::class => [
            'upload_path' => 'public/upload/zip',
            'storage_path' => 'app/public/upload/zip',
            'export_path' => storage_path('app/public/upload/temp'),
            'extensions' => ['jpg', 'jpeg', 'png']
        ],
        \App\Utiles\Files\Uploaders\Uploaders\JSONUploader::class => [
            'upload_path' => 'public/upload/json',
            'storage_path' => 'app/public/upload/json',
        ],
        \App\Utiles\Files\Uploaders\Uploaders\CSVUploader::class => [
            'upload_path' => 'public/upload/csv',
            'storage_path' => 'app/public/upload/csv',
        ],
        \App\Utiles\Files\Uploaders\Uploaders\ZipUploader::class => [
            'upload_path' => 'public/upload/zip',
            'storage_path' => 'app/public/upload/zip',
            'export_path' => storage_path('app/public/upload/temp'),
            'extensions' => ['jpg', 'jpeg', 'png']
        ]
    ]
];
