<?php

return [

    /*__________________________________________________________________________________________________________________
     *
     *                                           SCENARIOS
     *------------------------------------------------------------------------------------------------------------------
     *
     * Here you can declare the list of scenarios
     *__________________________________________________________________________________________________________________
     */

    'scenarios' => [
        'brand.groups',
        'brand.group.snapshot.exporter',
        'brand.group.snapshot.refresher',
        'brand.group.snapshot.saver',
        'brand.group.snapshot.converter',
        'filters.snapshot',
        'filters.snapshot.saver',
        'filters.snapshot.converter',
        'filters.snapshot.refresher',
        'filters.snapshot.exporter',
        'synonyms.snapshot',
        'synonym.snapshot.exporter',
        'synonym.snapshot.saver',
        'synonym.snapshot.refresher',
        'synonyms.snapshot.converter',
    ],

    /*__________________________________________________________________________________________________________________
     *
     *                                           SNAPSHOT SERVICES CONFIGS
     *------------------------------------------------------------------------------------------------------------------
     *
     * Here you can declare the args that should be given to the configurable elements
     *__________________________________________________________________________________________________________________
     */

    'configs' => [
        'brand.group' => [
            'file' => 'brand_group_map.txt',
            'disk' => 'snapshots',
        ],
        'brand.group.snapshot.import' => [
            'scenario' => 'brand-group-map'
        ],
        'filters.snapshot' => [
            'file' => 'synonyms-filters-map.txt',
            'disk' => 'snapshots',
        ],
        'filters.snapshot.import' => [
            'scenario' => 'filter-synonyms-map'
        ],
        'synonyms.snapshot' => [
            'file' => 'synonyms_map.txt',
            'disk' => 'snapshots',
        ],
        'synonyms.snapshot.import' => [
            'scenario' => 'synonyms-map'
        ],
    ],


    /*__________________________________________________________________________________________________________________
     *
     *                                      SCENARIOS DECLARATION SCHEMA
     *------------------------------------------------------------------------------------------------------------------
     *
     * Here you can declare the building schema of components according the scenario
     *
     * Example.
     * 'scenario name' => [
     *     'base' => Name of class that should be built,
     *      --------------------------------------------
     *      * Here you should declare the components which is needed to build the base class
     *      * Each component should have the type property, if type resolve - you should declare the
     *      * the name of the components class. If component is also consist from some components you should declare
     *      * name of scenario for its building and put the type of component to build.
     *      * If your component need some scalar properties it should implements Configurable Interface
     *      * and properties should be declared at configs section of this file.
     *      * To inject this properties into class you have to declare config property with alias to
     *      * the necessary config list
     *     'components' => [
     *          'name of the class if type resolve' => [
     *              'type' => 'resolve',
     *              'config' => 'alias to the necessary configs declared at config section'
     *          ],
     *          'scenario if class should be build from components' => [
     *              'type => 'build',
     *          ]
     *     ]
     * ]
     *__________________________________________________________________________________________________________________
     */
    'schema' => [
        /*__________________________________________________________________________
         *
         * Model synonyms snapshot scenarios
         *__________________________________________________________________________
         */
        'brand.groups' => [
            'base' => \App\Utiles\Snapshot\SnapshotService::class,
            'components' => [
                \App\Services\Tyre\Brand\Group\Snapshot\Cleaner::class => [
                    'type' => 'resolve'
                ],
                'brand.group.snapshot.saver' => [
                    'type' => 'build'
                ],
                'brand.group.snapshot.refresher' => [
                    'type' => 'build'
                ],
                'brand.group.snapshot.exporter' => [
                    'type' => 'build',
                ],
                \App\Utiles\Snapshot\Importers\Importer::class => [
                    'type' => 'resolve',
                    'configs' => 'brand.group.snapshot.import'
                ],
            ],
        ],
        'brand.group.snapshot.saver' => [
            'base' => \App\Utiles\Snapshot\Savers\Saver::class,
            'components' => [
                \App\Utiles\Snapshot\Interfaces\SnapshotMakerInterface::class => [
                    'type' => 'resolve',
                    'configs' => 'brand.group'
                ],
                'brand.group.snapshot.converter' => [
                    'type' => 'build',
                ],
                \App\Repositories\Interfaces\BrandGroupRepositoryInterface::class => [
                    'type' => 'resolve',
                ],
            ]
        ],
        'brand.group.snapshot.refresher' => [
            'base' => \App\Services\Tyre\Brand\Group\Snapshot\Refresher::class,
            'components' => [
                \App\Utiles\Snapshot\Interfaces\SnapshotMakerInterface::class => [
                    'type' => 'resolve',
                    'configs' => 'brand.group'
                ],
                'brand.group.snapshot.converter' => [
                    'type' => 'build',
                ],
                \App\Services\Tyre\Brand\Group\Snapshot\Import::class => [
                    'type' => 'resolve',
                ],
            ]
        ],
        'brand.group.snapshot.exporter' => [
            'base' => \App\Utiles\Snapshot\Exporters\Exporter::class,
            'components' => [
                \App\Utiles\Snapshot\Interfaces\SnapshotMakerInterface::class => [
                    'type' => 'resolve',
                    'configs' => 'brand.group'
                ],
            ],
        ],
        'brand.group.snapshot.converter' => [
            'base' => \App\Utiles\Converter\Converter::class,
            'components' => [
                \App\Utiles\Converter\Transformers\JsonTransformer::class => [
                    'type' => 'resolve'
                ],
                \App\Services\Tyre\Brand\Group\Snapshot\Encoder::class => [
                    'type' => 'resolve'
                ],
                \App\Utiles\Converter\Decoders\SimpleDecoder::class => [
                    'type' => 'resolve'
                ],
            ]
        ],

        /*__________________________________________________________________________
         *
         * Models Filters for merge tool Scenarios
         *__________________________________________________________________________
         */
        'filters.snapshot' => [
            'base' => \App\Utiles\Snapshot\SnapshotService::class,
            'components' => [
               \App\Services\Tyre\Model\Synonyms\Filters\Snapshot\Cleaner::class => [
                    'type' => 'resolve'
                ],
                'filters.snapshot.saver' => [
                    'type' => 'build'
                ],
                'filters.snapshot.refresher' => [
                    'type' => 'build'
                ],
                'filters.snapshot.exporter' => [
                    'type' => 'build',
                ],
                \App\Utiles\Snapshot\Importers\Importer::class => [
                    'type' => 'resolve',
                    'configs' => 'filters.snapshot.import'
                ],
            ],
        ],
        'filters.snapshot.saver' => [
            'base' => \App\Utiles\Snapshot\Savers\Saver::class,
            'components' => [
                \App\Utiles\Snapshot\Interfaces\SnapshotMakerInterface::class => [
                    'type' => 'resolve',
                    'configs' => 'filters.snapshot'
                ],
                'filters.snapshot.converter' => [
                    'type' => 'build',
                ],
                \App\Repositories\Interfaces\SynonymFilterRepositoryInterface::class => [
                    'type' => 'resolve',
                ],
            ]
        ],
        'filters.snapshot.refresher' => [
            'base' => \App\Services\Tyre\Model\Synonyms\Filters\Snapshot\Refresher::class,
            'components' => [
                \App\Utiles\Snapshot\Interfaces\SnapshotMakerInterface::class => [
                    'type' => 'resolve',
                    'configs' => 'filters.snapshot'
                ],
                'filters.snapshot.converter' => [
                    'type' => 'build',
                ],
                \App\Services\Tyre\Model\Synonyms\Filters\Snapshot\Import::class => [
                    'type' => 'resolve'
                ],
            ]
        ],
        'filters.snapshot.exporter' => [
            'base' => \App\Utiles\Snapshot\Exporters\Exporter::class,
            'components' => [
                \App\Utiles\Snapshot\Interfaces\SnapshotMakerInterface::class => [
                    'type' => 'resolve',
                    'configs' => 'filters.snapshot'
                ],
            ],
        ],
        'filters.snapshot.converter' => [
            'base' => \App\Utiles\Converter\Converter::class,
            'components' => [
                \App\Utiles\Converter\Transformers\JsonTransformer::class => [
                    'type' => 'resolve'
                ],
                \App\Services\Tyre\Model\Synonyms\Filters\Snapshot\Encoder::class => [
                    'type' => 'resolve'
                ],
                \App\Utiles\Converter\Decoders\SimpleDecoder::class => [
                    'type' => 'resolve'
                ],
            ]
        ],

        /*__________________________________________________________________________
         *
         * Model synonyms snapshot scenarios
         *__________________________________________________________________________
         */
        'synonyms.snapshot' => [
            'base' => \App\Utiles\Snapshot\SnapshotService::class,
            'components' => [
                \App\Services\Tyre\Model\Synonyms\Synonyms\Snapshot\Cleaner::class => [
                    'type' => 'resolve'
                ],
                'synonym.snapshot.saver' => [
                    'type' => 'build'
                ],
                'synonym.snapshot.refresher' => [
                    'type' => 'build'
                ],
                'synonym.snapshot.exporter' => [
                    'type' => 'build',
                ],
                \App\Utiles\Snapshot\Importers\Importer::class => [
                    'type' => 'resolve',
                    'configs' => 'synonyms.snapshot.import'
                ],
            ],
        ],
        'synonym.snapshot.exporter' => [
            'base' => \App\Utiles\Snapshot\Exporters\Exporter::class,
            'components' => [
                \App\Utiles\Snapshot\Interfaces\SnapshotMakerInterface::class => [
                    'type' => 'resolve',
                    'configs' => 'synonyms.snapshot'
                ],
            ],
        ],
        'synonym.snapshot.saver' => [
            'base' => \App\Utiles\Snapshot\Savers\Saver::class,
            'components' => [
                \App\Utiles\Snapshot\Interfaces\SnapshotMakerInterface::class => [
                    'type' => 'resolve',
                    'configs' => 'synonyms.snapshot'
                ],
                'synonyms.snapshot.converter' => [
                    'type' => 'build',
                ],
                \App\Repositories\Interfaces\TyreModelRepositoryInterface::class => [
                    'type' => 'resolve',
                ],
            ]
        ],
        'synonym.snapshot.refresher' => [
            'base' => \App\Services\Tyre\Model\Synonyms\Synonyms\Snapshot\Refresher::class,
            'components' => [
                \App\Utiles\Snapshot\Interfaces\SnapshotMakerInterface::class => [
                    'type' => 'resolve',
                    'configs' => 'synonyms.snapshot'
                ],
                'synonyms.snapshot.converter' => [
                    'type' => 'build'
                ],
                \App\Services\Synonyms\MergeService::class => [
                    'type' => 'resolve',

                ],
            ]
        ],
        'synonyms.snapshot.converter' => [
            'base' => \App\Utiles\Converter\Converter::class,
            'components' => [
                \App\Utiles\Converter\Transformers\JsonTransformer::class => [
                    'type' => 'resolve'
                ],
                \App\Services\Tyre\Model\Synonyms\Synonyms\Snapshot\Encoder::class => [
                    'type' => 'resolve'
                ],
                \App\Services\Tyre\Model\Synonyms\Synonyms\Snapshot\Decoder::class => [
                    'type' => 'resolve'
                ],
            ]
        ],
    ],
];


