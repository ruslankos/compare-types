<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index()->unique();
            $table->string('parsing_name')->index()->nullable();
            $table->string('url_name')->index()->unique();
            $table->longText('api_link')->nullable();
            $table->decimal('trustpilot')->nullable();
            $table->string('trustpilot_link')->nullable();
            $table->integer('is_active')->default(1);
            $table->date('last_parsing_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites');
    }
}
