<?php

use Illuminate\Database\Seeder;
use App\Services\Price\Fitting\Seeds\Seeder as FittingSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seeders = [
            RoleSeeder::class,
            UserSeeder::class,
            SiteSeeder::class,
            PropertySeeder::class,
            SpecialGroupSeeder::class,
            BrandGroupSeeder::class,
            FittingSeeder::class,
            LabelImageSeeder::class,
        ];

        $testSeeders = [
            BrandSeeder::class,
            TyreSeeder::class,
            ValueSeeder::class,
        ];

        if (env('APP_ENV') === 'testing') {
            $seeders = array_merge($seeders, $testSeeders);
        }

        $this->call($seeders);
    }
}
