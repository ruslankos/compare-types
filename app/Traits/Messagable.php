<?php

namespace App\Traits;

use Illuminate\Http\Request;

trait Messagable
{
    /**
     * Flash session message to the next request
     *
     * @param Request $request
     * @param $message
     */
    protected function setMessage(Request $request, $message)
    {
        $request->session()->flash('statusMessage', $message);
    }

    /**
     * Returns message from the session
     *
     * @return \Illuminate\Session\SessionManager|\Illuminate\Session\Store|mixed
     */
    protected function getMessage()
    {
        return session('statusMessage');
    }

    /**
     * Generate store message
     *
     * @param string $alias
     * @param string $name
     * @param $id
     * @return string
     */
    protected function generateStoreMessage(string $alias, string $name, $id)
    {
        return sprintf('Success! %s - %s was %s!', ucfirst($alias), $name, $id ? 'updated' : 'created');
    }

    /**
     * Generate delete message
     *
     * @param string $alias
     * @param string $name
     * @return string
     */
    protected function generateDeleteMessage(string $alias, string $name)
    {
        return sprintf('Success! %s - %s was deleted!', ucfirst($alias), $name);
    }
}