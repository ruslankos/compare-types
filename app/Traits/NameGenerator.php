<?php

namespace App\Traits;

trait NameGenerator
{
    /**
     * Create url name of entity from its name
     *
     * @param string $name
     * @return string
     */
    protected function getUrlName(string $name)
    {
        return strtolower(implode('-', explode(' ', $this->filter($name))));
    }

    /**
     * Replace parts of the string according to filters
     *
     * @param $string
     * @return mixed
     */
    protected function filter($string)
    {
        foreach ($this->nameFilters as $filter) {
            $string = $filter['method']($filter['needle'], $filter['replace'], $string);
        }

        return $string;
    }

    /**
     * Create checking name of entity from its name
     *
     * @param string $name
     * @return string
     */
    protected function getCheckName(string $name)
    {
        $nameParts = explode(' ', $this->filter($name));
        natcasesort($nameParts);

        return trim(strtolower(implode(' ', $nameParts)));
    }

    /**
     * Remove duplicates from string
     *
     * @param $string
     * @return string
     */
    protected function removeDuplicates($string)
    {
        return implode(' ', array_filter(array_unique(explode(' ', $string))));
    }

    /**
     * Number of filters that will be used to replace unnecessary data
     *
     * @var array
     */
    protected $nameFilters = [
        0 => [
            'method' => 'str_ireplace',
            'needle' => ['(', ')', '*', '\'', '"', '.', ',', '-'],
            'replace' => '',
        ],
        1 => [
            'method' => 'str_ireplace',
            'needle' => ['+', '/', '&'],
            'replace' => ['plus', '-', 'and']
        ],
    ];
}