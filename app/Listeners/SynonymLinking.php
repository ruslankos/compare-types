<?php

namespace App\Listeners;

use App\Events\SynonymCheckEvent;
use App\Services\Synonyms\RelationLinks\Interfaces\GeneratorInterface;

class SynonymLinking
{
    /**
     * @var GeneratorInterface
     */
    protected $generator;

    /**
     * SynonymLinking constructor.
     * @param GeneratorInterface $generator
     */
    public function __construct(GeneratorInterface $generator)
    {
        $this->generator = $generator;
    }

    /**
     * Hadles event
     *
     * @param SynonymCheckEvent $event
     */
    public function handle(SynonymCheckEvent $event)
    {
        $parent = $event->getParent();
        $parent->load($this->getRelations());
        $synonym = $event->getSynonym();
        $synonym->load($this->getRelations());

        $this->generator->generate($parent, $synonym);
    }

    /**
     * Takse necessary relations from configs
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    protected function getRelations()
    {
        return config('synonyms.model-relations');
    }
}