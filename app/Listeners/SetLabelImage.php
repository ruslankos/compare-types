<?php

namespace App\Listeners;

use App\Events\TyreSetProperty;
use App\Services\Tyre\ImageLabel\Managers\TyreRelationManager;

class SetLabelImage
{
    protected $setter;

    /**
     * SetLabelImage constructor.
     * @param TyreRelationManager $setter
     */
    public function __construct(TyreRelationManager $setter)
    {
        $this->setter = $setter;
    }

    /**
     * Handle the event.
     *
     * @param TyreSetProperty $event
     * @return void
     */
    public function handle(TyreSetProperty $event)
    {
        $this->setter->set($event->getTyre());
    }
}
