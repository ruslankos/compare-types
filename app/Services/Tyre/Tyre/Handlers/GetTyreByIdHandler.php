<?php

namespace App\Services\Tyre\Tyre\Handlers;

use App\Commands\Tyre\Tyre\FindTyreByIdCommand;
use App\Models\Tyre;
use App\Repositories\Interfaces\TyreModelRepositoryInterface;
use App\Repositories\Interfaces\TyreRepositoryInterface;
use App\Services\Tyre\Storage\CookieService;
use App\Utiles\DTO\Interfaces\DTOInterface;
use App\Utiles\Resolver\Handler;
use App\Utiles\Mutators\Services\Facade as MutatorService;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class GetTyreByIdHandler
 * @package App\Services\Tyre\Tyre\Handlers
 */
final class GetTyreByIdHandler extends Handler
{
    /**
     * @var TyreRepositoryInterface
     */
    private $tyreRepository;

    /**
     * @var TyreModelRepositoryInterface
     */
    private $modelRepository;

    /**
     * @var CookieService
     */
    private $cookieService;

    /**
     * @var MutatorService
     */
    private $mutatorService;

    /**
     * GetTyreByIdHandler constructor.
     * @param TyreRepositoryInterface $tyreRepository
     * @param TyreModelRepositoryInterface $modelRepository
     * @param CookieService $cookieService
     * @param MutatorService $mutatorService
     */
    public function __construct(
        TyreRepositoryInterface $tyreRepository,
        TyreModelRepositoryInterface $modelRepository,
        CookieService $cookieService,
        MutatorService $mutatorService
    ) {
        $this->tyreRepository = $tyreRepository;
        $this->modelRepository = $modelRepository;
        $this->cookieService = $cookieService;
        $this->mutatorService = $mutatorService;
    }

    /**
     * @param $object
     * @return Tyre
     */
    protected function proceed($object)
    {
        /**
         * @var FindTyreByIdCommand $object
         */
        $tyre = $this->getTyre($object->getId());
        $model = $this->getModel($object->getModelUrlName(), $object->getBrandId());
        if (! $tyre || ! $model) {
            throw new ModelNotFoundException();
        }
        $tyre->setRelation(Tyre::MODEL, $model);
        $tyre = $this->mutatorService->mutateSingle($tyre, 'tyre-view');
        $this->cookieService->storeVisitedTyreSizeToCookie($tyre);

        return $tyre;
    }

    /**
     * @param int $id
     * @return Tyre
     */
    private function getTyre(int $id)
    {
        return $this->tyreRepository->findBy(
            [
                ['id', '=', $id],
                ['is_active', '=', Tyre::ACTIVE]
            ],
            [
                'relations' => ['tyresProperties', 'tyresProperties.property', 'tyresProperties.value', 'specials']
            ]
        )->first();
    }

    /**
     * @param string $urlName
     * @param int $brandId
     * @return mixed
     */
    private function getModel(string $urlName, int $brandId)
    {
        return $this->modelRepository->getUnique($brandId, $urlName);
    }

    /**
     * @param DTOInterface $object
     * @return bool
     */
    protected function isSatisfied(DTOInterface $object): bool
    {
        return $object instanceof FindTyreByIdCommand;
    }

}