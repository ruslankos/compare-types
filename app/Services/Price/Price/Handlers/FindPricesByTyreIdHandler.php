<?php

namespace App\Services\Price\Price\Handlers;

use App\Commands\Price\Price\FindPricesByTyreIdAndVouchersCommand;
use App\Services\Price\Price\Repository\RepositoryInterface as PriceRepositoryInterface;
use App\Services\Price\Fitting\DataObtainers\FittingsFromPricesCollector;
use App\Services\Price\Price\Collectors\BestPricesRangeCollector;
use App\Services\Price\Price\Filters\FittingFilterService;
use App\Services\Price\Price\Transformers\ByVouchersTransformer;
use App\Utiles\DTO\Interfaces\DTOInterface;
use App\Utiles\Resolver\Handler;

/**
 * Class FindPricesByTyreIdHandler
 * @package App\Services\Price\Price\Handlers
 */
final class FindPricesByTyreIdHandler extends Handler
{
    /**
     * @var PriceRepositoryInterface
     */
    private $priceRepository;

    /**
     * @var FittingFilterService
     */
    private $fittingFilterService;

    /**
     * @var FittingsFromPricesCollector
     */
    private $fittingCollector;

    /**
     * @var ByVouchersTransformer
     */
    private $priceTransformer;

    /**
     * @var BestPricesRangeCollector
     */
    private $bestPriceRangeCollector;

    /**
     * FindPricesByTyreIdHandler constructor.
     * @param PriceRepositoryInterface $priceRepository
     * @param FittingFilterService $filterService
     * @param FittingsFromPricesCollector $fittingCollector
     * @param ByVouchersTransformer $transformer
     * @param BestPricesRangeCollector $bestPricesRangeCollector
     */
    public function __construct(
        PriceRepositoryInterface $priceRepository,
        FittingFilterService $filterService,
        FittingsFromPricesCollector $fittingCollector,
        ByVouchersTransformer $transformer,
        BestPricesRangeCollector $bestPricesRangeCollector
    ) {
        $this->priceRepository = $priceRepository;
        $this->fittingFilterService = $filterService;
        $this->fittingCollector = $fittingCollector;
        $this->priceTransformer = $transformer;
        $this->bestPriceRangeCollector = $bestPricesRangeCollector;
    }

    /**
     * @param $object
     * @return array|mixed
     */
    protected function proceed($object)
    {
        /**
         * @var FindPricesByTyreIdAndVouchersCommand $object
         */
        $prices = $this->priceRepository->getPricesByTyreId($object->getTyreId());
        $filteredPrices = $this->fittingFilterService->filterPrices($prices, $object->getCookies());
        $fittings = $this->fittingCollector->collectFittings($prices, $filteredPrices);
        $range = $this->bestPriceRangeCollector
            ->getRangeOfPricesFromDifferentFittings($prices, $fittings, $object->getQuantity());

        return [
            'prices' => $this->priceTransformer->transformByVouchers($filteredPrices, $object->getVouchers()),
            'fittings' => $fittings,
            'range' => $range,
        ];
    }

    /**
     * @param DTOInterface $object
     * @return bool
     */
    protected function isSatisfied(DTOInterface $object): bool
    {
        return $object instanceof FindPricesByTyreIdAndVouchersCommand;
    }
}
