<?php

namespace App\Services\Price\Price\EntityManager;

use App\Utiles\EntityManager\EntityManagerInterface as EntityManager;

/**
 * Interface PriceGroupManager
 *
 * @package App\EntityManagers\Interfaces
 */
interface EntityManagerInterface extends EntityManager
{

}