<?php

namespace App\Services\Price\Price\EntityManager;

use App\Models\Price;
use App\Utiles\EntityManager\EntityManager as BaseManager;

/**
 * Class PriceGroupManager
 *
 * @package App\EntityManagers
 */
final class EntityManager extends BaseManager implements EntityManagerInterface
{
    /**
     * Morph relations of the Site class
     *
     * @var array
     */
    protected $morphRelations = ['link'];

    /**
     * Connect manager with the entity
     */
    protected function getClass()
    {
        return Price::class;
    }

}