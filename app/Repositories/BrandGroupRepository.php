<?php

namespace App\Repositories;

use App\Models\BrandGroup;
use App\Repositories\Interfaces\BrandGroupRepositoryInterface;
use App\Utiles\Repository\Repository;
use Illuminate\Support\Collection;

class BrandGroupRepository extends Repository implements BrandGroupRepositoryInterface
{
    /**
     * @return string
     */
    protected function getClass()
    {
        return BrandGroup::class;
    }

    /**
     * Returns collection of groups with brands
     *
     * @return Collection
     */
    public function getGroupsWithBrands(): Collection
    {
        return $this->className::whereHas('brands')->with('brands')->get();
    }

    /**
     * Returns collection for snapshot making
     *
     * @return Collection
     */
    public function getSnapshotCollection()
    {
        return $this->all(['relations' => ['brands']]);
    }
}