<?php

namespace App\Repositories\Interfaces;

use App\Utiles\Repository\RepositoryInterface;
use App\Utiles\Snapshot\Interfaces\SnapShotableInterface;
use Illuminate\Support\Collection;

interface BrandGroupRepositoryInterface extends RepositoryInterface, SnapShotableInterface
{
    public function getGroupsWithBrands() : Collection;
}