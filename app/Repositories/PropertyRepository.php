<?php

namespace App\Repositories;

use App\Models\Property;
use App\Repositories\Interfaces\PropertyRepositoryInterface;
use App\Utiles\Repository\Repository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class PropertyRepository extends Repository implements PropertyRepositoryInterface
{
    /**
     * Connected repository to some entity
     *
     * @return string
     */
    protected function getClass()
    {
        return Property::class;
    }

    public function getDefault() : Collection
    {
        $defaultAliases = array_keys(config('properties.default'));

        return $this->whereIn('alias', $defaultAliases, ['relations' => ['values'], 'orderBy' => ['order']]);
    }

    public function getWidth() : Model
    {
        $properties = $this->getDefault();

        return $properties->shift();
    }
}