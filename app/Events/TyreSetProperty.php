<?php

namespace App\Events;

use App\Models\Tyre;

class TyreSetProperty
{
    /**
     * @var Tyre
     */
    protected $tyre;

    /**
     * PropertyIndexEvent constructor.
     *
     * @param Tyre $tyre
     */
    public function __construct(Tyre $tyre)
    {
        $this->tyre = $tyre;
    }

    /**
     * @return Tyre
     */
    public function getTyre()
    {
        return $this->tyre;
    }
}