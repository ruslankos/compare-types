<?php

namespace App\Events;


use App\Models\TyreModel;

class SynonymCheckEvent
{
    /**
     * @var TyreModel
     */
    protected $parent;

    /**
     * @var TyreModel
     */
    protected $synonym;

    /**
     * SynonymCheckEvent constructor.
     * @param TyreModel $parent
     * @param TyreModel $synonym
     */
    public function __construct(TyreModel $parent, TyreModel $synonym)
    {
        $this->parent = $parent;
        $this->synonym = $synonym;
    }

    /**
     * @return TyreModel
     */
    public function getParent(): TyreModel
    {
        return $this->parent;
    }

    /**
     * @return TyreModel
     */
    public function getSynonym(): TyreModel
    {
        return $this->synonym;
    }
}