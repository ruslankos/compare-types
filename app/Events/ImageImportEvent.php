<?php

namespace App\Events;

use App\Services\Shared\Images\Import\Objects\Image;

class ImageImportEvent
{
    /**
     * @var Image
     */
    private $image;

    /**
     * Create a new event instance.
     *
     * @param Image $image
     */
    public function __construct(Image $image)
    {
        $this->image = $image;
    }

    /**
     * @return Image
     */
    public function getImage(): Image
    {
        return $this->image;
    }
}
