<?php

namespace App\Providers;

use App\EntityManagers\NegativeReportManager;
use App\EntityManagers\PositiveReportManager;
use App\Services\Tyre\Property\Parsing\Transformers\PropertyTransformer;
use App\Utiles\Parser\Preparator\DataBuilderInterface;
use App\Utiles\Resolver\Resolver;
use App\Utiles\Transformers\Arrays\Data\TransformerServiceInterface;
use App\Services\Site\Managers\EntityManagerInterface as SiteManager;
use App\Utiles\Filters\String\FilterBuilder;
use App\Services\Parsing\Parser\Preparator\DataBuilder;
use App\Utiles\Transformers\Arrays\Data\Factory;
use App\Services\Tyre\Property\Parsing\Transformers\LabelTransformer;
use App\Services\Price\Price\Parsing\Transformers\PriceTransformer;
use App\Services\Tyre\Special\Special\Parsing\Transformers\SpecialsTransformer;
use App\Services\Tyre\Tyre\Parsing\Transformers\TyreTransformer;
use App\Utiles\Transformers\Arrays\Data\TransformerService;
use App\Services\ImageServices\ImageParser;
use App\Services\ImageServices\ImageParseValidator;
use App\Services\ImageServices\Interfaces\ImageParserInterface;
use App\Services\ImageServices\Interfaces\ImageParsingValidatorInterface;
use App\Utiles\Config\ConfigUploader;
use App\Utiles\Generators\Strings\Services\NameGenerator;
use App\Services\ParseReportGenerator;
use Illuminate\Support\ServiceProvider;

class ParserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(DataBuilder::class, function ($app) {
            return new DataBuilder(
                $app->make(Resolver::class)
            );
        });

        $this->app->bind(FilterBuilder::class, function ($app) {
            return new FilterBuilder(
                $app->make(Resolver::class)
            );
        });

        $this->app->bind(ParseReportGenerator::class, function ($app) {
            return new ParseReportGenerator(
                $app->make(PositiveReportManager::class),
                $app->make(NegativeReportManager::class),
                $app->make(SiteManager::class)
            );
        });

        $this->app->bind(DataBuilderInterface::class, DataBuilder::class);

        $this->app->bind(TransformerServiceInterface::class, TransformerService::class);

        $this->app->bind(ImageParserInterface::class, ImageParser::class);

        $this->app->bind(ImageParsingValidatorInterface::class, ImageParseValidator::class);

        /*______________________________________________________________________________________________________________
         |
         |                                       TRANSFORMERS SECTION
         |______________________________________________________________________________________________________________
         */

        $this->app->bind(Factory::class, function ($app) {
            return new Factory();
        });

        $this->app->bind(TransformerService::class, function ($app) {
            return new TransformerService(
                $app->make(Factory::class)
            );
        });

        $this->app->bind(PropertyTransformer::class, function ($app) {
            return new PropertyTransformer(
                $app->make(FilterBuilder::class)
            );
        });

        $this->app->bind(SpecialsTransformer::class, function ($app) {
            return new SpecialsTransformer(
                $app->make(FilterBuilder::class),
                $app->make(NameGenerator::class),
                $app->make(ConfigUploader::class)
            );
        });

        $this->app->bind(TyreTransformer::class, function ($app) {
            return new TyreTransformer(
                $app->make(NameGenerator::class),
                $app->make(ConfigUploader::class)
            );
        });

        $this->app->bind(LabelTransformer::class, function ($app) {
            return new LabelTransformer(
                $app->make(FilterBuilder::class),
                $app->make(ConfigUploader::class)
            );
        });

        $this->app->bind(PriceTransformer::class, function () {
            return new PriceTransformer();
        });
    }
}
