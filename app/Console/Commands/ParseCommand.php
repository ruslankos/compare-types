<?php

namespace App\Console\Commands;

use App\Models\Site;
use App\Utiles\Parser\Factory as ParserBuilder;
use App\Services\Site\Repository\RepositoryInterface as SiteRepositoryInterface;
use App\Services\ParseReportGenerator;
use Illuminate\Console\Command;

class ParseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:start {site?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Starts the parsing';

    /**
     * @var ParserBuilder
     */
    protected $parseBuilder;

    /**
     * @var SiteRepositoryInterface
     */
    protected $siteRepository;

    /**
     * @var ParseReportGenerator
     */
    protected $reportGenerator;

    /**
     * Create a new command instance.
     *
     * @param SiteRepositoryInterface $siteRepository
     * @param ParserBuilder $parserBuilder
     * @param ParseReportGenerator $reportGenerator
     */
    public function __construct(
        ParserBuilder $parserBuilder,
        SiteRepositoryInterface $siteRepository,
        ParseReportGenerator $reportGenerator
    ) {
        $this->parseBuilder = $parserBuilder;
        $this->siteRepository = $siteRepository;
        $this->reportGenerator = $reportGenerator;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $site = $this->argument('site');

        return $site ? $this->parseSingle($site) : $this->parseAll();
    }

    /**
     * Starts the parsing of all sites
     */
    protected function parseAll()
    {
        $sites = $this->getSites();

        foreach ($sites as $site) {
            $this->line(sprintf('Trying to start %s parsing', $site->name));
            $this->startParsing($site);
        }
    }

    /**
     * Start parsing for single site by given ID
     *
     * @param int $siteId
     * @return bool
     */
    protected function parseSingle(int $siteId)
    {
        $site = $this->getSiteById($siteId);
        if (! $site) {
            $this->error(sprintf('There is no site with id %d at DB', $siteId));

            return false;
        }

        $this->line(sprintf('Trying to start %s parsing', $site->name));
        $this->startParsing($site);
    }

    /**
     * Returns site with given id
     *
     * @param $site
     * @return Site
     */
    protected function getSiteById($site)
    {
        return $this->siteRepository->findBy(['id' => $site])->first();
    }

    /**
     * Start parsing function
     *
     * @param Site $site
     */
    protected function startParsing(Site $site)
    {
        $parser = $this->parseBuilder->buildParser($site);
        try {
            $answer = $parser->parse();
            $this->makeReport($site, $answer);
        } catch (\Exception $e) {
            $answer = ['reason' => $e->getMessage()];
            $this->makeReport($site, $answer, true);
        }
    }

    /**
     * Make parsing report
     *
     * @param $site
     * @param $data
     * @param bool $negative
     */
    protected function makeReport($site, $data, $negative = false)
    {
        if ($negative) {
            $this->error(sprintf('%s parsing was failed', $site->name));
        } else {
            $this->info('Success');
        }
        $this->line('Preparing the report');
        $this->reportGenerator->setFailed($negative);
        $this->reportGenerator->generate($site, $data);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    protected function getSites()
    {
        return $this->siteRepository->findBy(['is_active' => 1]);
    }
}
