<?php

namespace App\Console\Commands;

use App\EntityManagers\Interfaces\VoucherManagerInterface;
use App\Models\Voucher;
use App\Repositories\Interfaces\VoucherRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

/**
 * Class DeleteInactiveVouchers
 * @package App\Console\Commands
 */
class DeleteInactiveVouchers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vouchers:delete:inactive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes inactive vouchers from DB';

    /**
     * @var VoucherRepositoryInterface
     */
    protected $voucherRepository;

    /**
     * @var VoucherManagerInterface
     */
    protected $voucherManager;

    /**
     * Create a new command instance.
     *
     * @param VoucherRepositoryInterface $repository
     * @param VoucherManagerInterface $manager
     * @return void
     */
    public function __construct(VoucherRepositoryInterface $repository, VoucherManagerInterface $manager)
    {
        $this->voucherRepository = $repository;
        $this->voucherManager = $manager;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('Collecting vouchers...');
        $vouchers = $this->getVouchers($this->getToday());
        if ($vouchers->count()) {
            $this->info('Success. Vouchers were collected');
            $this->deleteVouchers($vouchers);
        } else {
            $this->warn('Nothing to delete');
        }
    }

    /**
     * @param Collection $vouchers
     */
    protected function deleteVouchers(Collection $vouchers)
    {
        $count = $vouchers->count();
        $this->line(sprintf('Deleting %d vouchers...', $count));
        $bar = $this->output->createProgressBar($count);
        $bar->setFormat('debug');
        foreach ($vouchers as $voucher) {
            $this->deleteVoucher($voucher);
            $bar->advance();
        }
        $bar->finish();
        $this->info(sprintf('  - Success! %d vouchers were deleted', $count));
    }

    /**
     * @param Voucher $voucher
     * @return bool
     */
    protected function deleteVoucher(Voucher $voucher)
    {
        return $this->voucherManager->delete($voucher);
    }

    /**
     * @param string $date
     * @return Collection
     */
    protected function getVouchers(string $date)
    {
        return $this->voucherRepository->findBy([[
            Voucher::END_DATE, '<', $date
        ]]);
    }

    /**
     * @return string
     */
    protected function getToday()
    {
        return Carbon::now()->format('Y-m-d');
    }
}
