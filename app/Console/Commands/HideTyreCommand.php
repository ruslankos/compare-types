<?php

namespace App\Console\Commands;

use App\EntityManagers\Interfaces\TyreManagerIneterface;
use App\EntityManagers\Interfaces\TyreModelManagerInterface;
use App\Models\Tyre;
use App\Repositories\Interfaces\TyreRepositoryInterface;
use DebugBar\DebugBar;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class HideTyreCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hide:tyres {--show}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var TyreRepositoryInterface
     */
    protected $tyreRepository;

    /**
     * @var TyreModelManagerInterface
     */
    protected $tyreManager;

    /**
     * @var array
     */
    protected $specials = [
        'winter', 'xl', 'extra load', 'reinforced', 'runflat', 'all season'
    ];

    /**
     * HideTyreCommand constructor.
     * @param TyreRepositoryInterface $tyreRepository
     * @param TyreManagerIneterface $tyreManager
     */
    public function __construct(TyreRepositoryInterface $tyreRepository, TyreManagerIneterface $tyreManager)
    {
        $this->tyreRepository = $tyreRepository;
        $this->tyreManager = $tyreManager;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return $this->option('show') ? $this->show() : $this->hide();
    }

    /**
     * Functions that starts activation of tyres
     */
    protected function show()
    {
        $this->tyreRepository->setWithDefault(false);
        $tyres = $this->tyreRepository->findBy([['is_active', '=', Tyre::IN_ACTICE]]);

        return $this->showTyres($tyres);
    }

    /**
     * Function that switch is_active param into active state
     *
     * @param Collection $tyres
     */
    protected function showTyres(Collection $tyres)
    {
        $countTyres = count($tyres);
        $this->line(sprintf('Activating %d tyres', $countTyres));
        $bar = $this->output->createProgressBar($countTyres);
        $bar->setFormat('debug');
        foreach ($tyres as $tyre) {
            $tyre->is_active = Tyre::ACTIVE;
            $this->tyreManager->save($tyre);
            $bar->advance();
        }
        $bar->finish();
        $this->info(sprintf('  - Success! %d tyres were activated', $countTyres));
    }

    /**
     * Function, that starts tyre hiding process
     */
    protected function hide()
    {
        $tyres = $this->tyreRepository->getTyresBySpecials($this->specials);

        return $this->hideTyres($tyres);
    }

    /**
     * Put the is_active tyre attribute to inactive state
     *
     * @param Collection $tyres
     */
    protected function hideTyres(Collection $tyres)
    {
        $tyresCount = count($tyres);
        $this->line(sprintf('Hiding %d tyres', $tyresCount));
        $bar = $this->output->createProgressBar($tyresCount);
        $bar->setFormat('debug');
        foreach ($tyres as $tyre) {
            $tyre->is_active = Tyre::IN_ACTICE;
            $this->tyreManager->save($tyre);
            $bar->advance();
        }
        $bar->finish();
        $this->info(sprintf('  - Success! %d tyres were inactivated', $tyresCount));
    }
}
