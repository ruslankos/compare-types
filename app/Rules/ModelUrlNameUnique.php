<?php

namespace App\Rules;

use App\Models\TyreModel;
use App\Repositories\Interfaces\TyreModelRepositoryInterface;
use App\Utiles\Generators\Strings\NameGenerators\Filterable\UrlName;
use Illuminate\Contracts\Validation\Rule;

class ModelUrlNameUnique implements Rule
{
    /**
     * @var TyreModel
     */
    protected $model;

    /**
     * @var UrlName
     */
    protected $nameGenerator;

    /**
     * @var TyreModelRepositoryInterface
     */
    protected $modelRepository;

    /**
     * ModelUrlNameUnique constructor.
     * @param TyreModel $model
     * @param UrlName $nameGenerator
     * @param TyreModelRepositoryInterface $modelRepository
     */
    public function __construct(
        TyreModel $model,
        UrlName $nameGenerator,
        TyreModelRepositoryInterface $modelRepository
    ) {
        $this->model = $model;
        $this->nameGenerator = $nameGenerator;
        $this->modelRepository = $modelRepository;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $urlName = $this->nameGenerator->generate($value);

        $model = $this->modelRepository->findBy([
            ['url_name', '=', $urlName],
            ['brand_id', '=', $this->model->brand_id],
        ])->first();
        if (! $model) {
            return true;
        }

        return $model->id === $this->model->id;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The name has been already taken';
    }
}
