<?php

namespace App\Rules;

use App\Models\TyreModel;
use Illuminate\Contracts\Validation\Rule;

class ModelDisplayTypes implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return in_array($value, $this->getTypes());
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid display type';
    }

    /**
     * @return array
     */
    protected function getTypes()
    {
        return config('model-display.types');
    }
}
