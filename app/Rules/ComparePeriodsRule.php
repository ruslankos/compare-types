<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ComparePeriodsRule implements Rule
{
    /**
     * @var
     */
    protected $endDate;

    /**
     * Create a new rule instance.
     */
    public function __construct($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->endDate > $value;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'End date should be larger then start date';
    }
}
