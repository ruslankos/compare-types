<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    const DEFAULT_PATH = 'uplaoaghalhaglhalaghal.no-image.jpg';

    /*__________________________________________________________________________________________________________________
    |
    |                                              SETTINGS
    |__________________________________________________________________________________________________________________
    */
    /**
     * Fillable columns
     *
     * @var array
     */
    protected $fillable = [
        'name', 'path', 'imagable_id', 'imagable_type', 'is_thumbnail'
    ];

    /*__________________________________________________________________________________________________________________
    |
    |                                              MAIN ATTRIBUTES
    |__________________________________________________________________________________________________________________
    */
    /**
     * Returns name of the image
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name of the image
     *
     * @param string $name
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Returns path of the image
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set path of the image
     *
     * @param string $path
     * @return $this
     */
    public function setPath(string $path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Returns id of the image owner
     *
     * @return integer
     */
    public function getImagableId()
    {
        return $this->imagable_id;
    }

    /**
     * Set id of the image owner
     *
     * @param int $id
     * @return $this
     */
    public function setImagableId(int $id)
    {
        $this->imagable_id = $id;

        return $this;
    }

    /**
     * Returns owner class name
     *
     * @return string
     */
    public function getImagableType()
    {
        return $this->imagable_type;
    }

    /**
     * Set owner class name
     *
     * @param string $type
     * @return $this
     */
    public function setImagableType(string $type)
    {
        $this->imagable_type = $type;

        return $this;
    }

    /**
     * Returns is_thumbnail attribute
     *
     * @return integer
     */
    public function getIsThumbnail()
    {
        return $this->is_thumbnail;
    }

    /**
     * Set is_thumbnail attribute
     *
     * @param int $thumb
     * @return $this
     */
    public function setIsThumbnail(int $thumb)
    {
        $this->is_thumbnail = $thumb;

        return $this;
    }

    /*__________________________________________________________________________________________________________________
    |
    |                                              RELATIONS
    |__________________________________________________________________________________________________________________
    */
    /**
     * Owner of the image. It could be Tyre, Brand or Site entity
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function imagable()
    {
        return $this->morphTo();
    }
}
