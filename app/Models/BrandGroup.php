<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandGroup extends Model
{
    /*__________________________________________________________________________________________________________________
    |
    |                                              SETTINGS
    |__________________________________________________________________________________________________________________
    */
    /**
     * Fillable columns
     *
     * @var array
     */
    protected $fillable = [
        'name', 'url_name'
    ];

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'url_name';
    }

    /*__________________________________________________________________________________________________________________
    |
    |                                              RELATIONS
    |__________________________________________________________________________________________________________________
    */
    /**
     * Brands of current group
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function brands()
    {
        return $this->hasMany(Brand::class, 'brand_group_id');
    }
}
