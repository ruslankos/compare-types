<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Shared\Description\Contracts\OwnerInterface as DescriptionOwner;

class Brand extends Model implements DescriptionOwner
{
    /*__________________________________________________________________________________________________________________
    |
    |                                              CONSTANTS
    |__________________________________________________________________________________________________________________
    */
    const ACTIVE_MODELS = 'displayModels';

    /*__________________________________________________________________________________________________________________
    |
    |                                              SETTINGS
    |__________________________________________________________________________________________________________________
    */
    /**
     * Fillable columns
     *
     * @var array
     */
    protected $fillable = [
        'name', 'url_name', 'brand_group_id'
    ];

    /**
     * Name of the route key
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'url_name';
    }
    /*__________________________________________________________________________________________________________________
    |
    |                                              MAIN ATTRIBUTES
    |__________________________________________________________________________________________________________________
    */

    /**
     * Returns brand name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set brand name
     *
     * @param string $name
     * @return Brand
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Returns brand url name
     *
     * @return string
     */
    public function getUrlName()
    {
        return $this->url_name;
    }

    /**
     * Set brand url name
     *
     * @param string $urlName
     * @return Brand
     */
    public function setUrlName(string $urlName)
    {
        $this->url_name = $urlName;

        return $this;
    }

    /**
     * Returns created at date
     *
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Returns updated at date
     *
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /*__________________________________________________________________________________________________________________
    |
    |                                              RELATIONS
    |__________________________________________________________________________________________________________________
    */
    /**
     * Tyres of this brand
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function tyres()
    {
        return $this->hasManyThrough(
            Tyre::class,
            TyreModel::class,
            'brand_id',
            'tyre_model_id',
            'id',
            'id'
        );
    }

    /**
     * Active tyres of this brand
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function activeTyres()
    {
        return $this->hasManyThrough(
            Tyre::class,
            TyreModel::class,
            'brand_id',
            'tyre_model_id',
            'id',
            'id'
        )->where('is_active', Tyre::ACTIVE);
    }

    /**
     * Tyre models of current brand
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function models()
    {
        return $this->hasMany(TyreModel::class, 'brand_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function displayModels()
    {
        return $this->hasMany(TyreModel::class, 'brand_id')
            ->where('parent_id', '=', null);
    }

    /**
     * Image of the brand
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'imagable');
    }

    /**
     * Brand group
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(BrandGroup::class, 'brand_group_id');
    }

    /**
     * Filters for the given brand models
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function filters()
    {
        return $this->hasMany(SynonymFilter::class);
    }

    /**
     * Description of the brand
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function description()
    {
        return $this->morphOne(Description::class, 'describable');
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getOwnerType(): string
    {
        return self::class;
    }

    /**
     * @return int
     */
    public function getOwnerId(): int
    {
        return $this->getKey();
    }
}

