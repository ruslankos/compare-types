<?php

namespace App\Models\Car;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    /*__________________________________________________________________________________________________________________
    |
    |                                              CONSTANTS
    |__________________________________________________________________________________________________________________
    */
    const TABLE = 'cars';
    const FIELD_MANUFACTURER_ID = 'manufacturer_id';
    const FIELD_RANGE_ID = 'range_id';
    const FIELD_MODEL_ID = 'model_id';
    const FIELD_ENGINE_ID = 'engine_id';
    const FIELD_PRODUCTION_DATE_ID = 'production_date_id';
    const RELATION_MANUFACTURER = 'manufacturer';
    const RELATION_RANGE = 'range';
    const RELATION_MODEL = 'model';
    const RELATION_PRODUCTION_DATE = 'productionDate';
    const RELATION_ENGINE = 'engine';
    const RELATION_TYRES = 'tyres';
    const RELATION_TYRES_FOREIGN = 'car_id';

    /*__________________________________________________________________________________________________________________
    |
    |                                              SETTINGS
    |__________________________________________________________________________________________________________________
    */
    /**
     * @var string
     */
    protected $table = self::TABLE;

    /**
     * @var array
     */
    protected $fillable = [
        self::FIELD_MANUFACTURER_ID,
        self::FIELD_RANGE_ID,
        self::FIELD_MODEL_ID,
        self::FIELD_ENGINE_ID,
        self::FIELD_PRODUCTION_DATE_ID
    ];

    /*__________________________________________________________________________________________________________________
    |
    |                                              MAIN ATTRIBUTES
    |__________________________________________________________________________________________________________________
    */
    /**
     * @return int
     */
    public function getManufacturerId()
    {
        return $this->{self::FIELD_MANUFACTURER_ID};
    }

    /**
     * @return mixed
     */
    public function getRangeId()
    {
        return $this->{self::FIELD_RANGE_ID};
    }

    /**
     * @return mixed
     */
    public function getEngineId()
    {
        return $this->{self::FIELD_ENGINE_ID};
    }

    /**
     * @return int|null
     */
    public function getModelId()
    {
        return $this->{self::FIELD_MODEL_ID};
    }

    /**
     * @return mixed
     */
    public function getProductionDateId()
    {
        return $this->{self::FIELD_PRODUCTION_DATE_ID};
    }

    /*__________________________________________________________________________________________________________________
    |
    |                                              RELATIONS
    |__________________________________________________________________________________________________________________
    */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manufacturer()
    {
        return $this->belongsTo(Manufacturer::class, self::FIELD_MANUFACTURER_ID);
    }

    /**
     * @return mixed
     */
    public function getManufacturer()
    {
        return $this->{self::RELATION_MANUFACTURER};
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function model()
    {
        return $this->belongsTo(Model::class, self::FIELD_MODEL_ID);
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->{self::RELATION_MODEL};
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function engine()
    {
        return $this->belongsTo(Engine::class, self::FIELD_ENGINE_ID);
    }

    /**
     * @return mixed
     */
    public function getEngine()
    {
        return $this->{self::RELATION_ENGINE};
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function range()
    {
        return $this->belongsTo(Range::class, self::FIELD_RANGE_ID);
    }

    /**
     * @return mixed
     */
    public function getRange()
    {
        return $this->{self::RELATION_RANGE};
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function productionDate()
    {
        return $this->belongsTo(ProductionDates::class, self::FIELD_PRODUCTION_DATE_ID);
    }

    /**
     * @return mixed
     */
    public function getProductionDate()
    {
        return $this->{self::RELATION_PRODUCTION_DATE};
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tyres()
    {
        return $this->belongsToMany(
            Car::class,
            Tyre::RELATION_CARS_PIVOT,
            self::RELATION_TYRES_FOREIGN,
            Tyre::RELATION_CARS_FOREIGN
        );
    }

    /**
     * @return mixed
     */
    public function getTyres()
    {
        return $this->{self::RELATION_TYRES};
    }
}
