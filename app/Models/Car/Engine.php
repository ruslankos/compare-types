<?php

namespace App\Models\Car;

use Illuminate\Database\Eloquent\Model as Base;
use Illuminate\Support\Collection;

class Engine extends Base
{
    /*__________________________________________________________________________________________________________________
    |
    |                                              CONSTANTS
    |__________________________________________________________________________________________________________________
    */
    const TABLE = 'car_engines';
    const FIELD_NAME = 'name';
    const FIELD_URL_NAME = 'url_name';
    const FIELD_ALIAS = 'alias';
    const FIELD_MODEL_ID = 'model_id';
    const RELATION_MODEL = 'model';
    const RELATION_CARS = 'cars';

    /*__________________________________________________________________________________________________________________
    |
    |                                              SETTINGS
    |__________________________________________________________________________________________________________________
    */
    /**
     * @var string
     */
    protected $table = self::TABLE;

    /**
     * @var array
     */
    protected $fillable = [
        self::FIELD_NAME, self::FIELD_ALIAS, self::FIELD_MODEL_ID, self::FIELD_URL_NAME
    ];

    /*__________________________________________________________________________________________________________________
    |
    |                                              MAIN ATTRIBUTES
    |__________________________________________________________________________________________________________________
    */
    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->{self::FIELD_NAME};
    }

    /**
     * @return string|null
     */
    public function getUrlName()
    {
        return $this->{self::FIELD_URL_NAME};
    }

    /**
     * @return string|null
     */
    public function getAlias()
    {
        return $this->{self::FIELD_ALIAS};
    }

    /**
     * @return int|null
     */
    public function getModelId()
    {
        return $this->{self::FIELD_MODEL_ID};
    }

    /*__________________________________________________________________________________________________________________
    |
    |                                              RELATIONS
    |__________________________________________________________________________________________________________________
    */
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cars()
    {
        return $this->hasMany(Car::class);
    }

    /**
     * @return Collection
     */
    public function getCars(): Collection
    {
        return $this->{self::RELATION_CARS};
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function model()
    {
        return $this->belongsTo(Model::class, self::FIELD_MODEL_ID);
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->{self::RELATION_MODEL};
    }
}



