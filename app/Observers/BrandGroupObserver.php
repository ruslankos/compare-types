<?php

namespace App\Observers;

use App\Services\Tyre\Brand\Brand\Managers\ManagerInterface as BrandManagerInterface;
use App\Models\Brand;
use App\Models\BrandGroup;

class BrandGroupObserver
{
    /**
     * @var BrandManagerInterface
     */
    protected $brandManager;

    /**
     * BrandGroupObserver constructor.
     * @param BrandManagerInterface $brandManager
     */
    public function __construct(BrandManagerInterface $brandManager)
    {
        $this->brandManager = $brandManager;
    }

    /**
     * Listening of the brand group deleting and set the brands group id to the null.
     * This is made to avoid deleting of the brands
     *
     * @param BrandGroup $group
     */
    public function deleting(BrandGroup $group)
    {
        foreach ($group->brands as $brand) {
            $this->refreshBrandGroup($brand);
        }
    }

    /**
     * Set the brand_group_id to the native state (null)
     *
     * @param Brand $brand
     * @return mixed
     */
    protected function refreshBrandGroup(Brand $brand)
    {
        $brand->brand_group_id = null;

        return $this->brandManager->save($brand);
    }
}