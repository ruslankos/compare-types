<?php

namespace App\Http\Controllers;

use App\Helpers\DTO\Objects\Tyre\Properties\FindSelectedSizes;
use App\Services\Price\Fitting\FittingServiceInterface;
use App\Services\Tyre\Property\Search\PropertiesByValues;
use App\Services\Tyre\TyreServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

/**
 * Class SizeSelectController
 * @package App\Http\Controllers
 */
class SizeSelectController extends Controller
{
    /**
     * @param Request $request
     * @param PropertiesByValues $searcher
     * @param FittingServiceInterface $fittingService
     * @return \Illuminate\Http\JsonResponse
     */
    public function select(Request $request, PropertiesByValues $searcher, FittingServiceInterface $fittingService)
    {
        $properties = $searcher->search($request->query());

        return $this->makeResponse($properties, $fittingService->getAll());
    }

    /**
     * @param Request $request
     * @param TyreServiceInterface $tyreService
     * @param FittingServiceInterface $fittingService
     * @return \Illuminate\Http\JsonResponse
     */
    public function collect(
        Request $request,
        TyreServiceInterface $tyreService,
        FittingServiceInterface $fittingService
    ) {
        return $this->makeResponse(
            $tyreService->find(new FindSelectedSizes($cookies = $request->cookie())),
            $fittingService->getAll($cookies)
        );
    }

    /**
     * @param Collection $selectedSizes
     * @param Collection $fittings
     * @return \Illuminate\Http\JsonResponse
     */
    protected function makeResponse(Collection $selectedSizes, Collection $fittings)
    {
        $html = view('layouts.components.size_select', [
            'selectedSizes' => $selectedSizes,
            'fittings' => $fittings
        ])->render();

        return response()->json(['html' => $html]);
    }
}
