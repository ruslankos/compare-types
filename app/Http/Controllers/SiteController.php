<?php

namespace App\Http\Controllers;

use App\Helpers\DTO\Objects\Prices\Prices\FindPricesBySiteId;
use App\Models\Site;
use App\Services\Site\Repository\RepositoryInterface as SiteRepositoryInterface;
use App\Services\Tyre\Tyre\Storage\Session\SelectedQuantityStorage;
use App\Utiles\Resolver\Dispatcher;

/**
 * Class SiteController
 * @package App\Http\Controllers
 */
class SiteController extends Controller
{
    /**
     * @param SiteRepositoryInterface $siteRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(SiteRepositoryInterface $siteRepository)
    {
        $sites = $siteRepository->all(['relations' => ['link', 'image']]);

        return view('sites.index', ['sites' => $sites]);
    }

    /**
     * @param Dispatcher $dispatcher
     * @param SelectedQuantityStorage $tyreQuantityStorage
     * @param Site $site
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(
        Dispatcher $dispatcher,
        SelectedQuantityStorage $tyreQuantityStorage,
        Site $site
    ) {
        $site->load('image', 'link');
        $prices = $dispatcher->dispatch(new FindPricesBySiteId($site->id));

        return view('sites.view', [
            'site' => $site,
            'prices' => $prices,
            'tyresQuantity' => $tyreQuantityStorage->getQuantity(),
        ]);
    }
}
