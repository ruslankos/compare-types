<?php

namespace App\Http\Controllers;

use App\Commands\Price\Price\FilterPricesByFittingCommand;
use App\Commands\Price\Price\FindPricesByTyreIdAndVouchersCommand;
use App\Commands\Tyre\Tyre\FindTyreByIdCommand;
use App\Models\Brand;
use App\Models\Tyre;
use App\Models\TyreModel;
use App\Utiles\Resolver\Dispatcher;
use Illuminate\Http\Request;

class TyreController extends Controller
{
    /**
     * @param Request $request
     * @param Dispatcher $dispatcher
     * @param Brand $brand
     * @param $model
     * @param $tyre
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(
        Request $request,
        Dispatcher $dispatcher,
        Brand $brand,
        $model,
        $tyre
    ) {
        $tyre = $dispatcher->dispatch(new FindTyreByIdCommand($tyre, $model, $brand->id));
        /**
         * @var Tyre $tyre
         */
        $priceResult = $dispatcher
            ->dispatch(new FindPricesByTyreIdAndVouchersCommand(
                $tyre->id,
                $tyre->model->{TyreModel::VOUCHERS},
                $tyre->getQuantity(),
                $request->cookie()
            ));

        return view('tyres.view', [
            'brand' => $brand,
            'model' => $tyre->model,
            'tyre' => $tyre,
            'prices' => $priceResult['prices'],
            'fittings' => $priceResult['fittings'],
            'range' => $priceResult['range'],
        ]);
    }

    /**
     * @param Request $request
     * @param Dispatcher $dispatcher
     * @param Tyre $tyre
     * @return \Illuminate\Http\JsonResponse
     */
    public function filterPrice(
        Request $request,
        Dispatcher $dispatcher,
        Tyre $tyre
    ) {
        $tyre = $dispatcher
            ->dispatch(new FindTyreByIdCommand($tyre->id, $tyre->model->url_name, $tyre->model->brand_id));

        /**
         * @var Tyre $tyre
         */
        $pricesResult = $dispatcher
            ->dispatch(new FilterPricesByFittingCommand(
                $tyre->id,
                $tyre->model->{TyreModel::VOUCHERS},
                $tyre->getQuantity(),
                $request->input()
            ));

        $html = view(
            'tyres.components.table',
            [
                'prices' => $pricesResult['prices'],
                'tyre' => $tyre,
                'range' => $pricesResult['range']
            ]
        )->render();

        return response()->json(['html' => $html]);
    }
}
