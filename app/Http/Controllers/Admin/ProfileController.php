<?php

namespace App\Http\Controllers\Admin;

use App\EntityManagers\UserManager;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\ChangeProfileInfoRequest;
use App\Utiles\Encryptors\EncryptorInterface;
use App\Traits\Messagable;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    use Messagable;

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * ProfileController constructor.
     * @param UserManager $userManager
     */
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * Render the [rofile page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.profile.view', [
            'user' => Auth::getUser(),
            'message' => $this->getMessage(),

        ]);
    }

    /**
     * Storing of main profile info
     *
     * @param ChangeProfileInfoRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeInfo(ChangeProfileInfoRequest $request)
    {
        $this->userManager->updateById($request->userId, $request->input());
        $this->setMessage($request, 'Success. Info was updated');

        return redirect()->route('admin.profile.index');
    }

    /**
     * Changing the users password
     *
     * @param ChangePasswordRequest $request
     * @param EncryptorInterface $encryptor
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storePassword(ChangePasswordRequest $request, EncryptorInterface $encryptor)
    {
        $user = $this->userManager->find($request->userId);
        $user->password = $encryptor->encrypt($request->password);
        $this->userManager->save($user);
        $this->setMessage($request, 'Success! Password was changed');

        return redirect()->route('admin.profile.index');
    }
}
