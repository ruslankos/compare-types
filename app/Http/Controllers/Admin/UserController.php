<?php

namespace App\Http\Controllers\Admin;

use App\EntityManagers\Interfaces\UserManagerInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserFormRequest;
use App\Models\User;
use App\Repositories\Interfaces\RoleRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Utiles\Encryptors\EncryptorInterface;
use App\Traits\Messagable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    use Messagable;

    /**
     * @var UserManagerInterface
     */
    protected $userManager;

    /**
     * @var UserRepositoryInterface
     */
    protected $userRepository;

    /**
     * @var RoleRepositoryInterface
     */
    protected $roleRepository;

    /**
     * UserController constructor.
     * @param UserManagerInterface $userManager
     * @param UserRepositoryInterface $userRepository
     * @param RoleRepositoryInterface $roleRepository
     */
    public function __construct(
        UserManagerInterface $userManager,
        UserRepositoryInterface $userRepository,
        RoleRepositoryInterface $roleRepository
    ) {
        $this->middleware('permission');
        $this->middleware('not.superadmin')->except('storage', 'index', 'create');
        $this->middleware('not.itself')->except('storage', 'index', 'create');
        $this->userManager = $userManager;
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
    }

    /**
     * Render list of all users
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $currentUser = Auth::getUser();
        $users = $this->userRepository->findBy(
            [
                ['id', '!=', $currentUser->id],
                ['role_id', '!=', 1],
            ],
            [
                'relations' => ['role'],
            ]
        );

        return view('admin.users.index', [
            'users' => $users,
            'message' => $this->getMessage(),
        ]);
    }

    /**
     * Render user create page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $roles = $this->roleRepository->findBy(
            [
                ['name', '!=', 'SuperAdmin']
            ]
        );

        return view('admin.users.create', ['roles' => $roles]);
    }

    /**
     * Generate update page
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(User $user)
    {
        $roles = $this->roleRepository->findBy(
            [
                ['name', '!=', 'SuperAdmin']
            ]
        );

        return view('admin.users.update', ['user' => $user, 'roles' => $roles]);
    }

    /**
     * Store user info at DB
     *
     * @param UserFormRequest $request
     * @param EncryptorInterface $encryptor
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storage(UserFormRequest $request, EncryptorInterface $encryptor)
    {
        $data = $request->input();
        if ($data['password']) {
            $data['password'] = $encryptor->encrypt($data['password']);
        } else {
            unset($data['password']);
        }
        $user = $this->userManager->store($data, $request->user_id);
        $this->setMessage($request, $this->generateStoreMessage('user', $user->name, $request->user_id));

        return redirect()->route('admin.user.main');
    }

    /**
     * Destroy user from DB
     *
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, User $user)
    {
        $this->userManager->delete($user);
        $this->setMessage($request, $this->generateDeleteMessage('user', $user->name));

        return redirect()->route('admin.users.main');
    }
}
