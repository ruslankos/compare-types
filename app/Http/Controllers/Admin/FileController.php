<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\FileUploadRequest;
use App\Traits\Messagable;
use App\Utiles\Files\Uploaders\UploadingStrategy;

class FileController extends Controller
{
    use Messagable;

    /**
     * @param FileUploadRequest $request
     * @param UploadingStrategy $uploadingService
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Services\Uploaders\Exceptions\UploaderException
     */
    public function upload(FileUploadRequest $request, UploadingStrategy $uploadingService)
    {
        $result = $uploadingService->upload($request->target, $request->type, $request->file('file'));
        $this->setMessage(
            $request,
            sprintf(
                'File for %s of %s was uploaded. %s - succeed, %s - failed',
                $request->type,
                $request->target,
                $result['succeed'],
                $result['failed']
            )
        );

        return redirect()->route('admin.main');
    }
}
