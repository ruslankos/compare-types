<?php

namespace App\Http\Controllers\Admin\Voucher;

use App\EntityManagers\Interfaces\VoucherManagerInterface;
use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Voucher;
use App\Services\Tyre\Brand\Brand\Repository\RepositoryInterface as BrandRepositoryInterface;
use App\Services\Voucher\Collectors\ModelsByVoucher;
use App\Traits\Messagable;
use Illuminate\Http\Request;

/**
 * Class ModelController
 * @package App\Http\Controllers\Admin\Voucher
 */
class BrandController extends Controller
{
    use Messagable;

    /**
     * @param BrandRepositoryInterface $brandRepository
     * @param Voucher $voucher
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function refresh(BrandRepositoryInterface $brandRepository, Voucher $voucher)
    {
        $brands = $brandRepository->all(['orderBy' => ['name', 'asc']]);

        $html = view('admin.vouchers.models.components.brands-list', ['brands' => $brands, 'voucher' => $voucher])
            ->render();

        return response()->json([
            'html' => $html
        ]);
    }

    /**
     * @param Request $request
     * @param VoucherManagerInterface $manager
     * @param Voucher $voucher
     * @return \Illuminate\Http\RedirectResponse
     */
    public function clearAll(Request $request, VoucherManagerInterface $manager, Voucher $voucher)
    {
        $manager->sync($voucher, [], Voucher::MODELS);
        $this->setMessage($request, sprintf('Vouchers (%s) models were cleared', $voucher->{Voucher::CODE}));

        return redirect()->route('admin.vouchers.models.view', [
            'voucher' => $voucher
        ]);
    }

    /**
     * @param VoucherManagerInterface $manager
     * @param ModelsByVoucher $collector
     * @param Voucher $voucher
     * @param Brand $brand
     * @return \Illuminate\Http\JsonResponse
     */
    public function clear(
        VoucherManagerInterface $manager,
        ModelsByVoucher $collector,
        Voucher $voucher,
        Brand $brand
    ) {
        $models = $brand->models->pluck('id')->all();
        $manager->detach($voucher, $models, Voucher::MODELS);
        $voucher = $manager->load($voucher, [Voucher::MODELS, sprintf('%s.brand', Voucher::MODELS)]);
        $models = $collector->collect($voucher);

        $html = view('admin.vouchers.models.components.voucher-info', [
            'models' => $models,
            'voucher' => $voucher
        ])->render();

        return response()->json([
            'html' => $html
        ]);
    }

}
