<?php

namespace App\Http\Controllers\Admin\Voucher;

use App\Http\Controllers\Controller;
use App\Http\Requests\VoucherConnectionRequest;
use App\Models\Brand;
use App\Models\Voucher;
use App\Services\Tyre\Brand\Brand\Repository\RepositoryInterface as BrandRepositoryInterface;
use App\Services\Voucher\Collectors\ModelsByBrand;
use App\Services\Voucher\Collectors\ModelsByVoucher;
use App\Services\Voucher\VoucherConnectionHandler;
use App\Traits\Messagable;

/**
 * Class ModelController
 * @package App\Http\Controllers\Admin\Voucher
 */
class ModelController extends Controller
{
    use Messagable;

    /**
     * @param BrandRepositoryInterface $brandRepository
     * @param ModelsByVoucher $collector
     * @param Voucher $voucher
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(BrandRepositoryInterface $brandRepository, ModelsByVoucher $collector, Voucher $voucher)
    {
        $brands = $brandRepository->all(['orderBy' => ['name', 'asc']]);
        $voucher->load(Voucher::MODELS, sprintf('%s.brand', Voucher::MODELS));
        $models = $collector->collect($voucher);

        return view('admin.vouchers.models.view', [
            'brands' => $brands,
            'voucher' => $voucher,
            'models' => $models,
            'message' => $this->getMessage(),
        ]);
    }

    /**
     * @param ModelsByBrand $collector
     * @param Voucher $voucher
     * @param Brand $brand
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(ModelsByBrand $collector, Voucher $voucher, Brand $brand)
    {
        $voucher->load(Voucher::MODELS);
        $brand->load(Brand::ACTIVE_MODELS);
        $models = $collector->collect($voucher, $brand);

        $html = view('admin.vouchers.models.components.model-list', [
            'models' => $models,
            'brand' => $brand
        ])->render();

        return response()->json([
            'html' => $html
        ]);
    }

    /**
     * @param VoucherConnectionRequest $request
     * @param VoucherConnectionHandler $handler
     * @param Voucher $voucher
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(VoucherConnectionRequest $request, VoucherConnectionHandler $handler, Voucher $voucher)
    {
        $models = $handler->handle($voucher, $request->only(['brands', 'models']));

        $html = view('admin.vouchers.models.components.voucher-info', [
            'models' => $models,
            'voucher' => $voucher
        ])->render();

        return response()->json([
            'html' => $html
        ]);
    }
}
