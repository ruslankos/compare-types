<?php

namespace App\Http\Controllers\Admin\Voucher;

use App\EntityManagers\Interfaces\VoucherManagerInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\VoucherFormRequest;
use App\Models\Voucher;
use App\Services\Site\Repository\RepositoryInterface as SiteRepositoryInterface;
use App\Repositories\Interfaces\VoucherRepositoryInterface;
use App\Traits\Messagable;
use Illuminate\Http\Request;

/**
 * Class VoucherController
 * @package App\Http\Controllers\Admin\Voucher
 */
class VoucherController extends Controller
{
    use Messagable;

    /**
     * @var VoucherManagerInterface
     */
    protected $manager;

    /**
     * @var VoucherRepositoryInterface
     */
    protected $repository;

    /**
     * VoucherController constructor.
     * @param VoucherManagerInterface $manager
     * @param VoucherRepositoryInterface $repository
     */
    public function __construct(VoucherManagerInterface $manager, VoucherRepositoryInterface $repository)
    {
        $this->manager = $manager;
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $vouchers = $this->repository->all(['relations' => ['site']]);

        return view('admin.vouchers.resources.index', [
            'vouchers' => $vouchers,
            'message' => $this->getMessage(),
        ]);
    }

    /**
     * @param SiteRepositoryInterface $siteRepository
     * @param Voucher $voucher
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(SiteRepositoryInterface $siteRepository, Voucher $voucher)
    {
        return view('admin.vouchers.resources.update', [
            'sites' => $siteRepository->all(),
            'voucher' => $voucher
        ]);
    }

    /**
     * @param SiteRepositoryInterface $siteRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(SiteRepositoryInterface $siteRepository)
    {
        return view('admin.vouchers.resources.create', [
            'sites' => $siteRepository->all()
        ]);
    }

    /**
     * @param Request $request
     * @param Voucher $voucher
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, Voucher $voucher)
    {
        $this->manager->delete($voucher);
        $this->setMessage($request, $this->generateDeleteMessage('voucher', $voucher->{Voucher::CODE}));

        return redirect()->route('admin.vouchers.main');
    }

    /**
     * @param VoucherFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storage(VoucherFormRequest $request)
    {
        $voucher = $this->manager->store($request->input(), $request->voucher_id);
        $this->setMessage(
            $request,
            $this->generateStoreMessage('voucher', $voucher->{Voucher::CODE}, $request->voucher_id)
        );

        return redirect()->route('admin.vouchers.main');
    }
}
