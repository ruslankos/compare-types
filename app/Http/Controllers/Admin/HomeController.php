<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Site\Repository\RepositoryInterface as SiteRepository;
use App\Utiles\HighCharts\HighChartService;
use App\Traits\Messagable;

class HomeController extends Controller
{
    use Messagable;

    /**
     * SHow the admin main page
     *
     * @param SiteRepository $siteRepository
     * @param HighChartService $chartService
     * @return \Illuminate\Http\Response
     */
    public function index(SiteRepository $siteRepository, HighChartService $chartService)
    {
        $sites = $siteRepository->all(['relations' => ['positiveReports', 'negativeReports']]);

        return view('admin.main', [
            'sites' => $sites,
            'charts' => $chartService->generate($sites),
            'message' => $this->getMessage(),
        ]);
    }

    public function test()
    {
        dd('hello');
    }
}
