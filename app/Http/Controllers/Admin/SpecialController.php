<?php

namespace App\Http\Controllers\Admin;

use App\EntityManagers\Interfaces\SpecialManagerInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\SpecialFormRequest;
use App\Models\Special;
use App\Repositories\Interfaces\SpecialGroupRepositoryInterface;
use App\Repositories\Interfaces\SpecialRepositoryInterface;
use App\Services\Shared\Images\Store\UploadImageStorage;
use App\Traits\Messagable;
use App\Traits\NameGenerator;

class SpecialController extends Controller
{
    use Messagable, NameGenerator;

    /**
     * @var SpecialManagerInterface
     */
    protected $manager;

    /**
     * @var SpecialRepositoryInterface
     */
    protected $specialRepository;

    /**
     * @var SpecialGroupRepositoryInterface
     */
    protected $groupRepository;

    /**
     * SpecialController constructor.
     * @param SpecialManagerInterface $manager
     * @param SpecialRepositoryInterface $specialRepository
     * @param SpecialGroupRepositoryInterface $groupRepository
     */
    public function __construct(
        SpecialManagerInterface $manager,
        SpecialRepositoryInterface $specialRepository,
        SpecialGroupRepositoryInterface $groupRepository
    ) {
        $this->manager = $manager;
        $this->specialRepository = $specialRepository;
        $this->groupRepository = $groupRepository;
    }

    /**
     * Render the list of all specials
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $specials = $this->specialRepository->all(['relations' => ['group', 'image']]);

        return view('admin.specials.index', [
            'specials' => $specials->groupBy('group.name')->sort(),
            'message' => $this->getMessage(),
        ]);
    }

    /**
     * Render the special update page
     *
     * @param Special $special
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Special $special)
    {
        $special->load('group');
        $groups = $this->groupRepository->all();

        return view('admin.specials.update', [
            'special' => $special,
            'groups' => $groups
        ]);
    }

    /**
     * Store the special data into DB
     *
     * @param SpecialFormRequest $request
     * @param UploadImageStorage $imageStorage
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storage(SpecialFormRequest $request, UploadImageStorage $imageStorage)
    {
        $data = $request->input();
        $data['url_name'] = $this->getUrlName($data['name']);
        $special = $this->manager->store($data, $request->special_id);

        if ($request->file('logo')) {
            $imageStorage->store($request->file('logo'), $special);
        }
        $this->setMessage($request, $this->generateStoreMessage('special', $special->name, $request->special_id));

        return redirect()->route('admin.specials.main');
    }
}
