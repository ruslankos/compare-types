<?php

namespace App\Http\Controllers\Admin\Models;

use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\TyreModelRepositoryInterface;

class NoImageController extends Controller
{
    public function view(TyreModelRepositoryInterface $repository)
    {
        return view('admin.models.no-image.view', ['models' => $repository->getWithoutImage()]);
    }
}
