<?php

namespace App\Http\Controllers\Admin\Models;

use App\Http\Controllers\Controller;
use App\Http\Requests\ModelDisplayChangeRequest;
use App\Services\ModelDisplaySwitcher;

class DisplayController extends Controller
{
    public function change(ModelDisplayChangeRequest $request, ModelDisplaySwitcher $switcher)
    {
        $model = $switcher->change(
            (int) $request->{ModelDisplayChangeRequest::MODEL_FIELD},
            $request->{ModelDisplayChangeRequest::TYPE}
        );

        return response()->json([
            'message' => sprintf('%s display mode was changed', $model->name)
        ]);
    }
}
