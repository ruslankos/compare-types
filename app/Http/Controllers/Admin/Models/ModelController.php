<?php

namespace App\Http\Controllers\Admin\Models;

use App\EntityManagers\Interfaces\TyreModelManagerInterface;
use App\Helpers\DTO\Objects\Tyre\Model\StoreModelData;
use App\Http\Controllers\Controller;
use App\Http\Requests\ModelUpdateRequest;
use App\Models\Brand;
use App\Models\Tyre;
use App\Models\TyreModel;
use App\Services\Tyre\Brand\Brand\Repository\RepositoryInterface as BrandRepositoryInterface;
use App\Repositories\Interfaces\TyreModelRepositoryInterface;
use App\Repositories\Interfaces\TyreRepositoryInterface;
use App\Services\Tyre\TyreServiceInterface;
use App\Traits\Messagable;

/**
 * Class ModelController
 * @package App\Http\Controllers\Admin
 */
class ModelController extends Controller
{
    use Messagable;

    /**
     * @var TyreModelManagerInterface
     */
    protected $modelManager;

    /**
     * @var TyreRepositoryInterface
     */
    protected $tyreRepository;

    /**
     * @var TyreModelRepositoryInterface
     */
    protected $modelRepository;

    /**
     * ModelController constructor.
     * @param TyreModelManagerInterface $modelManager
     * @param TyreRepositoryInterface $tyreRepository
     * @param TyreModelRepositoryInterface $modelRepository
     */
    public function __construct(
        TyreModelManagerInterface $modelManager,
        TyreRepositoryInterface $tyreRepository,
        TyreModelRepositoryInterface $modelRepository
    ) {
        $this->modelManager = $modelManager;
        $this->tyreRepository = $tyreRepository;
        $this->modelRepository = $modelRepository;
        $this->middleware('permission')->only(['main', 'index']);
    }

    /**
     * @param BrandRepositoryInterface $brandRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function main(BrandRepositoryInterface $brandRepository)
    {
        $brands = $brandRepository->all(['relations' => [Brand::ACTIVE_MODELS], 'orderBy' => ['name']]);

        return view('admin.models.brand-selection', ['brands' => $brands]);
    }

    /**
     * @param Brand $brand
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Brand $brand)
    {
        $models = $this->modelRepository->findBy(
            [
                [TyreModel::PARENT, '=', null],
                ['brand_id', '=', $brand->id]
            ],
            [
                'relations' => ['image'],
                'orderBy' => ['name', 'asc']
            ]
        );

        return view('admin.models.index', [
            'models' => $models,
            'brand' => $brand,
            'message' => $this->getMessage(),
        ]);
    }

    /**
     * @param Brand $brand
     * @param $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Brand $brand, $model)
    {
        $model = $this->modelRepository->getUnique($brand->id, $model, true);
        $model->load([TyreModel::CHILDREN, TyreModel::CHILDREN . '.brand']);
        $tyres = $this->tyreRepository->findBy(
            [Tyre::REAL_MODEL_ID => $model->id],
            ['relations' => ['specials', 'prices']]
        );

        return view('admin.models.view', [
            'model' => $model,
            'tyres' => $tyres,
            'brand' => $brand,
            'message' => $this->getMessage(),
        ]);
    }

    /**
     * @param Brand $brand
     * @param $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Brand $brand, $model)
    {
        $model = $this->modelRepository->getUnique($brand->id, $model, true);

        return view('admin.models.update', [
            'model' => $model,
            'brand' => $brand,
        ]);
    }

    /**
     * @param ModelUpdateRequest $request
     * @param TyreServiceInterface $tyreService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storage(ModelUpdateRequest $request, TyreServiceInterface $tyreService)
    {
        $model = $tyreService->store(new StoreModelData($request->input(), $request->file('logo')));
        $this->setMessage($request, $this->generateStoreMessage('model', $model->name, $request->model_id));

        return redirect()->route('admin.models.view', ['brand' => $model->brand, 'model' => $model]);
    }
}
