<?php

namespace App\Http\Controllers\Admin\Price\Fitting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Price\Fitting\DisplayOrderUpdateRequest;
use App\Services\Price\Fitting\FittingServiceInterface;

/**
 * Class OrderController
 * @package App\Http\Controllers\Admin\Price\Fitting
 */
class OrderController extends Controller
{
    /**
     * @param DisplayOrderUpdateRequest $request
     * @param FittingServiceInterface $fittingService
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(DisplayOrderUpdateRequest $request, FittingServiceInterface $fittingService)
    {
        $result = $fittingService->storeDisplayOrder(json_decode($request->group, true));

        return response()->json(['message' => $result ? 'success' : 'failed']);
    }
}
