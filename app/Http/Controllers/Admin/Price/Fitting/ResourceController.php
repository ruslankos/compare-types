<?php

namespace App\Http\Controllers\Admin\Price\Fitting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Price\Fitting\StoreFormRequest;
use App\Models\Price\Fitting;
use App\Services\Price\Fitting\FittingServiceInterface;
use App\Traits\Messagable;

class ResourceController extends Controller
{
    use Messagable;

    /**
     * @var FittingServiceInterface
     */
    private $fittingService;

    /**
     * ResourceController constructor.
     * @param FittingServiceInterface $fittingService
     */
    public function __construct(FittingServiceInterface $fittingService)
    {
        $this->fittingService = $fittingService;
    }

    /**
     * Render the list of all price groups
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $fittings = $this->fittingService->getAll();

        return view('admin.prices.fitting.index', [
            'fittings' => $fittings,
            'message' => $this->getMessage(),
        ]);
    }

    /**
     * @param Fitting $fitting
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Fitting $fitting)
    {
        return view('admin.prices.fitting.update', [
            'fitting' => $fitting,
        ]);
    }

    /**
     * @param StoreFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storage(StoreFormRequest $request)
    {
        try {
            $fitting = $this->fittingService->storeData($request->input());
            $this->generateStoreMessage('fitting type', $fitting->name, $request->id);
        } catch (\Exception $exception) {
            $this->setMessage($request, 'Sorry but we cannot update fitting data');
        }

        return redirect()->route('admin.price.fitting.main');
    }
}
