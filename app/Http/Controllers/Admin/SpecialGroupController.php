<?php

namespace App\Http\Controllers\Admin;

use App\EntityManagers\Interfaces\SpecialGroupManagerInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\SpecialGroupFormRequest;
use App\Models\SpecialGroup;
use App\Repositories\Interfaces\SpecialGroupRepositoryInterface;
use App\Traits\Messagable;
use App\Traits\NameGenerator;
use Illuminate\Http\Request;

class SpecialGroupController extends Controller
{
    use Messagable, NameGenerator;

    /**
     * @var SpecialGroupManagerInterface
     */
    protected $manager;

    /**
     * @var SpecialGroupRepositoryInterface
     */
    protected $repository;

    /**
     * SpecialGroupController constructor.
     * @param SpecialGroupRepositoryInterface $repository
     * @param SpecialGroupManagerInterface $manager
     */
    public function __construct(SpecialGroupRepositoryInterface $repository, SpecialGroupManagerInterface $manager)
    {
        $this->repository = $repository;
        $this->manager = $manager;
    }

    /**
     * Render the index page with the list of all special groups
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $groups = $this->repository->all(['relations' => ['specials']]);

        return view('admin.specials_groups.index', [
            'groups' => $groups,
            'message' => $this->getMessage(),
        ]);

    }

    /**
     * Generate special group create page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.specials_groups.create');
    }

    /**
     * Generate special group update page
     *
     * @param SpecialGroup $group
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(SpecialGroup $group)
    {
        return view('admin.specials_groups.update', ['group' => $group]);
    }

    /**
     * Store the special group data into DB
     *
     * @param SpecialGroupFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storage(SpecialGroupFormRequest $request)
    {
        $data = $request->input();
        $data['url_name'] = $this->getUrlName($data['name']);
        $group = $this->manager->store($data, $request->group_id);
        $this->setMessage($request, $this->generateStoreMessage('group', $group->name, $request->group_id));

        return redirect()->route('admin.specials-groups.main');
    }

    /**
     * Destroy the special group
     *
     * @param Request $request
     * @param SpecialGroup $group
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, SpecialGroup $group)
    {
        $this->manager->delete($group);
        $this->setMessage($request, $this->generateDeleteMessage('group', $group->name));

        return redirect()->route('admin.specials-groups.main');
    }
}
