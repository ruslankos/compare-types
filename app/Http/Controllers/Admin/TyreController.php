<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tyre;

class TyreController extends Controller
{
    /**
     * Render tyre page of admin section
     *
     * @param Tyre $tyre
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Tyre $tyre)
    {
        $tyre->load([
            'model',
            'model.brand',
            'model.image',
            'tyresProperties',
            'tyresProperties.property',
            'tyresProperties.value',
            'specials',
            Tyre::REAL_PRICES,
            Tyre::REAL_PRICES . '.site',
            Tyre::REAL_PRICES . '.fitting'
        ]);
        $prices = $tyre->{Tyre::REAL_PRICES}->groupBy('site_id');

        return view('admin.tyres.view', [
            'tyre' => $tyre,
            'prices' => $prices
        ]);
    }
}
