<?php

namespace App\Http\Controllers\Admin;

use App\Services\Shared\Images\Managers\ManagerInterface as ImageManagerInterface;
use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Traits\Messagable;

/**
 * Class ImageController
 * @package App\Http\Controllers\Admin
 */
class ImageController extends Controller
{
    use Messagable;

    /**
     * @param ImageManagerInterface $imageManager
     * @param Image $image
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(ImageManagerInterface $imageManager, Image $image)
    {
        $imageManager->delete($image);

        return redirect()->back();
    }
}
