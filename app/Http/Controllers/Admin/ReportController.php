<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Site;
use App\Services\ParseReportsService;
use App\Traits\Messagable;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    use Messagable;

    /**
     * @var ParseReportsService
     */
    protected $reportService;

    /**
     * ReportController constructor.
     * @param ParseReportsService $parseReportsService
     */
    public function __construct(ParseReportsService $parseReportsService)
    {
        $this->reportService = $parseReportsService;
    }

    /**
     * @param Site $site
     * @param $type
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Site $site, $type)
    {
        return view('admin.reports.view', [
            'site' => $site,
            'reports' => $this->reportService->getBySiteAndType($site, $type),
            'type' => $type,
            'message' => $this->getMessage(),
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        $this->reportService->destroyAll();
        $this->setMessage($request, 'Success! All reports were deleted');

        return redirect()->route('admin.main');

    }

    /**
     * @param Request $request
     * @param Site $site
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroyBySite(Request $request, Site $site)
    {
        $this->reportService->destroyBySite($site);
        $this->setMessage($request, sprintf('Success! All reports of %s were deleted', $site->name));

        return redirect()->route('admin.main');
    }

    /**
     * @param Request $request
     * @param Site $site
     * @param $type
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroyBySiteAndType(Request $request, Site $site, $type)
    {
        $this->reportService->destroyBySiteAndType($site, $type);
        $this->setMessage($request, sprintf('Success!. %s reports of %s were deleted', ucfirst($type), $site->name));

        return redirect()->route('admin.sites.reports.view', [
                'site' => $site->url_name, 'type' => $type
            ]);
    }

    /**
     * @param Request $request
     * @param $type
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroySingle(Request $request, $type, $id)
    {
        $this->reportService->destroyByIdAndType($id, $type);
        $this->setMessage($request, sprintf('Success! Report was deleted'));

        return redirect()->back();
    }
}
