<?php

namespace App\Http\Controllers\Admin\Synonyms;

use App\Http\Controllers\Controller;
use App\Http\Requests\FileUploadRequest;
use App\Utiles\Container\Builder;
use App\Services\RelationsSavers\Interfaces\SnapshotServiceInterface;
use App\Traits\Messagable;
use Illuminate\Http\Request;

class FilterSnapshotController extends Controller
{
    use Messagable;

    /**
     * @var SnapshotServiceInterface
     */
    protected $mapService;

    /**
     * FilterSnapshotController constructor.
     * @param Builder $builder
     */
    public function __construct(Builder $builder)
    {
        $this->mapService = $builder->get('filters.snapshot');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function clear(Request $request)
    {
        $this->mapService->clear();
        $this->setMessage($request, 'Filters were cleared');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request)
    {
        $this->mapService->save();
        $this->setMessage($request, 'Connections were saved');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function rollback(Request $request)
    {
        $this->mapService->rollback();
        $this->setMessage($request, 'Filters were recovered');

        return redirect()->route('admin.synonyms.merge.main');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        $path = $this->mapService->export();

        return response()->download($path);
    }

    /**
     * @param FileUploadRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function import(FileUploadRequest $request)
    {
        $this->mapService->import($request->file('filters'));
        $this->setMessage($request, 'Filters were uploaded');

        return redirect()->route('admin.synonyms.merge.main');
    }
}
