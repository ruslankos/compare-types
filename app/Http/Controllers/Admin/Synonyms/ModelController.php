<?php

namespace App\Http\Controllers\Admin\Synonyms;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Repositories\Interfaces\TyreModelRepositoryInterface;
use App\Services\Synonyms\MergeService;
use App\Services\Synonyms\Selector;
use App\Services\Synonyms\SynonymService;
use App\Traits\Messagable;
use Illuminate\Http\Request;

class ModelController extends Controller
{
    use Messagable;

    /**
     * @var TyreModelRepositoryInterface
     */
    protected $modelRepository;

    /**
     * ModelController constructor.
     * @param TyreModelRepositoryInterface $modelRepository
     */
    public function __construct(TyreModelRepositoryInterface $modelRepository)
    {
        $this->modelRepository = $modelRepository;
    }

    /**
     * @param Selector $selector
     * @param Brand $brand
     * @param $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Selector $selector, Brand $brand, $model)
    {
        $model = $this->modelRepository->getUnique($brand->id, $model, true);
        $model->load(['children']);
        $potentialSynonyms = $selector->select($model->name, $model->id);

        return view('admin.models.synonyms.view', [
            'model' => $model,
            'potentialSynonyms' => $potentialSynonyms,
            'message' => $this->getMessage(),
        ]);
    }

    /**
     * @param Request $request
     * @param SynonymService $service
     * @param $model
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh(Request $request, SynonymService $service, $model)
    {
        $synonyms = $request->input('synonyms');
        $service->connect($model, json_decode($synonyms, true), true);

        return response()->json(['data' => 'Success']);
    }

    /**
     * @param Request $request
     * @param MergeService $mergeService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function add(Request $request, MergeService $mergeService)
    {
        $result = $mergeService->merge($request->parent, $request->child);
        $message = $result ? 'Synonym was added' : 'Cannot add synonym';
        $this->setMessage($request, $message);

        return redirect()->back();
    }
}