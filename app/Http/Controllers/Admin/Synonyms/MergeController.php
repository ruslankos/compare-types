<?php

namespace App\Http\Controllers\Admin\Synonyms;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Services\Tyre\Brand\Brand\Repository\RepositoryInterface as BrandRepositoryInterface;
use App\Repositories\Interfaces\TyreModelRepositoryInterface;
use App\Services\Synonyms\FilterService;
use App\Services\Synonyms\ToolService;
use App\Traits\Messagable;
use Illuminate\Http\Request;

class MergeController extends Controller
{
    use Messagable;

    /**
     * @var BrandRepositoryInterface
     */
    protected $brandRepository;

    /**
     * MergeController constructor.
     * @param BrandRepositoryInterface $brandRepository
     */
    public function __construct(BrandRepositoryInterface $brandRepository)
    {
        $this->brandRepository = $brandRepository;
    }

    /**
     * @param TyreModelRepositoryInterface $modelRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(TyreModelRepositoryInterface $modelRepository)
    {
        $models = $modelRepository->all();
        $baseModels = $models->filter(function ($model) {
            return $model->parent_id === null;
        });
        $brands = $this->brandRepository
            ->all(['orderBy' => ['name', 'asc'], 'relations' => ['displayModels', 'filters']]);

        return view(
            'admin.merge.tool.index',
            [
                'brands' => $brands,
                'models' => $models->count(),
                'baseModels' => $baseModels->count(),
                'message' => $this->getMessage(),
            ]
        );
    }

    /**
     * @param FilterService $selector
     * @param Brand $brand
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(FilterService $selector, Brand $brand)
    {
        $result = $selector->filter($brand);

        return view(
            'admin.merge.tool.view',
            [
                'brand' => $brand,
                'groups' => $result['groups'],
                'unknown' => $result['unknown'],
                'message' => $this->getMessage(),
            ]
        );
    }

    /**
     * @param Request $request
     * @param ToolService $toolService
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request, ToolService $toolService)
    {
        $toolService->handle($request->input('models'));
        $this->setMessage($request, 'Saved...');

        return response()->json(['data' => 'success']);
    }
}
