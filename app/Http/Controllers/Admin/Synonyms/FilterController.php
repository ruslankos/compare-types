<?php

namespace App\Http\Controllers\Admin\Synonyms;

use App\EntityManagers\Interfaces\SynonymFilterManagerInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\SynonymFilterStoreRequest;
use App\Models\Brand;
use App\Models\SynonymFilter;
use App\Services\Tyre\Model\Synonyms\Filters\Storage\FilterStoreService;
use App\Traits\Messagable;
use Illuminate\Http\Request;

class FilterController extends Controller
{
    use Messagable;

    /**
     * @param SynonymFilterStoreRequest $request
     * @param FilterStoreService $store
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SynonymFilterStoreRequest $request, FilterStoreService $store)
    {
        $store->store($request->input(), $request->filter_id);
        $message = $this->generateStoreMessage('filter', $request->filter, $request->filter_id);
        $this->setMessage($request, $message);

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param SynonymFilterManagerInterface $filterManager
     * @param SynonymFilter $filter
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, SynonymFilterManagerInterface $filterManager, SynonymFilter $filter)
    {
        $filterManager->delete($filter);
        $message = $this->generateDeleteMessage('filter', $filter->name);
        $this->setMessage($request, $message);

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param SynonymFilterManagerInterface $filterManager
     * @param Brand $brand
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroyByBrand(Request $request, SynonymFilterManagerInterface $filterManager, Brand $brand)
    {
        $filterManager->deleteSeveral($brand->filters);
        $message = sprintf('Filters for %s brand were deleted', $brand->name);
        $this->setMessage($request, $message);

        return redirect()->back();
    }
}