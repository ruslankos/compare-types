<?php

namespace App\Http\Controllers\Admin\Synonyms;

use App\Http\Controllers\Controller;
use App\Http\Requests\FileUploadRequest;
use App\Repositories\Interfaces\TyreModelRepositoryInterface;
use App\Utiles\Container\Builder;
use App\Services\RelationsSavers\Interfaces\SnapshotServiceInterface;
use App\Traits\Messagable;
use Illuminate\Http\Request;

class MapController extends Controller
{
    use Messagable;

    /**
     * @var SnapshotServiceInterface
     */
    protected $mapService;

    /**
     * MapController constructor.
     * @param Builder $builder
     */
    public function __construct(Builder $builder)
    {
        $this->mapService = $builder->get('synonyms.snapshot');
    }

    /**
     * @param TyreModelRepositoryInterface $repository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(TyreModelRepositoryInterface $repository)
    {
        $models = $repository->all(['relations' => ['children', 'brand'], 'has' => 'children']);

        return view('admin.merge.map.view', [
            'models' => $models,
            'message' => $this->getMessage(),
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function clear(Request $request)
    {
        $this->mapService->clear();
        $this->setMessage($request, 'Connections were cleared');

        return redirect()->route('admin.synonyms.map.view');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(Request $request)
    {
        $this->mapService->save();
        $this->setMessage($request, 'Connections were saved');

        return redirect()->route('admin.synonyms.map.view');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function rollback(Request $request)
    {
        $this->mapService->rollback();
        $this->setMessage($request, 'Connections were recovered');

        return redirect()->route('admin.synonyms.map.view');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        $path = $this->mapService->export();

        return response()->download($path);
    }

    /**
     * @param FileUploadRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function import(FileUploadRequest $request)
    {
        $this->mapService->import($request->file('synonyms'));
        $this->setMessage($request, 'Connections were uploaded');

        return redirect()->route('admin.synonyms.map.view');
    }
}
