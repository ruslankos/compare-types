<?php

namespace App\Http\Controllers\Admin\Location;

use App\EntityManagers\Interfaces\RegionManagerInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegionFormRequest;
use App\Models\Location\Region;
use App\Traits\Messagable;
use App\Traits\NameGenerator;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    use Messagable, NameGenerator;

    /**
     * @var RegionManagerInterface
     */
    protected $manager;

    /**
     * RegionController constructor.
     * @param RegionManagerInterface $manager
     */
    public function __construct(RegionManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Render region creation page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.location.region.create');
    }

    /**
     * Render region updating page
     *
     * @param Region $region
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Region $region)
    {
        return view('admin.location.region.update', ['region' => $region]);
    }

    /**
     * Delete region from DB
     *
     * @param Request $request
     * @param Region $region
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, Region $region)
    {
        $this->manager->delete($region);
        $this->setMessage($request, $this->generateDeleteMessage('region', $region->name));

        return redirect()->route('admin.location.view');
    }

    /**
     * Store region data into DB
     *
     * @param RegionFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storage(RegionFormRequest $request)
    {
        $data = $request->input();
        $data['url_name'] = $this->getUrlName($data['name']);
        $region = $this->manager->store($data, $request->region_id);
        $this->setMessage($request, $this->generateStoreMessage('region', $region->name, $request->region_id));

        return redirect()->route('admin.location.view');
    }
}
