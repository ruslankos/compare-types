<?php

namespace App\Http\Controllers\Admin\Location;

use App\EntityManagers\Interfaces\RegionManagerInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\LocationUploadRequest;
use App\Repositories\Interfaces\RegionRepositoryInterface;
use App\Traits\Messagable;
use App\Utiles\Files\Uploaders\Builder;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    use Messagable;

    /**
     * @var RegionRepositoryInterface
     */
    protected $regionRepository;

    /**
     * @var RegionManagerInterface
     */
    protected $regionManager;

    /**
     * LocationController constructor.
     * @param RegionRepositoryInterface $repository
     * @param RegionManagerInterface $manager
     */
    public function __construct(RegionRepositoryInterface $repository, RegionManagerInterface $manager)
    {
        $this->regionRepository = $repository;
        $this->regionManager = $manager;
    }

    /**
     * Render general location info
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $regions = $this->regionRepository->all([
            'relations' => ['counties', 'towns', 'counties.towns', 'counties.towns.addresses']
        ]);

        return view('admin.location.index', [
            'regions' => $regions,
            'message' => $this->getMessage()
        ]);
    }

    /**
     * Import location data from uploaded file
     *
     * @param LocationUploadRequest $request
     * @param Builder $builder
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LocationUploadRequest $request, Builder $builder)
    {
        $handler = $builder->build('location');
        $result = $handler->handle($request->file('file'));
        $message = sprintf(
            'File was uploaded: %d locations were imported, %d - failed!',
            $result['succeed'],
            $result['failed']
        );

        $this->setMessage($request, $message);

        return redirect()->route('admin.location.view');
    }

    /**
     * Destroy all location info
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        $regions = $this->regionRepository->all();
        foreach ($regions as $region) {
            $this->regionManager->delete($region);
        }

        $this->setMessage($request, 'All locations were deleted');

        return redirect()->route('admin.location.view');
    }
}
