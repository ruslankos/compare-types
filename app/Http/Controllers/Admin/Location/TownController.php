<?php

namespace App\Http\Controllers\Admin\Location;

use App\EntityManagers\Interfaces\TownManagerInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\TownFormRequest;
use App\Models\Location\Town;
use App\Services\Location\County\Repository\RepositoryInterface as  CountyRepositoryInterface;
use App\Traits\Messagable;
use App\Traits\NameGenerator;
use Illuminate\Http\Request;

class TownController extends Controller
{
    use Messagable, NameGenerator;

    /**
     * @var CountyRepositoryInterface
     */
    protected $countyRepository;

    /**
     * @var TownManagerInterface
     */
    protected $manager;

    /**
     * TownController constructor.
     * @param CountyRepositoryInterface $repository
     * @param TownManagerInterface $manager
     */
    public function __construct(CountyRepositoryInterface $repository, TownManagerInterface $manager)
    {
        $this->countyRepository = $repository;
        $this->manager = $manager;
    }

    /**
     * Render town create page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $counties = $this->countyRepository->all(['orderBy' => ['name']]);

        return view('admin.location.town.create', ['counties' => $counties]);
    }

    /**
     * Render town update page
     *
     * @param Town $town
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Town $town)
    {
        $counties = $this->countyRepository->all(['orderBy' => ['name']]);

        return view('admin.location.town.update', ['counties' => $counties, 'town' => $town]);
    }

    /**
     * Delete town from DB
     *
     * @param Request $request
     * @param Town $town
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, Town $town)
    {
        $this->manager->delete($town);
        $this->setMessage($request, $this->generateDeleteMessage('town', $town->name));

        return redirect()->route('admin.location.view');
    }

    /**
     * Store town data into DB
     *
     * @param TownFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storage(TownFormRequest $request)
    {
        $data = $request->input();
        $data['url_name'] = $this->getUrlName($data['name']);
        $town = $this->manager->store($data, $request->town_id);
        $this->setMessage($request, $this->generateStoreMessage('town', $town->name, $request->town_id));

        return redirect()->route('admin.location.view');
    }
}
