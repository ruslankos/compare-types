<?php

namespace App\Http\Controllers\Admin\Location;

use App\EntityManagers\Interfaces\AddressManagerInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddressFormRequest;
use App\Models\Location\Address;
use App\Models\Location\Town;
use App\Repositories\Interfaces\AddressRepositoryInterface;
use App\Repositories\Interfaces\TownRepositoryInterface;
use App\Traits\Messagable;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    use Messagable;

    /**
     * @var AddressRepositoryInterface
     */
    protected $townRepository;

    /**
     * @var AddressManagerInterface
     */
    protected $manager;

    /**
     * AddressController constructor.
     * @param TownRepositoryInterface $repository
     * @param AddressManagerInterface $manager
     */
    public function __construct(TownRepositoryInterface $repository, AddressManagerInterface $manager)
    {
        $this->townRepository = $repository;
        $this->manager = $manager;
    }

    /**
     * Render list of the addresses of the given town
     *
     * @param Town $town
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Town $town)
    {
        $town->load('addresses');

        return view('admin.location.address.view', ['town' => $town, 'message' => $this->getMessage()]);
    }

    /**
     * Render the creation form for address
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $towns = $this->townRepository->all(['orderBy' => ['name']]);

        return view('admin.location.address.create', ['towns' => $towns]);
    }

    /**
     * Render the form for address updating
     *
     * @param Address $address
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Address $address)
    {
        $towns = $this->townRepository->all(['orderBy' => ['name']]);

        return view('admin.location.address.update', ['towns' => $towns, 'address' => $address]);
    }

    /**
     * Delete given address from DB
     *
     * @param Request $request
     * @param Address $address
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, Address $address)
    {
        $this->manager->delete($address);
        $this->setMessage($request, sprintf('Address of %s was deleted', $address->town->name));

        return redirect()->route('admin.location.address.view', ['town' => $address->town]);
    }

    /**
     * Save address info into DB
     *
     * @param AddressFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storage(AddressFormRequest $request)
    {
        $address = $this->manager->store($request->input(), $request->address_id);
        $this->setMessage(
            $request,
            sprintf(
                'Address for %s was %s',
                $address->town->name,
                $request->address_id ? 'updated' : 'created'
            )
        );

        return redirect()->route('admin.location.address.view', ['town' => $address->town]);
    }

    /**
     * Render address creation form with selected town
     *
     * @param Request $request
     * @param Town $town
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createFor(Request $request, Town $town)
    {
        $towns = $this->townRepository->all(['orderBy' => ['name']]);

        return view('admin.location.address.create', ['towns' => $towns, 'town' => $town]);
    }

    /**
     * Delete all addresses of given town from DB
     *
     * @param Request $request
     * @param Town $town
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteFor(Request $request, Town $town)
    {
        $town->load('addresses');

        foreach ($town->addresses as $address) {
            $this->manager->delete($address);
        }
        $this->setMessage($request, sprintf('Addresses for %s were deleted', $town->name));

        return redirect()->route('admin.location.address.view', ['town' => $town]);
    }
}
