<?php

namespace App\Http\Controllers\Admin\Location;

use App\EntityManagers\Interfaces\CountyManagerInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\CountyFormRequest;
use App\Models\Location\County;
use App\Repositories\Interfaces\RegionRepositoryInterface;
use App\Traits\Messagable;
use App\Traits\NameGenerator;
use Illuminate\Http\Request;

class CountyController extends Controller
{
    use Messagable, NameGenerator;

    /**
     * @var CountyManagerInterface
     */
    protected $manager;

    /**
     * @var RegionRepositoryInterface
     */
    protected $regionRepository;

    /**
     * CountyController constructor.
     * @param CountyManagerInterface $manager
     * @param RegionRepositoryInterface $regionRepository
     */
    public function __construct(CountyManagerInterface $manager, RegionRepositoryInterface $regionRepository)
    {
        $this->manager = $manager;
        $this->regionRepository = $regionRepository;
    }

    /**
     * Render county creation page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $regions = $this->regionRepository->all(['orderBy' => ['name']]);

        return view('admin.location.county.create', ['regions' => $regions]);
    }

    /**
     * Render county updating page
     *
     * @param County $county
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(County $county)
    {
        $regions = $this->regionRepository->all(['orderBy' => ['name']]);

        return view('admin.location.county.update', [
            'county' => $county,
            'regions' => $regions
        ]);
    }

    /**
     * Delete county from DB
     *
     * @param Request $request
     * @param County $county
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, County $county)
    {
        $this->manager->delete($county);
        $this->setMessage($request, $this->generateDeleteMessage('county', $county->name));

        return redirect()->route('admin.location.view');
    }

    /**
     * Store county data into DB
     *
     * @param CountyFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storage(CountyFormRequest $request)
    {
        $data = $request->input();
        $data['url_name'] = $this->getUrlName($data['name']);
        $county = $this->manager->store($data, $request->county_id);
        $this->setMessage($request, $this->generateStoreMessage('county', $county->name, $request->county_id));

        return redirect()->route('admin.location.view');
    }
}
