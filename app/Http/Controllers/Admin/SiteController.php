<?php

namespace App\Http\Controllers\Admin;

use App\Commands\Shared\Link\FindSkimlinksList;
use App\Helpers\DTO\Objects\Site\StoreSiteData;
use App\Services\Site\Managers\EntityManagerInterface as SiteManager;
use App\Http\Controllers\Controller;
use App\Http\Requests\SiteFormRequest;
use App\Models\Site;
use App\Services\Site\Repository\RepositoryInterface as SiteRepository;
use App\Traits\Messagable;
use App\Utiles\Resolver\Dispatcher;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    use Messagable;

    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * @var SiteManager
     */
    protected $siteManager;

    /**
     * @var SiteRepository
     */
    protected $siteRepository;

    /**
     * SiteController constructor.
     * @param Dispatcher $dispatcher
     * @param SiteManager $siteManager
     * @param SiteRepository $siteRepository
     */
    public function __construct(Dispatcher $dispatcher, SiteManager $siteManager, SiteRepository $siteRepository)
    {
        $this->dispatcher = $dispatcher;
        $this->siteManager = $siteManager;
        $this->siteRepository = $siteRepository;
    }

    /**
     * Render list of the sites
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.sites.index', [
            'sites' => $this->siteRepository->all(['relations' => ['image', 'link']]),
            'message' => $this->getMessage(),
        ]);
    }

    /**
     * Render view of the site
     *
     * @param Site $site
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Site $site)
    {
        $site->load('image');

        return view('admin.sites.view', ['site' => $site]);
    }

    /**
     * Render new site creation form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view(
            'admin.sites.create',
            [
                'skimlinks' => $this->dispatcher->dispatch(new FindSkimlinksList())
            ]
        );
    }

    /**
     * Render site editing form
     *
     * @param Site $site
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Site $site)
    {
        return view('admin.sites.update', [
            'site' => $site,
            'skimlinks' => $this->dispatcher->dispatch(new FindSkimlinksList())
        ]);
    }

    /**
     * Remove site from db
     *
     * @param $request
     * @param Site $site
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, Site $site)
    {
        $this->siteManager->delete($site);
        $this->setMessage($request, $this->generateDeleteMessage('site', $site->name));

        return redirect()->route('admin.sites.main');
    }

    /**
     * @param SiteFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storage(SiteFormRequest $request)
    {
        $site = $this->dispatcher->dispatch(new StoreSiteData($request->input(), $request->file('logo')));
        $this->setMessage($request, $this->generateStoreMessage('site', $site->name, $request->site_id));

        return redirect()->route('admin.sites.main');
    }
}
