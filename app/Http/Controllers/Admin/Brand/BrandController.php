<?php

namespace App\Http\Controllers\Admin\Brand;

use App\Helpers\DTO\Objects\Tyre\Brand\StoreBrandData;
use App\Services\Tyre\Brand\Brand\Managers\ManagerInterface as BrandManagerInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\BrandRequest;
use App\Models\Brand;
use App\Repositories\Interfaces\BrandGroupRepositoryInterface;
use App\Services\Tyre\Brand\Brand\Repository\RepositoryInterface as BrandRepositoryInterface;
use App\Services\Shared\Images\Store\UploadImageStorage;
use App\Services\Tyre\TyreServiceInterface;
use App\Traits\Messagable;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    use Messagable;

    /**
     * @var BrandManagerInterface
     */
    protected $brandManager;

    /**
     * @var BrandRepositoryInterface
     */
    protected $brandRepository;

    /**
     * @var BrandGroupRepositoryInterface
     */
    protected $groupRepository;

    /**
     * BrandController constructor.
     * @param BrandManagerInterface $brandManager
     * @param BrandRepositoryInterface $brandRepository
     * @param BrandGroupRepositoryInterface $groupRepository
     */
    public function __construct(
        BrandManagerInterface $brandManager,
        BrandRepositoryInterface $brandRepository,
        BrandGroupRepositoryInterface $groupRepository
    ) {
        $this->brandManager = $brandManager;
        $this->brandRepository = $brandRepository;
        $this->groupRepository = $groupRepository;
    }

    /**
     * Brands list render
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $brands = $this->brandRepository->all(['relations' => ['group', 'image']]);

        return view('admin.brands.index', [
            'brands' => $brands->groupBy('group.name')->sort(),
            'message' => $this->getMessage(),
        ]);
    }


    /**
     * Brand update page rendering
     *
     * @param Brand $brand
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Brand $brand)
    {
        $brand->load('group');
        $groups = $this->groupRepository->all();

        return view('admin.brands.update', ['brand' => $brand, 'groups' => $groups]);
    }

    /**
     * Brand creating page rendering
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $groups = $this->groupRepository->all();

        return view('admin.brands.create', ['groups' => $groups]);
    }

    /**
     * @param BrandRequest $request
     * @param TyreServiceInterface $tyreService
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storage(BrandRequest $request, TyreServiceInterface $tyreService)
    {
        $brand = $tyreService->store(new StoreBrandData($request->input(), $request->file('logo')));
        $this->setMessage($request, $this->generateStoreMessage('manufacturer', $brand->name, $request->brand_id));

        return redirect()->route('admin.brands.main');
    }

    /**
     * Brand deleting
     *
     * @param Request $request
     * @param Brand $brand
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, Brand $brand)
    {
        $this->brandManager->delete($brand);
        $this->setMessage($request, $this->generateDeleteMessage('manufacturer', $brand->name));

        return redirect()->route('admin.brands.main');
    }
}
