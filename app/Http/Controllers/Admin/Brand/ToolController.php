<?php

namespace App\Http\Controllers\Admin\Brand;

use App\Http\Controllers\Controller;
use App\Http\Requests\FileUploadRequest;
use App\Utiles\Container\Builder;
use App\Services\RelationsSavers\Importers\Services\BrandGroupService;
use App\Services\RelationsSavers\Interfaces\SnapshotServiceInterface;
use App\Traits\Messagable;
use Illuminate\Http\Request;

class ToolController extends Controller
{
    use Messagable;

    /**
     * @var SnapshotServiceInterface
     */
    protected $tool;

    /**
     * ToolController constructor.
     * @param Builder $builder
     */
    public function __construct(Builder $builder)
    {
        $this->tool = $builder->get('brand.groups');
    }

    /**
     * @param BrandGroupService $service
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(BrandGroupService $service)
    {
        $groups = $service->getList();

        return view('admin.brands.tool.view', ['groups' => $groups, 'message' => $this->getMessage()]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request, BrandGroupService $service)
    {
        $service->saveGroups(json_decode($request->groups, true));
        $this->tool->save();
        $this->setMessage($request, 'Manufacturers-Groups connections were updated');

        return response()->json(['url' => route('admin.brand-groups.tool.view')]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function clear(Request $request)
    {
        $this->tool->clear();
        $this->setMessage($request, 'All connections were removed from DB');

        return redirect()->route('admin.brand-groups.tool.view');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function rollback(Request $request)
    {
        $this->tool->rollback();
        $this->setMessage($request, 'Connections were refreshed');

        return redirect()->route('admin.brand-groups.tool.view');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(Request $request)
    {
        $path = $this->tool->export();
        if (! $path) {
            $this->setMessage($request, 'Sorry. But export file was not found');
            return redirect()->route('admin.brand-groups.tool.view');
        }

        return response()->download($path);
    }

    /**
     * @param FileUploadRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function import(FileUploadRequest $request)
    {
        $this->tool->import($request->file('brand-groups'));
        $this->setMessage($request, 'Brand groups were uploaded');

        return redirect()->route('admin.brand-groups.tool.view');
    }
}
