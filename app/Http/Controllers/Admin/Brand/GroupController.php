<?php

namespace App\Http\Controllers\Admin\Brand;

use App\EntityManagers\Interfaces\BrandGroupManagerInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\BrandGroupRequest;
use App\Models\BrandGroup;
use App\Repositories\Interfaces\BrandGroupRepositoryInterface;
use App\Traits\Messagable;
use App\Traits\NameGenerator;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    use Messagable, NameGenerator;

    /**
     * @var BrandGroupManagerInterface
     */
    protected $manager;

    /**
     * @var BrandGroupRepositoryInterface
     */
    protected $repository;

    /**
     * BrandGroupController constructor.
     * @param BrandGroupManagerInterface $manager
     * @param BrandGroupRepositoryInterface $repository
     */
    public function __construct(
        BrandGroupManagerInterface $manager,
        BrandGroupRepositoryInterface $repository
    ) {
        $this->manager = $manager;
        $this->repository = $repository;
    }

    /**
     * BrandsGroups list render
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.brands.group.index', [
            'groups' => $this->repository->all(['relations' => ['brands']]),
            'message' => $this->getMessage(),
        ]);
    }


    /**
     * BrandGroup update page rendering
     *
     * @param BrandGroup $brandGroup
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(BrandGroup $brandGroup)
    {
        return view('admin.brands.group.update', ['group' => $brandGroup]);
    }

    /**
     * BrandGroup creating page rendering
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.brands.group.create');
    }

    /**
     * Storage of brand group data at DB
     *
     * @param BrandGroupRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storage(BrandGroupRequest $request)
    {
        $data = $request->input();
        $data['url_name'] = $this->getUrlName($data['name']);
        $group = $this->manager->store($data, $request->group_id);

        $this->setMessage($request, $this->generateStoreMessage('brand group', $group->name, $request->group_id));

        return redirect()->route('admin.brand-groups.main');
    }

    /**
     * Brand group deleting
     *
     * @param Request $request
     * @param BrandGroup $brandGroup
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, BrandGroup $brandGroup)
    {
        $this->manager->delete($brandGroup);
        $this->setMessage($request, $this->generateDeleteMessage('brand group', $brandGroup->name));

        return redirect()->route('admin.brand-groups.main');
    }
}
