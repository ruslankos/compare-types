<?php

namespace App\Http\Controllers;

use App\Services\Tyre\TyreServiceInterface;
use App\Utiles\Paginator\Paginator;
use Illuminate\Http\Request;
use App\Utiles\DTO\Factory as AssemblersFactory;

class FilterController extends Controller
{
    /**
     * @var Paginator
     */
    private $paginator;

    /**
     * FilterController constructor.
     * @param Paginator $paginator
     */
    public function __construct(Paginator $paginator)
    {
        $this->middleware('search')->only('filter');
        $this->paginator = $paginator;
    }

    /**
     * @param Request $request
     * @param TyreServiceInterface $tyreService
     * @param AssemblersFactory $factory
     * @return \Illuminate\Http\JsonResponse
     */
    public function filter(
        Request $request,
        TyreServiceInterface $tyreService,
        AssemblersFactory $factory
    ) {
        $assembler = $factory->buildAssembler('search-by-size');
        $tyres = $tyreService->find($assembler->build($request->except('page')));
        $paginatedTyres = $this->paginator
            ->paginate($tyres, route('search.size'), $request->query(), 24, $request->query('page'));

        $html = view('search.search_tyres_list', [
            'tyres' => $paginatedTyres,
            'filters' => [],
        ])->render();

        return response()->json(['html' => $html]);
    }
}
