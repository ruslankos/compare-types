<?php

namespace App\Http\Controllers;

use App\Models\Location\County;
use App\Models\Location\Region;
use App\Models\Location\Town;
use App\Repositories\Interfaces\RegionRepositoryInterface;

class LocationController extends Controller
{
    /**
     * @var RegionRepositoryInterface
     */
    protected $regionRepository;

    /**
     * LocationController constructor.
     * @param RegionRepositoryInterface $regionRepository
     */
    public function __construct(RegionRepositoryInterface $regionRepository)
    {
        $this->regionRepository = $regionRepository;
    }

    /**
     * Render page with list of regions
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewRegions()
    {
        $regions = $this->regionRepository->all(['relations' => ['counties', 'towns']]);

        return view('location.regions', ['regions' => $regions]);
    }

    /**
     * Render page with list of counties
     *
     * @param Region $region
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewCounties(Region $region)
    {
        $region->load(['counties', 'counties.towns']);

        return view('location.counties', ['region' => $region]);

    }

    /**
     * Render page with list of towns
     *
     * @param Region $region
     * @param County $county
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewTowns(Region $region, County $county)
    {
        $county->load('towns');

        return view('location.towns', ['region' => $region, 'county' => $county]);
    }

    /**
     * Render page with single town info
     *
     * @param Region $region
     * @param County $county
     * @param Town $town
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewTown(Region $region, County $county, Town $town)
    {
        $town->load('addresses');

        return view('location.town', [
            'region' => $region,
            'county' => $county,
            'town' => $town,
        ]);
    }
}
