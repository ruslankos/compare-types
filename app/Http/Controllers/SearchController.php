<?php

namespace App\Http\Controllers;

use App\Helpers\DTO\Objects\Tyre\Search\FindFiltersForSearchResult;
use App\Helpers\DTO\Objects\Tyre\Search\FindTyresForAutocomplete;
use App\Helpers\DTO\Objects\Tyre\Search\SearchByVRM;
use App\Helpers\DTO\Objects\Tyre\Search\SearchModelByName;
use App\Http\Requests\RegistrationSearchRequest;
use App\Http\Requests\SearchFormRequest;
use App\Http\Resources\TyreModelResource;
use App\Models\Tyre;
use App\Models\TyreModel;
use App\Services\Tyre\Tyre\Search\Templates\Objects\Filters;
use App\Utiles\DTO\Factory as AssemblersFactory;
use App\Services\Tyre\TyreServiceInterface;
use App\Utiles\Paginator\Paginator;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class SearchController extends Controller
{
    /**
     * SearchController constructor.
     */
    public function __construct()
    {
        $this->middleware('search')->only('search');
    }

    /**
     * @param Request $request
     * @param TyreServiceInterface $tyreService
     * @param AssemblersFactory $factory
     * @param Paginator $paginator
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function search(
        Request $request,
        TyreServiceInterface $tyreService,
        AssemblersFactory $factory,
        Paginator $paginator
    ) {
        $assembler = $factory->buildAssembler('search-by-size');
        $searchObject = $assembler->build($request->except('filters', 'page'));
        /**
         * @var Collection $tyres
         */
        $tyres = $tyreService->find($searchObject);
        if (count($tyres) === 1) {
            return $this->redirectToTyrePage($tyres->first());
        }
        $paginatedTyres = $paginator->paginate(
            $tyres,
            $request->url(),
            $request->query(),
            24,
            $request->query('page')
        );

        /**
         * @var Filters $filters
         */
        $filters = $tyreService->find(new FindFiltersForSearchResult($searchObject, $tyres));

        return view('search.search_result', [
            'tyres' => $paginatedTyres,
            'filters' => $filters->getBrandFilters(),
            'specials' => $filters->getSpecials(),
            'query' => http_build_query($request->except('filters')),
            'selectedSizes' => $filters->getSizes(),
            'sortDropdown' => $filters->getSortDropdown(),
        ]);
    }

    /**
     * @param SearchFormRequest $request
     * @param TyreServiceInterface $tyreService
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchByTyreName(SearchFormRequest $request, TyreServiceInterface $tyreService)
    {
        /**
         * @var LengthAwarePaginator $models
         */
        $models = $tyreService->find(new SearchModelByName($request->input('search')));
        if (count($models) === 1) {
            $this->redirectToModelPage($models->first());
        }
        $models->appends(['search' => $request->input('search')])->links();

        return view('search.models_search_result', [
            'models' => $models,
        ]);
    }

    /**
     * @param RegistrationSearchRequest $request
     * @param TyreServiceInterface $tyreService
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchByRegistration(RegistrationSearchRequest $request, TyreServiceInterface $tyreService)
    {
        $result = $tyreService->find(new SearchByVRM($request->input('registration')));

        return view('search.registration_search_result', [
            'result' => $result,
            'registration' => $request->input('registration')
        ]);
    }

    /**
     * @param Request $request
     * @param TyreServiceInterface $tyreService
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function autocompleteByTyreName(Request $request, TyreServiceInterface $tyreService)
    {
        $models = $tyreService->find(new FindTyresForAutocomplete($request->input('search')));

        if (! count($models)) {
            $result[0]['message'] = 'We cannot find any tyre by your request';
            return response()->json(['data' => $result]);
        }

        return TyreModelResource::collection($models);
    }

    /**
     * @param Tyre $tyre
     * @return \Illuminate\Http\RedirectResponse
     */
    private function redirectToTyrePage(Tyre $tyre)
    {
        return redirect()->route('tyres.view', [
            'brand' => $tyre->model->brand,
            'model' => $tyre->model,
            'tyre' => $tyre
        ]);
    }

    /**
     * @param TyreModel $model
     * @return \Illuminate\Http\RedirectResponse
     */
    private function redirectToModelPage(TyreModel $model)
    {
        return redirect()->route('model.view', [
            'brand' => $model->brand,
            'model' => $model->url_name,
        ]);
    }
}
