<?php

namespace App\Http\Controllers;

use App\Commands\Price\Price\FindBestPricesCommand;
use App\Commands\Site\FindSiteForMainPageCommand;
use App\Commands\Tyre\Brand\FindBrandsForMainPageCommand;
use App\Commands\Tyre\Model\FindTopModelsForMainPageCommand;
use App\Services\Tyre\Tyre\Storage\Session\SelectedQuantityStorage;
use App\Utiles\Resolver\Dispatcher;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @param Request $request
     * @param Dispatcher $dispatcher
     * @param SelectedQuantityStorage $tyreQuantityStorage
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(
        Request $request,
        Dispatcher $dispatcher,
        SelectedQuantityStorage $tyreQuantityStorage
    ) {


        $sites = $dispatcher->dispatch(new FindSiteForMainPageCommand(12));
        $manufacturers = $dispatcher->dispatch(new FindBrandsForMainPageCommand(24));
        $prices = $dispatcher->dispatch(new FindBestPricesCommand(24, $request->cookie()));
        $models = $dispatcher->dispatch(new FindTopModelsForMainPageCommand(24));

        return view('home', [
            'sites' => $sites,
            'manufacturers' => $manufacturers,
            'models' => $models,
            'prices' => $prices,
            'tyresQuantity' => $tyreQuantityStorage->getQuantity(),
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function explain()
    {
        return view('general.how-its-works');
    }
}
