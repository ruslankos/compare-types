<?php

namespace App\Http\Controllers;

use App\Helpers\DTO\Objects\Shared\Link\FindSkimlink;
use App\Services\Shared\Link\LinkServiceInterface;
use App\Utiles\Resolver\Dispatcher;

class LinkController extends Controller
{
    /**
     * @param Dispatcher $dispatcher
     * @param $link
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirect(Dispatcher $dispatcher, $link)
    {
        $path = $dispatcher->dispatch(new FindSkimlink($link));

        return redirect($path);
    }
}
