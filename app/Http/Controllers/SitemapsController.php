<?php

namespace App\Http\Controllers;

use App\Services\Location\LocationServiceInterface;
use App\Services\Site\SiteServiceInterface;
use App\Utiles\Sitemaps\Service as SitemapService;
use App\Utiles\Sitemaps\Helper;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SitemapsController extends Controller
{
    /**
     * @param Helper $helper
     * @param LocationServiceInterface $locationService
     * @param SiteServiceInterface $siteService
     * @param SitemapService $service
     * @return mixed
     */
    public function index(
        Helper $helper,
        LocationServiceInterface $locationService,
        SiteServiceInterface $siteService,
        SitemapService $service
    ) {
        try {
            $service->generateSitemaps();
            $locationService->generateSitemaps();
            $siteService->generateSitemaps();

            return $helper->render();
        } catch (\Exception $exception) {
            throw new NotFoundHttpException();
        }
    }
}
