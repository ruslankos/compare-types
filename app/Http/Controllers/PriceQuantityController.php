<?php

namespace App\Http\Controllers;

use App\Http\Requests\TyreQuantityRequest;
use App\Services\Tyre\Tyre\Storage\Session\SelectedQuantityStorage;

class PriceQuantityController extends Controller
{
    /**
     * @param TyreQuantityRequest $request
     * @param SelectedQuantityStorage $quantityStorage
     * @return \Illuminate\Http\JsonResponse
     */
    public function set(TyreQuantityRequest $request, SelectedQuantityStorage $quantityStorage)
    {
        $quantityStorage->setQuantity($request->value);

        return response()->json(['message' => 'Success']);
    }
}
