<?php

namespace App\Http\Controllers;

use App\Helpers\DTO\Objects\Prices\Fitting\FindSelectedFitting;
use App\Helpers\DTO\Objects\Tyre\Model\FindModelByBrandIdAndAlias;
use App\Models\Brand;
use App\Models\TyreModel;
use App\Services\Price\Fitting\FittingServiceInterface;
use App\Services\Tyre\TyreServiceInterface;
use Illuminate\Http\Request;

/**
 * Class ModelController
 * @package App\Http\Controllers
 */
class ModelController extends Controller
{
    /**
     * Returns the view page of the model
     *
     * @param Request $request
     * @param TyreServiceInterface $tyreService
     * @param FittingServiceInterface $fittingService
     * @param Brand $brand
     * @param $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(
        Request $request,
        TyreServiceInterface $tyreService,
        FittingServiceInterface $fittingService,
        Brand $brand,
        $model
    ) {
        /**
         * @var TyreModel $model
         */
        $model = $tyreService->find(new FindModelByBrandIdAndAlias($model, $brand->getKey()));
        $selectedFitting = $fittingService->find(new FindSelectedFitting($request->cookie()));

        return view('models.view', [
            'brand' => $brand,
            'model' => $model,
            'sizes' => $model->getSizes(),
            'fitting' => $selectedFitting
        ]);
    }
}
