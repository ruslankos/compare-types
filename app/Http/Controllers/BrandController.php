<?php

namespace App\Http\Controllers;

use App\Helpers\DTO\Objects\Tyre\Model\FindModelsByBrandId;
use App\Models\Brand;
use App\Services\Tyre\Brand\Brand\Repository\RepositoryInterface as BrandRepositoryInterface;
use App\Utiles\Resolver\Dispatcher;

class BrandController extends Controller
{
    /**
     * @var BrandRepositoryInterface
     */
    protected $brandRepository;

    /**
     * BrandController constructor.
     * @param BrandRepositoryInterface $brandRepository
     */
    public function __construct(BrandRepositoryInterface $brandRepository)
    {
        $this->brandRepository = $brandRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('brands.index', [
            'brands' => $this->brandRepository->all([
                'has' => 'activeTyres',
                'orderBy' => ['name', 'asc'],
                'relations' => ['image', 'displayModels']
            ]),
        ]);
    }

    /**
     * @param Dispatcher $dispatcher
     * @param Brand $brand
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Dispatcher $dispatcher, Brand $brand)
    {
        $models = $dispatcher->dispatch(new FindModelsByBrandId($brand->id));
        $groups = $models->keys();

        return view('brands.view', [
            'brand' => $brand,
            'models' => $models,
            'groups' => $groups,
        ]);
    }
}
