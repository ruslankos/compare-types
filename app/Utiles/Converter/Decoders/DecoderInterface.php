<?php

namespace App\Utiles\Converter\Decoders;

interface DecoderInterface
{
    public function decode(array $data);
}