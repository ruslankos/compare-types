<?php

namespace App\Utiles\Converter;

use Illuminate\Support\Collection;

interface ConverterInterface
{
    /**
     * Decode snapshot content
     *
     * @param string $content
     * @return array|mixed|object
     */
    public function decode(string $content);


    /**
     * Encode data for snapshot
     *
     * @param Collection $collection
     * @return mixed|string
     */
    public function encode(Collection $collection);
}
