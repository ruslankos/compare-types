<?php

namespace App\Utiles\Converter;

use App\Utiles\Converter\Decoders\DecoderInterface;
use App\Utiles\Converter\Encoders\EncoderInterface;
use App\Utiles\Converter\Transformers\TransformerInterface;
use Illuminate\Support\Collection;

class Converter implements ConverterInterface
{
    /**
     * @var TransformerInterface
     */
    protected $transformer;

    /**
     * @var EncoderInterface
     */
    protected $encoder;

    /**
     * @var DecoderInterface
     */
    protected $decoder;

    /**
     * Converter constructor.
     * @param TransformerInterface $transformer
     * @param EncoderInterface $encoder
     * @param DecoderInterface $decoder
     */
    public function __construct(TransformerInterface $transformer, EncoderInterface $encoder, DecoderInterface $decoder)
    {
        $this->transformer = $transformer;
        $this->encoder = $encoder;
        $this->decoder = $decoder;
    }

    /**
     * Decode snapshot content
     *
     * @param string $content
     * @return array|mixed|object
     */
    public function decode(string $content)
    {
        $data = $this->transformer->decode($content);

        return $this->decoder->decode($data);
    }

    /**
     * Encode data for snapshot
     *
     * @param Collection $collection
     * @return mixed|string
     */
    public function encode(Collection $collection)
    {
        $content = $this->encoder->encode($collection);

        return $this->transformer->encode($content);
    }
}