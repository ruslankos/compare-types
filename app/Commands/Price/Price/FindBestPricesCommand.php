<?php

namespace App\Commands\Price\Price;

use App\Utiles\DTO\Interfaces\DTOInterface;

final class FindBestPricesCommand implements DTOInterface
{
    /**
     * @var int
     */
    private $limit;

    /**
     * @var array
     */
    private $cookies = [];

    /**
     * FindBestPrices constructor.
     * @param int $limit
     * @param array $cookies
     */
    public function __construct($limit, array $cookies)
    {
        $this->limit = $limit;
        $this->cookies = $cookies;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return mixed
     */
    public function getCookies()
    {
        return $this->cookies;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'cookies' => $this->cookies
        ];
    }
}