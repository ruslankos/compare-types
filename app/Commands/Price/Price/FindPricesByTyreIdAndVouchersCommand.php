<?php

namespace App\Commands\Price\Price;

use App\Utiles\DTO\Interfaces\DTOInterface;
use Illuminate\Support\Collection;

final class FindPricesByTyreIdAndVouchersCommand implements DTOInterface
{
    /**
     * @var int
     */
    private $tyreId;

    /**
     * @var Collection
     */
    private $vouchers;

    /**
     * @var array
     */
    private $cookies;

    /**
     * @var int
     */
    private $quantity;

    /**
     * FindPricesByTyreIdAndVouchersCommand constructor.
     * @param int $tyreId
     * @param Collection $vouchers
     * @param int $quantity
     * @param array $cookies
     */
    public function __construct(int $tyreId, Collection $vouchers, int $quantity, array $cookies)
    {
        $this->tyreId = $tyreId;
        $this->vouchers = $vouchers;
        $this->cookies = $cookies;
        $this->quantity = $quantity;
    }

    /**
     * @return int
     */
    public function getTyreId(): int
    {
        return $this->tyreId;
    }

    /**
     * @return Collection
     */
    public function getVouchers(): Collection
    {
        return $this->vouchers;
    }

    /**
     * @return array
     */
    public function getCookies(): array
    {
        return $this->cookies;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'vouchers' => $this->vouchers,
            'tyreId' => $this->tyreId,
            'quantity' => $this->quantity,
            'cookies' => $this->cookies
        ];
    }
}