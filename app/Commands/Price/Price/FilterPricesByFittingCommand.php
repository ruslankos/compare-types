<?php

namespace App\Commands\Price\Price;

use App\Utiles\DTO\Interfaces\DTOInterface;
use Illuminate\Support\Collection;

final class FilterPricesByFittingCommand implements DTOInterface
{
    /**
     * @var int
     */
    private $tyreId;

    /**
     * @var Collection
     */
    private $vouchers;

    /**
     * @var array
     */
    private $fitting;

    /**
     * @var int
     */
    private $quantity;

    /**
     * FilterPricesByFittingCommand constructor.
     * @param int $tyreId
     * @param Collection $vouchers
     * @param int $quantity
     * @param array $fitting
     */
    public function __construct(int $tyreId, Collection $vouchers, int $quantity, array $fitting)
    {
        $this->tyreId = $tyreId;
        $this->vouchers = $vouchers;
        $this->quantity = $quantity;
        $this->fitting = $fitting;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return int
     */
    public function getTyreId(): int
    {
        return $this->tyreId;
    }

    /**
     * @return Collection
     */
    public function getVouchers(): Collection
    {
        return $this->vouchers;
    }

    /**
     * @return array
     */
    public function getFitting(): array
    {
        return $this->fitting;
    }

    /**
     * @return Collection
     */
    public function getFittingForCookies()
    {
        $fitting = $this->fitting['fitting'] ?? null;

        return collect([$fitting]);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'vouchers' => $this->vouchers,
            'tyreId' => $this->tyreId,
            'fitting' => $this->fitting,
            'quantity' => $this->quantity
        ];
    }
}