<?php

namespace App\Commands\Tyre\Tyre;

use App\Utiles\DTO\Interfaces\DTOInterface;

/**
 * Class FindTyreByIdCommand
 * @package App\Commands\Tyre\Tyre
 */
final class FindTyreByIdCommand implements DTOInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $modelUrlName;

    /**
     * @var int
     */
    private $brandId;

    /**
     * FindTyreByIdCommand constructor.
     * @param int $id
     * @param string $modelUrlName
     * @param int $brandId
     */
    public function __construct(int $id, string $modelUrlName, int $brandId)
    {
        $this->id = $id;
        $this->modelUrlName = $modelUrlName;
        $this->brandId = $brandId;
    }

    /**
     * @return int
     */
    public function getBrandId(): int
    {
        return $this->brandId;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getModelUrlName()
    {
        return $this->modelUrlName;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'modelUrlName' => $this->modelUrlName,
            'brandId' => $this->brandId
        ];
    }
}
