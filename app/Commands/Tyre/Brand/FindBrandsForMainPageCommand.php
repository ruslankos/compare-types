<?php

namespace App\Commands\Tyre\Brand;

use App\Utiles\DTO\Interfaces\DTOInterface;

final class FindBrandsForMainPageCommand implements DTOInterface
{
    /**
     * @var int
     */
    private $limit;

    /**
     * FindBrandsForMainPageCommand constructor.
     * @param int $limit
     */
    public function __construct($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'limit' => $this->limit
        ];
    }
}
