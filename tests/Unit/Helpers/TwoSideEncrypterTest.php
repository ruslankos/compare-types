<?php

namespace Tests\Unit\Helpers;

use App\Helpers\Encrypters\TwoSideEncrypter;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class TwoSideEncrypterTest
 * @package Tests\Unit\Helpers
 */
class TwoSideEncrypterTest extends TestCase
{
    /**
     * @var TwoSideEncrypter
     */
    protected $encrypter;

    /**
     *
     */
    public function setUp()
    {
        $this->encrypter = $this->getEncrypter();

        parent::setUp();
    }

    /**
     *
     */
    public function tearDown()
    {
        $this->encrypter = null;

        parent::tearDown();
    }

    /**
     * @return TwoSideEncrypter
     */
    protected function getEncrypter()
    {
        return new TwoSideEncrypter();
    }

    /**
     * A basic test example.
     *
     * @param string $string
     * @param string $result
     * @dataProvider encryptDataProvider
     * @return void
     */
    public function testEncrypt(string $string, string $result)
    {
        $encrypted = $this->encrypter->encrypt($string);

        $this->assertEquals($result, $encrypted);
    }

    /**
     * @param string $result
     * @param string $encrypt
     * @dataProvider encryptDataProvider
     * @return void
     */
    public function testDecrypt(string $result, string $encrypt)
    {
        $decrypted = $this->encrypter->decrypt($encrypt);

        $this->assertEquals($result, $decrypted);
    }

    /**
     * @return array
     */
    public function encryptDataProvider()
    {
        return [
            ['test', 'VzNsT0VxUDQ5N2lMVXRVUmkrWEtidz09'],
            ['Hello', 'bjg4anhreXFhb1FnQjMzendNZFlVQT09']
        ];
    }

}
