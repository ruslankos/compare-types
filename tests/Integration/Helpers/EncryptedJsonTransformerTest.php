<?php

namespace Tests\Integration\Helpers;

use App\Helpers\Encrypters\TwoSideEncrypter;
use App\Helpers\Transformers\EncryptedJsonTransformer;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EncryptedJsonTransformerTest extends TestCase
{
    /**
     * @var EncryptedJsonTransformer
     */
    protected $transformer;

    public function setUp()
    {
        $this->transformer = $this->getTransformer();

        parent::setUp();
    }

    protected function getTransformer()
    {
        return new EncryptedJsonTransformer(new TwoSideEncrypter());
    }

    /**
     * A basic test example.
     *
     * @dataProvider generalDataProvider
     * @return void
     */
    public function testEncode(array $data, string $result)
    {
        $string = $this->transformer->encode($data);

        $this->assertEquals($result, $string);
    }

    /**
     * @param array $result
     * @param string $string
     * @dataProvider generalDataProvider
     * @return void
     */
    public function testDecode(array $result, string $string)
    {
        $data = $this->transformer->decode($string);

        $this->assertEquals($result, $data);
    }

    public function generalDataProvider()
    {
        return [
            [['testIndex' => 'testValue'], 'bmJ4bW1LR1orRXE0U2RtQStUZWRqNzJzc0dEMVpQNjkwM2FGYXFWUnVXWT0=']
        ];
    }
}
