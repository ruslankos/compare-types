<?php

namespace Tests\Traits;

use Illuminate\Support\Collection;

trait ModelsIdsSetable
{
    /**
     * @param Collection $collection
     * @param int $idsStart
     * @return Collection
     */
    protected function setModelsIds(Collection $collection, $idsStart = 1)
    {
        $result = collect([]);
        foreach ($collection as $model) {
            $model->id = $idsStart;
            $result->push($model);
            $idsStart++;
        }

        return $result;
    }
}