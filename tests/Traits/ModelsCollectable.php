<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 30.07.18
 * Time: 12:31
 */

namespace Tests\Traits;


use App\Models\TyreModel;

trait ModelsCollectable
{
    /**
     * @param int $count
     * @param int $idStart
     * @return mixed
     */
    protected function getModels(int $count = 5, int $idStart = 1)
    {
        $result = collect([]);
        $models = factory(TyreModel::class, $count)->make([
            'brand_id' => 1,
        ]);
        $id = $idStart;
        foreach ($models as $model) {
            $model->id = $id;
            $result->push($model);
            $id++;
        }

        return $models;
    }
}