<?php

namespace Tests\Traits;

trait MockObjectable
{
    /**
     * @param string $className
     * @return \PHPUnit\Framework\MockObject\MockObject
     */
    protected function getMockObject(string $className)
    {
        return $this->getMockBuilder($className)
            ->disableOriginalConstructor()
            ->getMock();
    }
}