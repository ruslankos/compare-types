<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', 'CompareTyres.com')</title>
    <meta name="description" content="@yield('meta-desc')">
    <link rel="canonical" href="@yield('canonical-link')"/>
    <link href="https://fonts.googleapis.com/css?family=Karla" rel="stylesheet">

    <!-- Styles
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->
    <link href="{{ asset('css/main.css?ver=1.3') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<div id="app">
    @include('layouts.header', ['searchHeader' => $searchHeader ?? true])

    @yield('content')
</div>
@include('layouts.footer')

    @yield('scripts')
</body>
</html>
