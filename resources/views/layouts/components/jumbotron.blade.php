<div class="jumbotron bg-dark text-white">
    <div class="container justify-content-md-center">
        <h1 class="text-center mb-3">Start search now!</h1>
        @include('layouts.components.jumbotron_search')
    </div>
</div>