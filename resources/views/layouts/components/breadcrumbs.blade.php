<nav aria-label="breadcrumb">
    <div class="container">
        @yield('breadcrumbs')
    </div>
</nav>