@extends('layouts.app')

@section('title', 'CompareTyres | Compare prices from 14+ tyre filters and get you tyres')

@section('canonical-link', route('home'))

@section('meta-desc', 'Compare tyres prices with CompareTyres. View instant quotes from over 30 tyres sellers &amp; buy your tyres today. 100% FREE!')

@section('content')
<div class="container">
    <div class="row">
        @include('main.top_models', ['models' => $models])
        @include('main.best_price', ['prices' => $prices, 'tyresQuantity' => $tyresQuantity])
        @include('main.how-its-works')
        @include('main.sites', ['sites' => $sites])
        @include('main.manufacturers', ['manufacturers' => $manufacturers])
    </div>
</div>
@endsection
